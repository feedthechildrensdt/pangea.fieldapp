﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Appium.Windows;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium;
using System;
using OpenQA.Selenium.Appium;

using Centraler;


namespace Automater
{
    [TestClass]
    public class pangeaFieldAppSession
    {

        protected const string WindowsApplicationDriverUrl = "http://127.0.0.1:4723";

        private const string Pangea_Field_App_ID = Centraler.Settingz.Pangea_Field_App_File_Location;

        public static WindowsDriver<WindowsElement> app_session;

        protected static WindowsElement editBox;

        [TestMethod]
        public void TestMethod1()
        {

        }



        public static void Setup(TestContext context)
        {

            ;

            // Launch a new instance of Notepad application
            if (app_session == null)
            {
                // Create a new session to launch Notepad application
                // DesiredCapabilities appCapabilities = new DesiredCapabilities();
                AppiumOptions appium_Session = new AppiumOptions();

                // appCapabilities.SetCapability("app", NotepadAppId);
                appium_Session.AddAdditionalCapability("app", Pangea_Field_App_ID);

                appium_Session.AddAdditionalCapability("deviceName", "WindowsPC");

                app_session = new WindowsDriver<WindowsElement>(new Uri("http://127.0.0.1:4723"), appium_Session);


                // _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
                // session = new WindowsDriver<WindowsElement>(new Uri(WindowsApplicationDriverUrl), appCapabilities);

                ;

                Assert.IsNotNull(app_session);
                Assert.IsNotNull(app_session.SessionId);

                // Verify that Notepad is started with untitled new file
                // Assert.AreEqual("Untitled - Notepad", session.Title);

                /*
                // Set implicit timeout to 1.5 seconds to make element search to retry every 500 ms for at most three times
                session.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1.5);

                // Keep track of the edit box to be used throughout the session
                editBox = session.FindElementByClassName("Edit");
                Assert.IsNotNull(editBox);
                */

                /*

                */

                ;

            }
        }


        public static void TearDown()
        {
            // Close the application and delete the session
            if (app_session != null)
            {
                app_session.Close();

                try
                {
                    // Dismiss Save dialog if it is blocking the exit
                    app_session.FindElementByName("Don't Save").Click();
                }
                catch { }

                app_session.Quit();
                app_session = null;
            }
        }

    }
}
