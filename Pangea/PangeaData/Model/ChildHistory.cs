﻿using Pangea.Data.Collections;
using Pangea.Data.Model.CodeTables;
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model
{
    /// <summary>
    /// Used for Updating a Child Record.
    /// This is to just store the Child Records History Information.
    /// </summary>
    public partial class ChildHistory 
    {
        private Child _child;

        public ChildHistory()
        { }

        public void Copy(Child child)
        {
            _child = child;
        }

        /// <summary>
        /// Storage for the Country of the Child
        /// </summary>
        public Country ChildCountry
        {
            get { return _child.ChildCountry; }
        }

        /// <summary>
        /// Storage for the Location of the Child
        /// </summary>
        public Location ChildLocation
        {
            get { return _child.ChildLocation; }
        }

        /// <summary>
        /// Storage for the First Name of the Child
        /// </summary>
        public String FirstName
        {
            get { return _child.FirstName; }
        }

        /// <summary>
        /// Storage for the Middle Name of the Child
        /// </summary>
        public String MiddleName
        {
            get { return _child.MiddleName; }
        }

        /// <summary>
        /// Storage for the Last Name of the Child
        /// </summary>
        public String LastName
        {
            get { return _child.LastName; }
        }

        /// <summary>
        /// Storage for the Other Name Know by of the Child
        /// </summary>
        public String NickName
        {
            get { return _child.NickName; }
        }

        /// <summary>
        /// Storage for the DOB of the Child
        /// </summary>
        public DateTime DateOfBirth
        {
            get { return (_child.DateOfBirth.HasValue ? _child.DateOfBirth.Value.Date : DateTime.MinValue.Date); }
        }

        /// <summary>
        /// The Current Numeric Age of the Child.
        /// </summary>
        public int ChildAge
        {
            get { return new DateTime(DateTime.Now.Date.Subtract(DateOfBirth.Date).Ticks).Year - 1; }
        }

        /// <summary>
        /// Storage for the Gender of the Child
        /// </summary>
        public Gender Gender
        {
            get { return _child.Gender; }
        }

        /// <summary>
        /// Storage for the Disability of the Child
        /// </summary>
        public bool? HasDisability
        {
            get { return _child.HasDisability; }
        }

        /// <summary>
        /// Storage for the Number of Brothers of the Child
        /// </summary>
        public int NumberBrothers
        {
            get { return _child.NumberBrothers; }
        }

        /// <summary>
        /// Storage for the Number of Sisters of the Child
        /// </summary>
        public int NumberSisters
        {
            get { return _child.NumberSisters; }
        }

        /// <summary>
        /// Storage for the Favorite Learning of the Child
        /// </summary>
        public FavoriteLearning FavLearning
        {
            get { return _child.FavLearning; }
        }

        /// <summary>
        /// Storage for the Grade Level of the Child
        /// </summary>
        public GradeLevel GradeLvl
        {
            get { return _child.GradeLvl; }
        }

        /// <summary>
        /// Storage for the Personality Type of the Child
        /// </summary>
        public PersonalityType Personality_type
        {
            get { return _child.Personality_type; }
        }

        /// <summary>
        /// Storage for the Lives With of the Child
        /// </summary>
        public LivesWith Lives_With
        {
            get { return _child.Lives_With; }
        }

        /// <summary>
        /// Storage for the Health of the Child
        /// </summary>
        public HealthStatus Health
        {
            get { return _child.Health; }
        }

        /// <summary>
        /// Storage for the Collection of the Childs Chores
        /// </summary>
        public ObservableCollection<Chore> Chores
        {
            get { return _child.Chores; }
        }

        /// <summary>
        /// Storage for the Collection of the Childs Favorite Activity
        /// </summary>
        public ObservableCollection<FavoriteActivity> FavActivity
        {
            get { return _child.FavActivity; }
        }

        /// <summary>
        /// Storage for the Collection of Additional Resources of the child.
        /// Additional Resources are Consent Form, Profile Picture, and Action Picture.
        /// In future there may be more Additional Resources.. videos and stuff.
        /// </summary>
        public FilesCollection AdditionalResources
        {
            get 
            { 
                return _child.AdditionalResources; 
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public String MajorLifeEvent
        {
            get 
            { 
                return _child.MajorLifeEvent; 
            }
        }
    }
}
