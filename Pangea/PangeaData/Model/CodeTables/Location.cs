﻿////////////////////////////////////////////////////////////////////
/// This is the Location Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class Location
    {
        public Location(ObservableCollection<Object> _location)
        {
            // 0        1           2           3           4              5    6       7       8                   9           10              11                  12
            // Active, Action_ID, Action_User, Action_Date, Action_Reason, TS, TSLONG, TSMAN, Location_Code_ID, Location_Name, Country_Code_ID, [Default_Language], Sponsorship_Site
            Active = Convert.ToBoolean(_location[0]);
            CodeID = Convert.ToInt32(_location[8]);
            Name = Convert.ToString(_location[9]);
            CountryCodeID = !_location[10].Equals(DBNull.Value) ? Convert.ToInt32(_location[10]) : int.MinValue;
            DefaultLanguage = !_location[11].Equals(DBNull.Value) ? Convert.ToInt32(_location[11]) : int.MinValue;
            SponsorshipSite = Convert.ToBoolean(_location[12]);
        }


        public Location
            (
            bool _Active,
            Int32 _CodeID,
            string _Name,
            int _CountryCodeID,
            bool _SponsorshipSite,
            int _DefaultLanguage = int.MinValue
            )
        {
            // 0        1           2           3           4              5    6       7       8                   9           10              11                  12
            // Active, Action_ID, Action_User, Action_Date, Action_Reason, TS, TSLONG, TSMAN, Location_Code_ID, Location_Name, Country_Code_ID, [Default_Language], Sponsorship_Site
            Active = _Active; // = Convert.ToBoolean(_location[0]);
            CodeID = _CodeID; // Convert.ToInt32(_location[8]);
            Name = _Name; // Convert.ToString(_location[9]);
            CountryCodeID = _CountryCodeID; //  !_location[10].Equals(DBNull.Value) ? Convert.ToInt32(_location[10]) : int.MinValue;
            DefaultLanguage = _DefaultLanguage; // !_location[11].Equals(DBNull.Value) ? Convert.ToInt32(_location[11]) : int.MinValue;
            SponsorshipSite = _SponsorshipSite; //  Convert.ToBoolean(_location[12]);
        }


        public bool Active { get; set; }

        /// <summary>
        /// This is the Code that is associated with this Location.
        /// In the Original Field App this was called Site Code.
        /// </summary>
        public int CodeID { get; set; }

        /// <summary>
        /// This is name of the Location.
        /// In the Original Field App this was called Site Name
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// This is the Code associated to the Country ID for the Location.
        /// </summary>
        public int CountryCodeID { get; set; }

        public int DefaultLanguage { get; set; }

        public bool SponsorshipSite { get; set; }

        /// <summary>
        /// Used to Determine if the two Location are equal.
        /// </summary>
        /// <param name="obj">The Location obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Location _obj = obj as Location;
            
            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Location are Equal
        /// </summary>
        /// <param name="c1">Location</param>
        /// <param name="c2">Location</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(Location c1, Location c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Location are not Equal
        /// </summary>
        /// <param name="c1">Location</param>
        /// <param name="c2">Location</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(Location c1, Location c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
