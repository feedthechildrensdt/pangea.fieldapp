﻿
using Centraler;
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class ChildRemoveReason
    {
        public ChildRemoveReason(ObservableCollection<Object> _childRemoveReason)
        {
            Active = Convert.ToBoolean(_childRemoveReason[0]);
            CodeID = Convert.ToInt32(_childRemoveReason[1]);
            Name = !_childRemoveReason[2].Equals(DBNull.Value) ? Convert.ToString(_childRemoveReason[2]) : String.Empty;
            Description = !_childRemoveReason[3].Equals(DBNull.Value) ? Convert.ToString(_childRemoveReason[3]) : String.Empty; 
            PublicDescription = !_childRemoveReason[4].Equals(DBNull.Value) ? Convert.ToString(_childRemoveReason[4]) : String.Empty;
            MinAge = !_childRemoveReason[5].Equals(DBNull.Value) ? Convert.ToInt32(_childRemoveReason[5]) : int.MinValue;
        }


        public ChildRemoveReason(CodeTables_StandardData std_data)
        {
            Active = std_data.Active;
            CodeID = std_data.CodeID;
            Name = std_data.Name;
            Description = std_data.Description;
            PublicDescription = std_data.PublicDescription;
            MinAge = std_data.MinAge;
        }


        /// <summary>
        /// Is the Object active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// The objects identifier
        /// </summary>
        public int CodeID { get; set; }

        /// <summary>
        /// Name of the Object
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Description of the Object
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// Public Description of the Object
        /// </summary>
        public String PublicDescription { get; set; }

        /// <summary>
        /// Min Age of the Object.
        /// </summary>
        public int MinAge { get; set; }

        /// <summary>
        /// Used to Determine if the two Child Remove Reason are equal.
        /// </summary>
        /// <param name="obj">The Child Remove Reason obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            ChildRemoveReason _obj = obj as ChildRemoveReason;
            
            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Child Remove Reasons are Equal
        /// </summary>
        /// <param name="c1">Child Remove Reason</param>
        /// <param name="c2">Child Remove Reason</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(ChildRemoveReason c1, ChildRemoveReason c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Child Remove Reasons are not Equal
        /// </summary>
        /// <param name="c1">Child Remove Reason</param>
        /// <param name="c2">Child Remove Reason</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(ChildRemoveReason c1, ChildRemoveReason c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
