﻿
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    /// <summary>
    /// This is the Buisness Object for the Culumn Rules
    /// It has a reference to the  Data Object but only exposes certain properties.
    /// </summary>
    public class ColumnRule
    {
        public ColumnRule(ObservableCollection<Object> _rule)
        {
            Active = Convert.ToBoolean(_rule[0]);
            CodeID = Convert.ToInt32(_rule[1]);
            Name = Convert.ToString(_rule[2]);
            TableName = Convert.ToString(_rule[3]);
            ModuleName = Convert.ToString(_rule[4]);
            ColumnName = Convert.ToString(_rule[5]);
            DefaultStrValue = !_rule[6].Equals(DBNull.Value) ? Convert.ToString(_rule[6]) : String.Empty;
            DefaultIntValue = !_rule[7].Equals(DBNull.Value) ? Convert.ToInt32(_rule[7]) : int.MinValue;
            MinAllowed = !_rule[8].Equals(DBNull.Value) ? Convert.ToInt32(_rule[8]) : int.MinValue;
            MaxAllowed = !_rule[9].Equals(DBNull.Value) ? Convert.ToInt32(_rule[9]) : int.MinValue;
            Required = Convert.ToBoolean(_rule[10]);
            ValidationType = !_rule[11].Equals(DBNull.Value) ? Convert.ToString(_rule[11]) : String.Empty;
            Checked = false;
        }

        /// <summary>
        /// Is this an Active Object
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Id for Object
        /// </summary>
        public int CodeID { get; set; }

        /// <summary>
        /// Name of the Objecct
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Table Name associated to this object
        /// </summary>
        public String TableName { get; set; }

        /// <summary>
        /// Module Name associated to this object
        /// </summary>
        public String ModuleName { get; set; }

        /// <summary>
        /// Column Name associated to this object
        /// </summary>
        public String ColumnName { get; set; }

        /// <summary>
        /// Default String value for this Object
        /// </summary>
        public String DefaultStrValue { get; set; }

        /// <summary>
        /// Default Int Value for this Object
        /// </summary>
        public long? DefaultIntValue { get; set; }
        
        /// <summary>
        /// Min Allowed Value for this Object
        /// </summary>
        public long? MinAllowed { get; set; }

        /// <summary>
        /// Max Allowed Value for this Object
        /// </summary>
        public long? MaxAllowed { get; set; }

        /// <summary>
        /// Is this a required Object
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// What type of validation is this object
        /// </summary>
        public string ValidationType { get; set; }

        /// <summary>
        /// If this column rule is for default values
        /// or for validationvalues.
        /// </summary>
        public bool IsDefaultValues
        {
            get
            {
                return (!String.IsNullOrEmpty(DefaultStrValue) && (DefaultIntValue != null && DefaultIntValue != long.MinValue));
            }
        }

        /// <summary>
        /// If this rule has been checked for the current
        /// validation check.
        /// </summary>
        public bool Checked { get; set; }

        /// <summary>
        /// Used to Determine if the two Objects are equal.
        /// </summary>
        /// <param name="obj">The Action obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            ColumnRule _obj = obj as ColumnRule;

            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Objects are Equal
        /// </summary>
        /// <param name="c1">Action</param>
        /// <param name="c2">Action</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(ColumnRule c1, ColumnRule c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Objects are not Equal
        /// </summary>
        /// <param name="c1">Action</param>
        /// <param name="c2">Action</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(ColumnRule c1, ColumnRule c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
