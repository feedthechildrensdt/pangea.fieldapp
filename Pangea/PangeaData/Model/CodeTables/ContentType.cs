﻿
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class ContentType 
    {
        public ContentType(ObservableCollection<Object> _contentType)
        {
            Active = Convert.ToBoolean(_contentType[0]);
            CodeID = Convert.ToInt32(_contentType[1]);
            Name = Convert.ToString(_contentType[2]);
            Description = !_contentType[3].Equals(DBNull.Value) ? Convert.ToString(_contentType[3]) : String.Empty;
            PublicDescription = !_contentType[4].Equals(DBNull.Value) ? Convert.ToString(_contentType[4]) : String.Empty;
            Required = Convert.ToBoolean(_contentType[5]);
        }

        /// <summary>
        /// Is this an Active Object
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// ID for the Object
        /// </summary>
        public int CodeID { get; set; }

        /// <summary>
        /// Name of the Object
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Description of the Object
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// Public Description of the Object
        /// </summary>
        public String PublicDescription { get; set; }

        /// <summary>
        /// Is the Object Required.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Used to Determine if the two Content Types are equal.
        /// </summary>
        /// <param name="obj">The Content Type obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            ContentType _obj = obj as ContentType;

            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Content Types are Equal
        /// </summary>
        /// <param name="c1">Content Type</param>
        /// <param name="c2">Content Type</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(ContentType c1, ContentType c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Content Types are not Equal
        /// </summary>
        /// <param name="c1">Content Type</param>
        /// <param name="c2">Content Type</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(ContentType c1, ContentType c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
