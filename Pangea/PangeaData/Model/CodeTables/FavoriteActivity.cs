﻿////////////////////////////////////////////////////////////////////
/// This is the Favorite Activity Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class FavoriteActivity
    {
        public FavoriteActivity(ObservableCollection<Object> _favoriteActivity)
        {
            Active = Convert.ToBoolean(_favoriteActivity[0]);
            CodeID = Convert.ToInt32(_favoriteActivity[1]);
            Name = Convert.ToString(_favoriteActivity[2]);
            Description = !_favoriteActivity[3].Equals(DBNull.Value) ? Convert.ToString(_favoriteActivity[3]) : String.Empty;
            PublicDescription = !_favoriteActivity[4].Equals(DBNull.Value) ? Convert.ToString(_favoriteActivity[4]) : String.Empty;
            MinAge = !_favoriteActivity[5].Equals(DBNull.Value) ? Convert.ToInt32(_favoriteActivity[5]) : int.MinValue;
        }


        public FavoriteActivity
            (
            bool _Active,
            Int32 _CodeID,
            string _Name,
            string _Description,
            string _PublicDescription,
            int _MinAge
            )
        {
            Active = _Active;// Convert.ToBoolean(_favoriteActivity[0]);
            CodeID = _CodeID; //  Convert.ToInt32(_favoriteActivity[1]);
            Name = _Name;  // Convert.ToString(_favoriteActivity[2]);
            Description = _Description; // !_favoriteActivity[3].Equals(DBNull.Value) ? Convert.ToString(_favoriteActivity[3]) : String.Empty;
            PublicDescription = _PublicDescription; // !_favoriteActivity[4].Equals(DBNull.Value) ? Convert.ToString(_favoriteActivity[4]) : String.Empty;
            MinAge = _MinAge; // !_favoriteActivity[5].Equals(DBNull.Value) ? Convert.ToInt32(_favoriteActivity[5]) : int.MinValue;
        }

        public bool Active { get; set; }

        public int CodeID { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public String PublicDescription { get; set; }

        public int MinAge { get; set; }

        /// <summary>
        /// Used to Determine if the two Favorite Activities are equal.
        /// </summary>
        /// <param name="obj">The Favorite Activity obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            FavoriteActivity _obj = obj as FavoriteActivity;
            
            return _obj.CodeID == CodeID ;
        }

        /// <summary>
        /// Operator to determine if the two Favorite Activities are Equal
        /// </summary>
        /// <param name="c1">Favorite Activity</param>
        /// <param name="c2">Favorite Activity</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(FavoriteActivity c1, FavoriteActivity c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Favorite Activities are not Equal
        /// </summary>
        /// <param name="c1">Favorite Activity</param>
        /// <param name="c2">Favorite Activity</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(FavoriteActivity c1, FavoriteActivity c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
