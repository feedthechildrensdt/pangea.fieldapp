﻿////////////////////////////////////////////////////////////////////
/// This is the Personality Type Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using Centraler;
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class PersonalityType
    {
        public PersonalityType(ObservableCollection<Object> _personalityType)
        {
            Active = Convert.ToBoolean(_personalityType[0]);
            CodeID = Convert.ToInt32(_personalityType[1]);
            Name = Convert.ToString(_personalityType[2]);
            Description = !_personalityType[3].Equals(DBNull.Value) ? Convert.ToString(_personalityType[3]) : String.Empty;
            PublicDescription = !_personalityType[4].Equals(DBNull.Value) ? Convert.ToString(_personalityType[4]) : String.Empty;
            MinAge = !_personalityType[5].Equals(DBNull.Value) ? Convert.ToInt32(_personalityType[5]) : int.MinValue;
        }


        public PersonalityType(CodeTables_StandardData std_data)
        {
            Active = std_data.Active;
            CodeID = std_data.CodeID;
            Name = std_data.Name;
            Description = std_data.Description;
            PublicDescription = std_data.PublicDescription;
            MinAge = std_data.MinAge;
        }


        public bool Active { get; set; }

        public int CodeID { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public String PublicDescription { get; set; }

        public int MinAge { get; set; }

        /// <summary>
        /// Used to Determine if the two Personality Type are equal.
        /// </summary>
        /// <param name="obj">The Personality Type obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            PersonalityType _obj = obj as PersonalityType;
            
            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Personality Type are Equal
        /// </summary>
        /// <param name="c1">Personality Type</param>
        /// <param name="c2">Personality Type</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(PersonalityType c1, PersonalityType c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Personality Type are not Equal
        /// </summary>
        /// <param name="c1">Personality Type</param>
        /// <param name="c2">Personality Type</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(PersonalityType c1, PersonalityType c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
