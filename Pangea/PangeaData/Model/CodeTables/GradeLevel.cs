﻿////////////////////////////////////////////////////////////////////
/// This is the Grade Level Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;

using Centraler;

namespace Pangea.Data.Model.CodeTables
{
    public class GradeLevel
    {
        public GradeLevel(ObservableCollection<Object> _gradeLevel)
        {
            Active = Convert.ToBoolean(_gradeLevel[0]);
            CodeID = Convert.ToInt32(_gradeLevel[1]);
            Name = Convert.ToString(_gradeLevel[2]);
            Description = !_gradeLevel[3].Equals(DBNull.Value) ? Convert.ToString(_gradeLevel[3]) : String.Empty;
            PublicDescription = !_gradeLevel[4].Equals(DBNull.Value) ? Convert.ToString(_gradeLevel[4]) : String.Empty;
            MinAge = !_gradeLevel[5].Equals(DBNull.Value) ? Convert.ToInt32(_gradeLevel[5]) : int.MinValue;
        }


        public GradeLevel(CodeTables_StandardData std_data)
        {
            Active = std_data.Active; // Convert.ToBoolean(_gradeLevel[0]);
            CodeID = std_data.CodeID; // Convert.ToInt32(_gradeLevel[1]);
            Name = std_data.Name; // Convert.ToString(_gradeLevel[2]);
            Description = std_data.Description; // !_gradeLevel[3].Equals(DBNull.Value) ? Convert.ToString(_gradeLevel[3]) : String.Empty;
            PublicDescription = std_data.PublicDescription; // !_gradeLevel[4].Equals(DBNull.Value) ? Convert.ToString(_gradeLevel[4]) : String.Empty;
            MinAge = std_data.MinAge; // !_gradeLevel[5].Equals(DBNull.Value) ? Convert.ToInt32(_gradeLevel[5]) : int.MinValue;
        }

        public bool Active { get; set; }

        public int CodeID { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public String PublicDescription { get; set; }

        public int MinAge { get; set; }

        /// <summary>
        /// Used to Determine if the two Grade Levels are equal.
        /// </summary>
        /// <param name="obj">The Grade Levels obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            GradeLevel _obj = obj as GradeLevel;
            
            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Grade Levels are Equal
        /// </summary>
        /// <param name="c1">Grade Levels</param>
        /// <param name="c2">Grade Levels</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(GradeLevel c1, GradeLevel c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Grade Levels are not Equal
        /// </summary>
        /// <param name="c1">Grade Level</param>
        /// <param name="c2">Grade Levels</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(GradeLevel c1, GradeLevel c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
