﻿////////////////////////////////////////////////////////////////////
/// This is the Chore Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class Chore
    {
        public Chore(ObservableCollection<Object> _chore)
        {
            Active = Convert.ToInt32(_chore[0]);
            CodeID = Convert.ToInt32(_chore[1]);
            Name = Convert.ToString(_chore[2]);
            Description = !_chore[3].Equals(DBNull.Value) ? Convert.ToString(_chore[3]) : String.Empty;
            PublicDescription = !_chore[4].Equals(DBNull.Value) ? Convert.ToString(_chore[4]) : String.Empty;
            MinAge = !_chore[5].Equals(DBNull.Value) ? Convert.ToInt32(_chore[5]) : int.MinValue;
        }



        public Chore
            (
            Int32 _Active,
            Int32 _CodeID,
            string _Name,
            string _Description,
            string _PublicDescription,
            int _MinAge
            )
        {
            Active = _Active;// Convert.ToInt32(_chore[0]);
            CodeID = _CodeID; // Convert.ToInt32(_chore[1]);
            Name = _Name; // Convert.ToString(_chore[2]);
            Description = _Description; // !_chore[3].Equals(DBNull.Value) ? Convert.ToString(_chore[3]) : String.Empty;
            PublicDescription = _PublicDescription; // !_chore[4].Equals(DBNull.Value) ? Convert.ToString(_chore[4]) : String.Empty;
            MinAge = _MinAge; // !_chore[5].Equals(DBNull.Value) ? Convert.ToInt32(_chore[5]) : int.MinValue;
        }



        /// <summary>
        /// Is this an Active Object
        /// </summary>
        public int Active { get; set; }

        /// <summary>
        /// ID for the Object
        /// </summary>
        public int CodeID { get; set; }

        /// <summary>
        /// Name of the Object
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Description of the Object
        /// </summary>
        public String Description { get; set; }

        /// <summary>
        /// Public Description of the Object
        /// </summary>
        public String PublicDescription { get; set; }

        /// <summary>
        /// Min Age for the Object
        /// </summary>
        public int? MinAge { get; set; }

        /// <summary>
        /// Used to Determine if the two Chores are equal.
        /// </summary>
        /// <param name="obj">The Chore obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Chore _obj = obj as Chore;

            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Chores are Equal
        /// </summary>
        /// <param name="c1">Chore</param>
        /// <param name="c2">Chore</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(Chore c1, Chore c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Chores are not Equal
        /// </summary>
        /// <param name="c1">Chore</param>
        /// <param name="c2">Chore</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(Chore c1, Chore c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
