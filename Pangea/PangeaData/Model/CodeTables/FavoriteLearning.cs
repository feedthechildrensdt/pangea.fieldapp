﻿////////////////////////////////////////////////////////////////////
/// This is the Favorite Learning Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class FavoriteLearning
    {
        public FavoriteLearning(ObservableCollection<Object> _favoriteLearning)
        {
            Active = Convert.ToBoolean(_favoriteLearning[0]);
            CodeID = Convert.ToInt32(_favoriteLearning[1]);
            Name = Convert.ToString(_favoriteLearning[2]);
            Description = !_favoriteLearning[3].Equals(DBNull.Value) ? Convert.ToString(_favoriteLearning[3]) : String.Empty;
            PublicDescription = !_favoriteLearning[4].Equals(DBNull.Value) ? Convert.ToString(_favoriteLearning[4]) : String.Empty;
            MinAge = !_favoriteLearning[5].Equals(DBNull.Value) ? Convert.ToInt32(_favoriteLearning[5]) : int.MinValue;
        }

        public FavoriteLearning
            (
            bool _Active,
            Int32 _CodeID,
            string _Name,
            string _Description,
            string _PublicDescription,
            int _MinAge
            )
        {
            Active = _Active; // Convert.ToBoolean(_favoriteLearning[0]);
            CodeID = _CodeID; // Convert.ToInt32(_favoriteLearning[1]);
            Name = _Name; // Convert.ToString(_favoriteLearning[2]);
            Description = _Description; //  !_favoriteLearning[3].Equals(DBNull.Value) ? Convert.ToString(_favoriteLearning[3]) : String.Empty;
            PublicDescription = _PublicDescription; // !_favoriteLearning[4].Equals(DBNull.Value) ? Convert.ToString(_favoriteLearning[4]) : String.Empty;
            MinAge = _MinAge; // !_favoriteLearning[5].Equals(DBNull.Value) ? Convert.ToInt32(_favoriteLearning[5]) : int.MinValue;
        }

        public bool Active { get; set; }

        public int CodeID { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public String PublicDescription { get; set; }

        public int MinAge { get; set; }

        /// <summary>
        /// Used to Determine if the two Favorite Learnings are equal.
        /// </summary>
        /// <param name="obj">The Favorite Learning obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            FavoriteLearning _obj = obj as FavoriteLearning;
            
            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Favorite Learnings are Equal
        /// </summary>
        /// <param name="c1">Favorite Learning</param>
        /// <param name="c2">Favorite Learning</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(FavoriteLearning c1, FavoriteLearning c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Favorite Learnings are not Equal
        /// </summary>
        /// <param name="c1">Favorite Learning</param>
        /// <param name="c2">Favorite Learning</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(FavoriteLearning c1, FavoriteLearning c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
