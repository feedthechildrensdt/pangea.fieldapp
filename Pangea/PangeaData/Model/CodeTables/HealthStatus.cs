﻿////////////////////////////////////////////////////////////////////
/// This is the Health Status Buisness Object.  It has a reference to the
/// Data Object and only exposes certain properties.
////////////////////////////////////////////////////////////////////

using Centraler;
using System;
using System.Collections.ObjectModel;

namespace Pangea.Data.Model.CodeTables
{
    public class HealthStatus
    {
        public HealthStatus(ObservableCollection<Object> _healthStatus)
        {
            Active = Convert.ToBoolean(_healthStatus[0]);
            CodeID = Convert.ToInt32(_healthStatus[1]);
            Name = Convert.ToString(_healthStatus[2]);
            Description = !_healthStatus[3].Equals(DBNull.Value) ? Convert.ToString(_healthStatus[3]) : String.Empty;
            PublicDescription = !_healthStatus[4].Equals(DBNull.Value) ? Convert.ToString(_healthStatus[4]) : String.Empty;
            MinAge = !_healthStatus[5].Equals(DBNull.Value) ? Convert.ToInt32(_healthStatus[5]) : int.MinValue;
        }


        public HealthStatus(CodeTables_StandardData std_data)
        {
            Active = std_data.Active; 
            CodeID = std_data.CodeID; 
            Name = std_data.Name; 
            Description = std_data.Description; 
            PublicDescription = std_data.PublicDescription; 
            MinAge = std_data.MinAge; 
        }

        public bool Active { get; set; }

        public int CodeID { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public String PublicDescription { get; set; }

        public int MinAge { get; set; }

        /// <summary>
        /// Used to Determine if the two Health Status are equal.
        /// </summary>
        /// <param name="obj">The Health Status obj that we are testing with</param>
        /// <returns>True if they are equal/False if they are not.</returns>
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            HealthStatus _obj = obj as HealthStatus;
            
            return _obj.CodeID == CodeID;
        }

        /// <summary>
        /// Operator to determine if the two Health Status are Equal
        /// </summary>
        /// <param name="c1">Health Status</param>
        /// <param name="c2">Health Status</param>
        /// <returns>True if Equal/False if Not Equal</returns>
        public static bool operator ==(HealthStatus c1, HealthStatus c2)
        {
            if ((object)c1 == null)
                return ((object)c2 == null);

            return c1.Equals(c2);
        }

        /// <summary>
        /// Operator to determine if the two Health Status are not Equal
        /// </summary>
        /// <param name="c1">Health Status</param>
        /// <param name="c2">Health Status</param>
        /// <returns>False if Equal/True if Not Equal</returns>
        public static bool operator !=(HealthStatus c1, HealthStatus c2)
        {
            if ((object)c1 == null)
                return ((object)c2 != null);

            return !c1.Equals(c2);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
