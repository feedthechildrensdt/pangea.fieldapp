﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PangeaData.Model.DataExport
{

    public class GridData_Details
    {
        public List<string> Grid_Export_Column_Headers = new List<string>()
        {
            "Location Name",
            "Location Code",
            "Child Number",
            "First Name",
            "Middle Name",
            "Last Name",
            "Other Name",
            "Date Of Birth",
            "Sex"
        };

        public List<gridData> GridData { get; set; }

        public GridData_Details()
        {
            GridData = new List<gridData>();
        }

    }
    

    public class gridData
    {

        public string Location_Name { get; set; }

        public string Location_Code { get; set; }

        public string Child_ID { get; set; }

        public string Child_Number { get; set; }

        public string First_Name { get; set; }

        public string Middle_Name { get; set; }

        public string Last_Name { get; set; }

        public string Other_Name { get; set; }

        public string Date_Of_Birth { get; set; }

        public string Sex { get; set; }

    }

}
