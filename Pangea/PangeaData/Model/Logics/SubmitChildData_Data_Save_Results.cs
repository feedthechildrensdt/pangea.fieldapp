﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PangeaData.Model.Logics
{
    public class SubmitChildData_Data_Save_Results
    {
        public List<SubmitChildData_Data_Save_Result> the_Results = new List<SubmitChildData_Data_Save_Result>();

        public long? ChildNumber { get; set; }

        public Guid? ChildID { get; set; }

        public List<SubmitChildData_Data_Save_Result> the_Failed_Results
        {
            get
            {
                List<SubmitChildData_Data_Save_Result> l_temp = 
                    the_Results
                    .Where
                    (
                        xx => 
                        xx.result == 1
                        )
                    .ToList();

                return l_temp;
            }
        }

    }
}
