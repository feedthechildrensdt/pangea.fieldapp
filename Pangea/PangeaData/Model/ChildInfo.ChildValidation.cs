﻿
using System;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.Linq;
using Pangea.Data.Collections;
using Pangea.Data.DataObjects;
using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Model
{
    /// <summary>
    /// This Class is a continuation of the Child Info Class
    /// This will contain the functions for Child Validation and Child Duplicates
    /// This will help keep the Child Info Class from Becoming too large in one file.
    /// </summary>
    public partial class ChildInfo
    {
        /// <summary>
        /// This will check to make sure that all the required fields have been
        /// filled out and that all of the required documents are there.
        /// </summary>
        /// <param name="forEnroll">Whether this is for Enrolling or just Saving the Enroll Progress</param>
        /// <returns>Whether it is valid to enroll or save progress of the child</returns>
        public Tuple<bool, bool> ValidateChildInfo()
        {
            PangeaInfo.Messages.RemoveAll(MessageTypes.ValidationError);

            bool retValSave = true;

            bool retValEnroll = true;

            long? minAllowed;

            long? maxAllowed;

            PangeaCollections.ColumnRulesCollection.ClearChecks();

            ColumnRulesCollection the_rules = PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild);

            foreach (ColumnRule colRule in the_rules)
            {
                if 
                    (
                    colRule.Active && 
                    colRule.Required && 
                    !colRule.IsDefaultValues
                    )
                {
                    // Check to make sure we have a valid first name.
                    if (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("firstname")
                        )
                    {
                        if (
                            String.IsNullOrEmpty(FirstName) || 
                            String.IsNullOrWhiteSpace(FirstName)
                            )
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required First Name is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValSave = false;
                        }

                        colRule.Checked = true;
                    }
                    // Check to make sue we have a valid middle name.
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("middlename")
                        )
                    {
                        if (
                            String.IsNullOrEmpty(MiddleName) || 
                            String.IsNullOrWhiteSpace(MiddleName)
                            )
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Middle Name is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValSave = false;
                        }

                        colRule.Checked = true;
                    }
                    // Check to make sure we have a valid last name.
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("lastname")
                        )
                    {
                        if (String.IsNullOrEmpty(LastName) || String.IsNullOrWhiteSpace(LastName))
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Last Name is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValSave = false;
                        }

                        colRule.Checked = true;
                    }
                    // Check to make sue we have a valid nickname or Other Name Child Goes By.
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("othernamegoesby")
                        )
                    {
                        if (
                            String.IsNullOrEmpty(NickName) || 
                            String.IsNullOrWhiteSpace(NickName)
                            )
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Other Name Child Goes By is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValSave = false;
                        }

                        colRule.Checked = true;
                    }
                    // Check to make sure we have a valid date of birth
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("dateofbirth")
                        )
                    {
                        if (
                            colRule
                            .ValidationType
                            .ToLower()
                            .Contains("rangevalidation")
                            )  // TODO : We seem to be missing the Disabled age range.
                        {
                            minAllowed = colRule.MinAllowed;

                            maxAllowed = colRule.MaxAllowed;

                            // Made a change to get how old the Child Was in Days since the age range is from 5 days to 14 Years.  
                            if (ChildAge < minAllowed)
                            {
                                // BUG 267 - Added Days old to the end of the Message
                                PangeaInfo.Messages.Add(new MessagesStruct(String.Format("Child is too young.  Minimum age is {0} days old.", minAllowed), MessageTypes.ValidationError, colRule.ColumnName));

                                retValSave = false;
                            }
                            else if (ChildAge > maxAllowed)
                            {
                                // BUG 267 - Converting MaxAllowed age from days to Years and added years old to the message.
                                PangeaInfo.Messages.Add(new MessagesStruct(String.Format("Child is too old.  Max age is {0} years old.", Convert.ToInt32(maxAllowed / 365)), MessageTypes.ValidationError, colRule.ColumnName));

                                retValSave = false;
                            }
                        }
                        else
                        {
                            if (DateOfBirth == null)
                            {
                                PangeaInfo.Messages.Add(new MessagesStruct("Child Date Of Birth is a Required.", MessageTypes.ValidationError, colRule.ColumnName));

                                retValSave = false;
                            }
                        }
                        colRule.Checked = true;
                    }
                    // Check to see if the Gender is filled.
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("gendercodeid")
                        )
                    {
                        if (Gender == null)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Sex of the Child is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValSave = false;
                        }

                        colRule.Checked = true;
                    }
                    // Check to see if the Child Location is Filled.
                    else if 
                        (
                        colRule
                        .ColumnName.
                        ToLower()
                        .Contains("locationcodeid")
                        )
                    {
                        if (ChildLocation == null)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Location information is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValSave = false;
                        }
                        colRule.Checked = true;
                    }
                    // Check to see if the Child Grade level is Filled.
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("gradelevelcodeid")
                        )
                    {
                        if (GradeLvl == null)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Grade Level is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }
                        colRule.Checked = true;
                    }
                    // Check to see if Diability status has been filled.
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("disabilitystatuscodeid")
                        )
                    {
                        // BUG 248 - Added Disability Validation Check for when you are trying to enroll a child.
                        if (HasDisability == null)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Disability of the Child is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }
                        colRule.Checked = true;
                    }
                    // Check to see if the Chores have been filled out.
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("chorecodeid")
                        )
                    {
                        if (
                            !colRule
                            .ValidationType
                            .ToLower()
                            .Contains("reqvalidation")
                            )
                        {
                            minAllowed = colRule.MinAllowed;

                            maxAllowed = colRule.MaxAllowed;

                            bool checkMin = minAllowed.HasValue ? (Chores.Count < minAllowed.Value) : false;

                            bool checkMax = maxAllowed.HasValue ? (Chores.Count > maxAllowed.Value) : false;

                            if (checkMin)
                            {
                                PangeaInfo.Messages.Add(new MessagesStruct(String.Format("Must have at least {0} Chore in order to Enroll a Child.", minAllowed), MessageTypes.ValidationError, colRule.ColumnName));

                                retValEnroll = false;
                            }
                            else if (checkMax)
                            {
                                PangeaInfo.Messages.Add(new MessagesStruct(String.Format("There can only be a Maximum of {0} Chores in order to Enroll a Child.", maxAllowed.Value), MessageTypes.ValidationError, colRule.ColumnName));

                                retValEnroll = false;
                            }
                        }
                        colRule.Checked = true;
                    }
                    // Check to see if the Favorite Activities have been filled out.
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("favoriteactivitycodeid")
                        )
                    {
                        if (
                            !colRule
                            .ValidationType
                            .ToLower()
                            .Contains("reqvalidation")
                            )
                        {
                            minAllowed = colRule.MinAllowed;

                            maxAllowed = colRule.MaxAllowed;


                            bool checkMin = minAllowed.HasValue ? (FavActivity.Count < minAllowed.Value) : false;

                            bool checkMax = maxAllowed.HasValue ? (FavActivity.Count > maxAllowed.Value) : false;

                            if (checkMin)
                            {
                                PangeaInfo.Messages.Add(new MessagesStruct(String.Format("Must have at least {0} Favorite Activity in order to Enroll a Child.", minAllowed.Value), MessageTypes.ValidationError, colRule.ColumnName));

                                retValEnroll = false;
                            }

                            if (checkMax)
                            {
                                PangeaInfo.Messages.Add(new MessagesStruct(String.Format("There can only be a Maximum of {0} Favorite Activity in order to Enroll a Child.", maxAllowed.Value), MessageTypes.ValidationError, colRule.ColumnName));

                                retValEnroll = false;
                            }
                        }

                        colRule.Checked = true;
                    }
                    // Check to see if health is filled
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("healthstatuscodeid")
                        )
                    {
                        if (Health == null)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Health is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }

                        colRule.Checked = true;
                    }
                    // Check to see if personality type is filled
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("personalitytypecodeid")
                        )
                    {
                        if (Personality_type == null)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Personality type is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }
                        colRule.Checked = true;
                    }
                    // Check to see if favorite learning is filled
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("favoritelearningcodeid")
                        )
                    {
                        if (FavLearning == null)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Favorite Learning is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }

                        colRule.Checked = true;
                    }
                    // Check to see if lives with is filled
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("liveswithcodeid")
                        )
                    {
                        if (Lives_With == null)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Lives With is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }

                        colRule.Checked = true;
                    }
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("numberofbrothers")
                        )
                    {
                        maxAllowed = colRule.MaxAllowed;

                        if (NumberBrothers > maxAllowed)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct(String.Format("Number of Brothers greater than max allowed of {0}.", maxAllowed), MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }

                        colRule.Checked = true;
                    }
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("numberofsisters")
                        )
                    {
                        maxAllowed = colRule.MaxAllowed;

                        if (NumberSisters > maxAllowed)
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct(String.Format("Number of Sisters greater than max allowed of {0}.", maxAllowed), MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }

                        colRule.Checked = true;
                    }
                    else if 
                        (
                        colRule
                        .ColumnName
                        .ToLower()
                        .Contains("majorlifeevent")
                        )
                    {
                        minAllowed = colRule.MinAllowed;

                        if 
                            (
                            MajorLifeEvent == null || 
                            MajorLifeEvent.Equals(String.Empty) || 
                            MajorLifeEvent.Trim().Equals(String.Empty)
                            )
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Major Life Event is Required.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }
                        else if 
                            (
                            (MajorLifeEvent.Length < minAllowed) || 
                            (MajorLifeEvent.Trim().Length < minAllowed)
                            )
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct(String.Format("Invalid Major Life Event.  Majore Life Event Requires a Minnimum of {0} characters.", minAllowed), MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }

                        colRule.Checked = true;
                    }
                    else
                    {
                        foreach (PangeaFileStruct panFileStruct in AdditionalResources)
                        {
                            string stemp = panFileStruct.ContentTypeInfo.Name.ToLower();

                            if (
                                colRule
                                .ColumnName
                                .ToLower()
                                .Contains(stemp)
                                )
                            {
                                colRule.Checked = true;

                                break;
                            }
                        }
                    }

                    if (!colRule.Checked)
                    {
                        if 
                            (
                            colRule
                            .ColumnName
                            .ToLower()
                            .Contains("actionphoto")
                            )
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Action Photo is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }
                        else if 
                            (
                            colRule
                            .ColumnName
                            .ToLower()
                            .Contains("consent")
                            )
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Consent Form is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }
                        else if 
                            (
                            colRule
                            .ColumnName
                            .ToLower()
                            .Contains("profilephoto")
                            )
                        {
                            PangeaInfo.Messages.Add(new MessagesStruct("Required Profile Picture is Missing.", MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }
                        else
                        {
                            // Rule was not Hit
                            PangeaInfo.Messages.Add(new MessagesStruct(colRule.Name, MessageTypes.ValidationError, colRule.ColumnName));

                            retValEnroll = false;
                        }
                    }
                }
            }

            return new Tuple<bool, bool>(retValSave, retValEnroll);
        }

        /// <summary>
        /// This checks for any Possible duplicate children and returns a collection
        /// of them.
        /// </summary>
        /// <returns>A collection of possible duplicate children</returns>
        public ObservableCollection<Child> PossibleDuplicates
        {
            get
            {
                ObservableCollection<Child> retVal = null;

                if 
                    (
                    ChildLocation == null || 
                    String.IsNullOrEmpty(FirstName) || 
                    String.IsNullOrEmpty(LastName) || 
                    Gender == null || 
                    PangeaInfo.UpdatingChild
                    )
                    return retVal;

                EnrollmentTablesDataContext _enrollTablesDC = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                ISingleResult<GetPossibleChildDuplicatesResult> _possibleDups = _enrollTablesDC.Get_Possible_Child_Duplicates
                    (
                    PangeaInfo.User.UserID,
                    PangeaInfo.Child.ChildID,
                    FirstName, 
                    MiddleName, 
                    LastName, 
                    NickName, 
                    DateOfBirth, 
                    (byte)Gender.CodeID, 
                    ChildLocation.CodeID
                    );

                foreach(GetPossibleChildDuplicatesResult _possDupRes in _possibleDups)
                {
                    ISingleResult<GetASavedResult> _childResults = _enrollTablesDC.Get_A_Saved_Enrollment(PangeaInfo.User.UserID, _possDupRes.Child_ID);

                    foreach(GetASavedResult _chRes in _childResults)
                    {
                        if (retVal == null)
                            retVal = new ObservableCollection<Child>();

                        if (ChildID.HasValue ? !_possDupRes.Child_ID.Equals(ChildID) : true)
                            retVal.Add(new Child(_possDupRes.Child_ID, _chRes));
                    }
                }
                
                foreach (ChildDuplicate _dupChild in NonDuplicates)
                {
                    retVal.Remove(retVal.Where(c => !c.ChildID.Equals(_dupChild.Child2_ID)).SingleOrDefault());
                }

                return retVal;
            }
        }
    }
}
