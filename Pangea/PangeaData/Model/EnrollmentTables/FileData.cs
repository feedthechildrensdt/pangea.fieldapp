﻿using Pangea.Data.DataObjects;
using Pangea.Utilities.Base.Classes;
using System;

namespace Pangea.Data.Model
{
    public class FileData : PangeaBaseNotifyProperties
    {
        private FileDataDO _fileDataDO;

        public FileData()
        {
            _fileDataDO = new FileDataDO();
        }

        public FileData(FileDataDO fileDataDO)
        {
            _fileDataDO = fileDataDO;
        }

        public FileData(GetFileDataResult _gefdr)
        {
            _fileDataDO = new FileDataDO();

            URI = _gefdr.URI;

            GeoLatitude = _gefdr.Geo_Latitude;

            GeoLongitude = _gefdr.Geo_Longitude;

            Make = _gefdr.Make;

            Model = _gefdr.Model;

            Software = _gefdr.Software;

            DateTime = _gefdr.DateTime;

            DateTimeOriginal = _gefdr.DateTimeOriginal;

            DateTimeDigitized = _gefdr.DateTimeDigitized;

            GPSVersionID = _gefdr.GPSVersionID;

            GPSLatitudeRef = _gefdr.GPSLatitudeRef;

            GPSLatitude = _gefdr.GPSLatitude;

            GPSLongitudeRef = _gefdr.GPSLongitudeRef;

            GPSLongitude = _gefdr.GPSLongitude;

            GPSAltitudeRef = _gefdr.GPSAltitudeRef;

            GPSAltitude = _gefdr.GPSAltitude;

            GPSTimeStamp = _gefdr.GPSTimeStamp;

            GPSImgDirectionRef = _gefdr.GPSImgDirectionRef;

            GPSImgDirection = _gefdr.GPSImgDirection;

            GPSDateStamp = _gefdr.GPSDateStamp;
        }

        public String URI
        {
            get { return _fileDataDO.URI; }
            set
            {
                if (_fileDataDO.URI != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.URI = value;
                    SendPropertyChanged();
                }
            }
        }

        public Decimal? GeoLatitude
        {
            get { return _fileDataDO.Geo_Latitude; }
            set
            {
                if (_fileDataDO.Geo_Latitude != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.Geo_Latitude = value;
                    SendPropertyChanged();
                }
            }
        }

        public Decimal? GeoLongitude
        {
            get { return _fileDataDO.Geo_Longitude; }
            set
            {
                if (_fileDataDO.Geo_Longitude != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.Geo_Longitude = value;
                    SendPropertyChanged();
                }
            }
        }

        public String Make
        {
            get { return _fileDataDO.Make; }
            set
            {
                if (_fileDataDO.Make != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.Make = value;
                    SendPropertyChanged();
                }
            }
        }

        public String Model
        {
            get { return _fileDataDO.Model; }
            set
            {
                if (_fileDataDO.Model != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.Model = value;
                    SendPropertyChanged();
                }
            }
        }

        public String Software
        {
            get { return _fileDataDO.Software; }
            set
            {
                if (_fileDataDO.Software != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.Software = value;
                    SendPropertyChanged();
                }
            }
        }

        public DateTime? DateTime
        {
            get { return _fileDataDO.DateTime; }
            set
            {
                if (_fileDataDO.DateTime != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.DateTime = value;
                    SendPropertyChanged();
                }
            }
        }

        public DateTime? DateTimeOriginal
        {
            get { return _fileDataDO.DateTimeOriginal; }
            set
            {
                if (_fileDataDO.DateTimeOriginal != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.DateTimeOriginal = value;
                    SendPropertyChanged();
                }
            }
        }

        public DateTime? DateTimeDigitized
        {
            get { return _fileDataDO.DateTimeDigitized; }
            set
            {
                if (_fileDataDO.DateTimeDigitized != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.DateTimeDigitized = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSVersionID
        {
            get { return _fileDataDO.GPSVersionID; }
            set
            {
                if (_fileDataDO.GPSVersionID != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSVersionID = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSLatitudeRef
        {
            get { return _fileDataDO.GPSLatitudeRef; }
            set
            {
                if (_fileDataDO.GPSLatitudeRef != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSLatitudeRef = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSLatitude
        {
            get { return _fileDataDO.GPSLatitude; }
            set
            {
                if (_fileDataDO.GPSLatitude != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSLatitude = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSLongitudeRef
        {
            get { return _fileDataDO.GPSLongitudeRef; }
            set
            {
                if (_fileDataDO.GPSLongitudeRef != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSLongitudeRef = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSLongitude
        {
            get { return _fileDataDO.GPSLongitude; }
            set
            {
                if (_fileDataDO.GPSLongitude != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSLongitude = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSAltitudeRef
        {
            get { return _fileDataDO.GPSAltitudeRef; }
            set
            {
                if (_fileDataDO.GPSAltitudeRef != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSAltitudeRef = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSAltitude
        {
            get { return _fileDataDO.GPSAltitude; }
            set
            {
                if (_fileDataDO.GPSAltitude != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSAltitude = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSTimeStamp
        {
            get { return _fileDataDO.GPSTimeStamp; }
            set
            {
                if (_fileDataDO.GPSTimeStamp != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSTimeStamp = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSImgDirectionRef
        {
            get { return _fileDataDO.GPSImgDirectionRef; }
            set
            {
                if (_fileDataDO.GPSImgDirectionRef != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSImgDirectionRef = value;
                    SendPropertyChanged();
                }
            }
        }

        public Decimal? GPSImgDirection
        {
            get { return _fileDataDO.GPSImgDirection; }
            set
            {
                if (_fileDataDO.GPSImgDirection != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSImgDirection = value;
                    SendPropertyChanged();
                }
            }
        }

        public String GPSDateStamp
        {
            get { return _fileDataDO.GPSDateStamp; }
            set
            {
                if (_fileDataDO.GPSDateStamp != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.GPSDateStamp = value;
                    SendPropertyChanged();
                }
            }
        }

        public int Action_ID
        {
            get { return _fileDataDO.Action_ID; }
            set
            {
                if (_fileDataDO.Action_ID != value)
                {
                    SendPropertyChanging();
                    _fileDataDO.Action_ID = value;
                    SendPropertyChanged();
                }
            }
        } 
    }
}
