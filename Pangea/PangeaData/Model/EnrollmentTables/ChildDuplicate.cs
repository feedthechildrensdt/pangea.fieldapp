﻿using System;

using Pangea.Data.DataObjects;
using Pangea.Utilities.Base.Classes;

namespace Pangea.Data.Model
{
    /// <summary>
    /// This class is used to store the Non Child Duplicates 
    /// that are associated with the Child
    /// Child1_ID is the Child
    /// Child2_ID is the possible duplicates
    /// </summary>
    public class ChildDuplicate : PangeaBaseNotifyProperties
    {
        private ChildDuplicateDO _childDuplicateDO;

        public ChildDuplicate()
        {
            _childDuplicateDO = new ChildDuplicateDO();
        }

        public ChildDuplicate(ChildDuplicateDO _childDuplicate)
        {
            _childDuplicateDO = _childDuplicate;
        }

        public ChildDuplicate(Child _child)
        {
            _childDuplicateDO = new ChildDuplicateDO();
            Child2_ID = _child.ChildID.Value;
        }

        public ChildDuplicate(GetEnrollmentNonDuplicateResult _childDup)
        {
            _childDuplicateDO = new ChildDuplicateDO();
            Child1_ID = _childDup.Child_ID_1;
            Child2_ID = _childDup.Child_ID_2;
        }

        public Guid Child1_ID
        {
            get { return _childDuplicateDO.Child_ID_1; }
            set
            {
                if (_childDuplicateDO.Child_ID_1 != value)
                {
                    SendPropertyChanging();
                    _childDuplicateDO.Child_ID_1 = value;
                    SendPropertyChanged();
                }
            }
        }

        public Guid Child2_ID
        {
            get { return _childDuplicateDO.Child_ID_2; }
            set
            {
                if (_childDuplicateDO.Child_ID_2 != value)
                {
                    SendPropertyChanging();
                    _childDuplicateDO.Child_ID_2 = value;
                    SendPropertyChanged();
                }
            }
        }
    }
}
