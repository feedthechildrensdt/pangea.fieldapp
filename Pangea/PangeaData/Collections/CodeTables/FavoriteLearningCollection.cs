﻿////////////////////////////////////////////////////////////////
/// Observable collection of the Favorite Learning data.  Used to store all
/// of the active Favorite Learning from the Favorite Learning Codes Database.
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class FavoriteLearningCollection : ObservableCollection<FavoriteLearning>
    {
        public FavoriteLearningCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new FavoriteLearning(_item));
            }
        }

        public FavoriteLearningCollection(IEnumerable<FavoriteLearning> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<FavoriteLearning> _items)
        {
            foreach (FavoriteLearning item in _items)
            {
                Add(item);
            }
        }

        public FavoriteLearning Find(FavoriteLearning _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public FavoriteLearning Find (int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(fl => fl.CodeID == _codeID.Value).FirstOrDefault();
        }

        // Bug 266 - Added to Restrict the List to only the ones that meet the current Age Requirement.
        public FavoriteLearningCollection RestrictByAge(double _childAge)
        {
            return new FavoriteLearningCollection(this.Where(item => item.MinAge <= _childAge));
        }
    }
}
