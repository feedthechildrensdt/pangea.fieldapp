﻿////////////////////////////////////////////////////////////////
/// Observable collection of the Lives With data.  Used to store all
/// of the active Lives With from the Lives With Codes Database.
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class LivesWithCollection : ObservableCollection<LivesWith>
    {
        public LivesWithCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new LivesWith(_item));
            }
        }
        public LivesWithCollection(IEnumerable<LivesWith> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<LivesWith> _items)
        {
            foreach (LivesWith item in _items)
            {
                Add(item);
            }
        }

        public LivesWith Find(LivesWith _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public LivesWith Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(lw => lw.CodeID == _codeID).FirstOrDefault();
        }
    }
}
