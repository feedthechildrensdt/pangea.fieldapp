﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class LanguageCollection : ObservableCollection<Language>
    {
        public LanguageCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new Language(_item));
            }
        }

        public LanguageCollection(IEnumerable<Language> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<Language> _items)
        {
            foreach (Language item in _items)
            {
                Add(item);
            }
        }

        public Language Find(Language _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public Language Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(c => c.CodeID == _codeID.Value).FirstOrDefault();
        }

        public Language Find(string _languageDesc)
        {
            if (string.IsNullOrEmpty(_languageDesc) || string.IsNullOrWhiteSpace(_languageDesc))
                return null;

            return this.Where(c => c.Code.ToLower().Equals(_languageDesc.ToLower())).FirstOrDefault();
        }
    }
}
