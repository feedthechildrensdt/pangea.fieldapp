﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class FileTypeCollection : ObservableCollection<FileType>
    {
        public FileTypeCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new FileType(_item));
            }
        }

        public FileTypeCollection(IEnumerable<FileType> _items)
        {
            Clear();
            foreach (FileType item in _items)
            {
                Add(item);
            }
        }

        public void Add(IEnumerable<FileType> _items)
        {
            foreach (FileType item in _items)
            {
                Add(item);
            }
        }

        public FileType Find(FileType _item)
        {
            if (_item == null)
                return _item;

            return this.Where(ft => ft.CodeID == _item.CodeID).FirstOrDefault();
        }

        public FileType Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(ft => ft.CodeID == _codeID.Value).FirstOrDefault();
        }

        public FileType Find(string _fileType)
        {
            if (string.IsNullOrEmpty(_fileType) || string.IsNullOrWhiteSpace(_fileType))
                return null;

            return this.Where(ft => ft.Name.ToLower().Equals(_fileType.ToLower())).FirstOrDefault();
        }

        public FileType Find_Extension(string _fileExt)
        {
            if (string.IsNullOrEmpty(_fileExt) || string.IsNullOrWhiteSpace(_fileExt))
                return null;

            return this.Where(ft => ft.Extension.ToLower().Equals(_fileExt.ToLower())).FirstOrDefault();
        }
    }
}
