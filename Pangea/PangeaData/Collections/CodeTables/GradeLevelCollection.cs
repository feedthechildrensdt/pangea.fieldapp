﻿////////////////////////////////////////////////////////////////
/// Observable collection of the Grade data.  Used to store all
/// of the active Grade from the Grade Codes Database.
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{
    public class GradeLevelCollection : ObservableCollection<GradeLevel>
    {
        public GradeLevelCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new GradeLevel(_item));
            }
        }

        public GradeLevelCollection(IEnumerable<GradeLevel> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IEnumerable<GradeLevel> _items)
        {
            foreach (GradeLevel item in _items)
            {
                Add(item);
            }
        }

        public GradeLevelCollection Without(GradeLevel _item)
        {
            if (_item == null)
                return this;

            return new GradeLevelCollection(this.Where(c => c.CodeID != _item.CodeID));
        }

        public GradeLevelCollection Without(IEnumerable<GradeLevel> _items)
        {
            if (_items == null)
                return this;

            GradeLevelCollection _col = new GradeLevelCollection(this);
            foreach (GradeLevel _c in _items)
            {
                _col = new GradeLevelCollection(_col.Where(c => c.CodeID != _c.CodeID));
            }

            return _col;
        }

        // Bug 266 - Added to make sure only the item we send will show up in the collection.
        public GradeLevelCollection OnlyWith(GradeLevel _item)
        {
            if (_item == null)
                return this;

            return new GradeLevelCollection(this.Where(c => c.CodeID == _item.CodeID));
        }

        public GradeLevelCollection RestrictByAge(double _childAge)
        {
            return new GradeLevelCollection(this.Where(item => item.MinAge <= _childAge));
        }

        public GradeLevel Find(GradeLevel _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public GradeLevel Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(gl => gl.CodeID == _codeID.Value).FirstOrDefault();
        }

        public GradeLevel Find(string _gradeLevel)
        {
            if (string.IsNullOrEmpty(_gradeLevel) || string.IsNullOrWhiteSpace(_gradeLevel))
                return null;

            return this.Where(gl => gl.Name.ToLower().Equals(_gradeLevel.ToLower())).FirstOrDefault();
        }
    }
}
