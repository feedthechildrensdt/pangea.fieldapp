﻿////////////////////////////////////////////////////////////////
/// Observable collection of Location data.  Used to store all
/// of the active Locations from the Location Codes Database.
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

using Pangea.Data.Model.CodeTables;

namespace Pangea.Data.Collections
{

    public class LocationCollection : ObservableCollection<Location>
    {

        public LocationCollection(ObservableCollection<ObservableCollection<Object>> _items)
        {
            foreach (ObservableCollection<Object> _item in _items)
            {
                Add(new Location(_item));
            }
        }

        public LocationCollection(IEnumerable<Location> _items)
        {
            Clear();

            Add(_items);
        }

        public void Add(IEnumerable<Location> _items)
        {
            foreach (Location item in _items)
            {
                Add(item);
            }
        }

        public Location Find(Location _item)
        {
            if (_item == null)
                return _item;

            return this.Where(c => c.CodeID == _item.CodeID).FirstOrDefault();
        }

        public Location Find(int? _codeID)
        {
            if (_codeID == null)
                return null;

            return this.Where(l => l.CodeID == _codeID.Value).FirstOrDefault();
        }

        public Location Find(String _name)
        {
            if (String.IsNullOrEmpty(_name))
                return null;

            return this.Where(l => l.Name.ToLower().Equals(_name.ToLower())).FirstOrDefault();
        }

        public LocationCollection GetCountryLocations(int? _countryCodeID)
        {
            if (_countryCodeID == null)
                return this;

            return new LocationCollection(this.Where(l => l.CountryCodeID == _countryCodeID));
        }

    }
}
