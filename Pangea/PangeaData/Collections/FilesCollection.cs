﻿using Pangea.Data.Model.CodeTables;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Pangea.Data.Collections
{
    /// <summary>
    /// This will be the collection used to keep the Consent Form, Child Photo, and Action Photo Information
    /// This will also eventually keep any other Files that are needed.
    /// </summary>
    public class FilesCollection : ObservableCollection<PangeaFileStruct>
    {
        public FilesCollection()
        {  }

        public FilesCollection(IEnumerable<PangeaFileStruct> _items)
        {
            Clear();
            Add(_items);
        }

        public FilesCollection(IQueryable<PangeaFileStruct> _items)
        {
            Clear();
            Add(_items);
        }

        public void Add(IQueryable<PangeaFileStruct> _items)
        {
            foreach (PangeaFileStruct item in _items)
            {
                Add(item);
            }
        }

        public void Add(IEnumerable<PangeaFileStruct> _items)
        {
            foreach (PangeaFileStruct item in _items)
            {
                Add(item);
            }
        }

        public PangeaFileStruct Find(PangeaFileStruct item)
        {
            return this.Where(i => i == item).FirstOrDefault();
        }

        public IEnumerable<PangeaFileStruct> Find(String _shortFN = "", String _fileLoc = "")
        {
            if (!String.IsNullOrEmpty(_shortFN) && !String.IsNullOrWhiteSpace(_shortFN))
                return this.Where(i => i.ShortFileName.Equals(_shortFN));

            return this.Where(i => i.FullFileLoc.Equals(_fileLoc));
        }

        public IEnumerable<PangeaFileStruct> Find(ContentType _contentType)
        {
            return this.Where(i => i.ContentTypeInfo.CodeID == _contentType.CodeID);
        }

        public bool Contains(ContentType _contentType)
        {
            if (_contentType == null)
                return false;

            return this.Any(i => i.ContentTypeInfo.CodeID == _contentType.CodeID);
        }

        public void Remove(ContentType _contentType)
        {
            PangeaFileStruct item = Find(_contentType).FirstOrDefault();
            if (item != null)
                Remove(item);
        }

        public void Remove(IEnumerable<PangeaFileStruct> _remItems)
        {
            if (_remItems == null)
                return;

            foreach (PangeaFileStruct remItem in _remItems)
            {
                Remove(remItem);
            }
        }

        public void Replace(PangeaFileStruct _item)
        {
            if (_item == null)
                return;

            Remove(_item.ContentTypeInfo);

            Add(_item);
        }

        // Bug 371 - Adding way to get files that are optional
        public FilesCollection GetOptionalFiles(bool _optionalFile)
        {
            return new FilesCollection(this.Where(pfs => pfs.OptionalFile == _optionalFile));
        }
    }
}
