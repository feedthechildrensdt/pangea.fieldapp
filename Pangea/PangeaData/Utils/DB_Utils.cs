﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PangeaData.Utils
{
    public class DB_Utils
    {

        public static Model.DataTable_From_SPROC_Results DataTable_From_SPROC
            (
            string the_schema, 
            List<SqlParameter> l_SqlParameters, 
            string s_SPROC_Suffix_After_Period
            )
        {

            Model.DataTable_From_SPROC_Results the_Results = new Model.DataTable_From_SPROC_Results();

            the_Results.Operation_Completed_With_No_Errors = true;

            try
            {

                using (SqlConnection conn = new SqlConnection(Centraler.Database_Settingz.Currently_Used_DB_Conn_String_svc_admin_conn_LOCAL))
                {

                    conn.Open();

                    string sql_CMD = the_schema + "." + s_SPROC_Suffix_After_Period;

                    SqlCommand cmd = new SqlCommand(sql_CMD, conn);

                    cmd.CommandType = CommandType.StoredProcedure;

                    l_SqlParameters
                        .ForEach
                        (
                        xx => cmd.Parameters.Add(xx)
                        );

                    SqlDataAdapter da = new SqlDataAdapter(cmd);

                    da.Fill(the_Results.the_DataTable);

                    conn.Close();

                    da.Dispose();

                }
            }
            catch(Exception ex)
            {
                the_Results.Operation_Completed_With_No_Errors = false;

                the_Results.the_Exception = ex;

                throw;
            }
            finally
            {
                
            }

            return the_Results;
        }

    }
}
