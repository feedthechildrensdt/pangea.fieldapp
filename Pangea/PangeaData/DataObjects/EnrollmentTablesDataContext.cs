﻿////////////////////////////////////////////////////////////////////////////
/// This is the Data Context used to get the Enrollment Tables Data.
/// It is also used to add child information to the different enrollment Tables.
////////////////////////////////////////////////////////////////////////////

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Reflection;

namespace Pangea.Data.DataObjects
{
    // [Database(Name = "Pangea")]
    public partial class EnrollmentTablesDataContext : DataContext
    {
        private static CustomMappingSource mappingSource = new CustomMappingSource("Enrollment");

        #region Extensibility Method Definitions
        partial void OnCreated();
        partial void InsertChild(ChildDO instance);
        partial void UpdateChild(ChildDO instance);
        partial void DeleteChild(ChildDO instance);
        partial void InsertLocation_Livelihood(LocationLivelihoodDO instance);
        partial void UpdateLocation_Livelihood(LocationLivelihoodDO instance);
        partial void DeleteLocation_Livelihood(LocationLivelihoodDO instance);
        partial void InsertChild_Chore(ChildChoreDO instance);
        partial void UpdateChild_Chore(ChildChoreDO instance);
        partial void DeleteChild_Chore(ChildChoreDO instance);
        partial void InsertChild_Favorite_Activity(ChildFavoriteActivityDO instance);
        partial void UpdateChild_Favorite_Activity(ChildFavoriteActivityDO instance);
        partial void DeleteChild_Favorite_Activity(ChildFavoriteActivityDO instance);
        partial void InsertChild_Major_Life_Event(ChildMajorLifeEventDO instance);
        partial void UpdateChild_Major_Life_Event(ChildMajorLifeEventDO instance);
        partial void DeleteChild_Major_Life_Event(ChildMajorLifeEventDO instance);
        partial void InsertChild_Major_Life_Event_Lang(ChildMajorLifeEventLangDO instance);
        partial void UpdateChild_Major_Life_Event_Lang(ChildMajorLifeEventLangDO instance);
        partial void DeleteChild_Major_Life_Event_Lang(ChildMajorLifeEventLangDO instance);
        partial void InsertChild_Paragraph(ChildParagraphDO instance);
        partial void UpdateChild_Paragraph(ChildParagraphDO instance);
        partial void DeleteChild_Paragraph(ChildParagraphDO instance);
        partial void InsertChild_Paragraph_Lang(ChildParagraphLangDO instance);
        partial void UpdateChild_Paragraph_Lang(ChildParagraphLangDO instance);
        partial void DeleteChild_Paragraph_Lang(ChildParagraphLangDO instance);
        partial void InsertChild_Personality_Type(ChildPersonalityTypeDO instance);
        partial void UpdateChild_Personality_Type(ChildPersonalityTypeDO instance);
        partial void DeleteChild_Personality_Type(ChildPersonalityTypeDO instance);
        partial void InsertChild_Sponsorship(ChildSponsorshipDO instance);
        partial void UpdateChild_Sponsorship(ChildSponsorshipDO instance);
        partial void DeleteChild_Sponsorship(ChildSponsorshipDO instance);
        partial void InsertClient(ClientDO instance);
        partial void UpdateClient(ClientDO instance);
        partial void DeleteClient(ClientDO instance);
        partial void InsertConfig(ConfigDO instance);
        partial void UpdateConfig(ConfigDO instance);
        partial void DeleteConfig(ConfigDO instance);
        partial void InsertFile_Data(FileDataDO instance);
        partial void UpdateFile_Data(FileDataDO instance);
        partial void DeleteFile_Data(FileDataDO instance);
        partial void InsertLocation_Education(LocationEducationDO instance);
        partial void UpdateLocation_Education(LocationEducationDO instance);
        partial void DeleteLocation_Education(LocationEducationDO instance);
        partial void InsertLocation_Food_and_Nutrition(LocationFoodAndNutritionDO instance);
        partial void UpdateLocation_Food_and_Nutrition(LocationFoodAndNutritionDO instance);
        partial void DeleteLocation_Food_and_Nutrition(LocationFoodAndNutritionDO instance);
        partial void InsertLocation_Health_and_Water(LocationHealthAndWaterDO instance);
        partial void UpdateLocation_Health_and_Water(LocationHealthAndWaterDO instance);
        partial void DeleteLocation_Health_and_Water(LocationHealthAndWaterDO instance);
        #endregion

        public EnrollmentTablesDataContext(string connString) :
            base(connString, mappingSource)
        {
            OnCreated();
        }

        [Function(Name = "Enrollment.usp_Add_Child")]
        public ISingleResult<AddChildResult> Add_Child(
                    [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
                    [Parameter(Name = "First_Name", DbType = "NVarChar(50)")] string first_Name,
                    [Parameter(Name = "Last_Name", DbType = "NVarChar(50)")] string last_Name,
                    [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
                    //[Parameter(Name = "Child_Number", DbType = "BigInt")] long? child_Number,
                    [Parameter(Name = "Middle_Name", DbType = "NVarChar(50)")] string middle_Name,
                    [Parameter(Name = "Date_of_Birth", DbType = "DateTime")] DateTime? date_of_Birth,
                    [Parameter(Name = "Grade_Level_Code_ID", DbType = "Int")] int? grade_Level_Code_ID,
                    [Parameter(Name = "Health_Status_Code_ID", DbType = "Int")] int? health_Status_Code_ID,
                    [Parameter(Name = "Lives_With_Code_ID", DbType = "Int")] int? lives_With_Code_ID,
                    [Parameter(Name = "Favorite_Learning_Code_ID", DbType = "Int")] int? favorite_Learning_Code_ID,
                    [Parameter(Name = "Child_Record_Status_Code_ID", DbType = "Int")] int? child_Record_Status_Code_ID,
                    [Parameter(Name = "Child_Remove_Reason_Code_ID", DbType = "Int")] int? child_Remove_Reason_Code_ID,
                    [Parameter(Name = "Gender_Code_ID", DbType = "Int")] int? gender_Code_ID,
                    [Parameter(Name = "Number_Brothers", DbType = "TinyInt")] byte? number_Brothers,
                    [Parameter(Name = "Number_Sisters", DbType = "TinyInt")] byte? number_Sisters,
                    [Parameter(Name = "Disability_Status_Code_ID", DbType = "Bit")] bool? disability_Status_Code_ID,
                    [Parameter(Name = "Location_Code_ID", DbType = "Int")] int? location_Code_ID,
                    [Parameter(Name = "NickName", DbType = "NVarChar(50)")] string nickName)
        {

            ;

            var xx = (MethodInfo)(MethodInfo.GetCurrentMethod());

            ;

            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                first_Name, 
                last_Name, 
                child_ID, 
                middle_Name, 
                date_of_Birth, 
                grade_Level_Code_ID, 
                health_Status_Code_ID, 
                lives_With_Code_ID, 
                favorite_Learning_Code_ID, 
                child_Record_Status_Code_ID, 
                child_Remove_Reason_Code_ID, 
                gender_Code_ID, 
                number_Brothers, 
                number_Sisters, 
                disability_Status_Code_ID, 
                location_Code_ID, 
                nickName
                );

            return ((ISingleResult<AddChildResult>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Add_Child_Chore")]
        public int Add_Child_Chore(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date,
            [Parameter(Name = "Chore_Code_ID", DbType = "Int")] int? chore_Code_ID,
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID, 
                start_Date, 
                chore_Code_ID, 
                end_Date
                );

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Add_Child_Favorite_Activity")]
        public int Add_Child_Favorite_Activity(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date,
            [Parameter(Name = "Favorite_Activity_Code_ID", DbType = "Int")] int? favorite_Activity_Code_ID,
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            
            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID, 
                start_Date, 
                favorite_Activity_Code_ID, 
                end_Date
                );

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Add_Child_File")]
        public int Add_Child_File(
                    [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
                    [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
                    [Parameter(Name = "File", DbType = "VarBinary(MAX)")] Binary file,
                    [Parameter(Name = "FileName", DbType = "NVarChar(512)")] string fileName,
                    [Parameter(Name = "Content_Type_ID", DbType = "Int")] int? content_Type_ID,
                    [Parameter(Name = "File_Type_ID", DbType = "Int")] int? file_Type_ID,
                    [Parameter(Name = "Make", DbType = "NVarChar(250)")] string make,
                    [Parameter(Name = "Model", DbType = "NVarChar(250)")] string model,
                    [Parameter(Name = "Software", DbType = "NVarChar(250)")] string software,
                    [Parameter(Name = "DateTime", DbType = "DateTime")] DateTime? dateTime,
                    [Parameter(Name = "DateTimeOriginal", DbType = "DateTime")] DateTime? dateTimeOriginal,
                    [Parameter(Name = "DateTimeDigitized", DbType = "DateTime")] DateTime? dateTimeDigitized,
                    [Parameter(Name = "GPSVersionID", DbType = "NVarChar(13)")] string gPSVersionID,
                    [Parameter(Name = "GPSLatitudeRef", DbType = "NVarChar(50)")] string gPSLatitudeRef,
                    [Parameter(Name = "GPSLatitude", DbType = "NVarChar(69)")] string gPSLatitude,
                    [Parameter(Name = "GPSLongitudeRef", DbType = "NVarChar(50)")] string gPSLongitudeRef,
                    [Parameter(Name = "GPSLongitude", DbType = "NVarChar(69)")] string gPSLongitude,
                    [Parameter(Name = "GPSAltitudeRef", DbType = "NVarChar(8)")] string gPSAltitudeRef,
                    [Parameter(Name = "GPSAltitude", DbType = "NVarChar(64)")] string gPSAltitude,
                    [Parameter(Name = "GPSTimeStamp", DbType = "NVarChar(69)")] string gPSTimeStamp,
                    [Parameter(Name = "GPSImgDirectionRef", DbType = "NVarChar(10)")] string gPSImgDirectionRef,
                    [Parameter(Name = "GPSDateStamp", DbType = "NVarChar(69)")] string gPSDateStamp)
        {

            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID, 
                file, 
                fileName, 
                content_Type_ID, 
                file_Type_ID, 
                make, 
                model, 
                software, 
                dateTime, 
                dateTimeOriginal, 
                dateTimeDigitized, 
                gPSVersionID, 
                gPSLatitudeRef, 
                gPSLatitude, 
                gPSLongitudeRef, 
                gPSLongitude, 
                gPSAltitudeRef, 
                gPSAltitude, 
                gPSTimeStamp, 
                gPSImgDirectionRef, 
                gPSDateStamp
                );

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Add_Child_Non_Duplicates")]
        public int Add_Child_Non_Duplicates(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(Name = "Child_ID_1", DbType = "UniqueIdentifier")] Guid? child_ID_1, 
            [Parameter(Name = "Child_ID_2", DbType = "UniqueIdentifier")] Guid? child_ID_2)
        {

            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID_1, 
                child_ID_2
                );

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Add_Child_Paragraph")]
        public int Add_Child_Paragraph(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date,
            [Parameter(Name = "Language_Code_ID", DbType = "Int")] int? language_Code_ID,
            [Parameter(Name = "Paragraph_Description", DbType = "NVarChar(255)")] string paragraph_Description,
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {

            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID, 
                start_Date, 
                language_Code_ID, 
                paragraph_Description, 
                end_Date
                );

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Add_Child_Paragraph_Other_Lang")]
        public int Add_Child_Paragraph_Other_Lang(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date,
            [Parameter(Name = "Language_Code_ID", DbType = "Int")] int? language_Code_ID,
            [Parameter(Name = "Child_Paragraph", DbType = "Int")] int? child_Paragraph,
            [Parameter(Name = "Paragraph_Description", DbType = "NVarChar(255)")] string paragraph_Description,
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {

            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID, 
                start_Date, 
                language_Code_ID, 
                child_Paragraph, 
                paragraph_Description, 
                end_Date
                );

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Add_Child_Personality_Type")]
        public int Add_Child_Personality_Type(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "Start_Date", DbType = "DateTime")] DateTime? start_Date,
            [Parameter(Name = "Personality_Type_Code_ID", DbType = "Int")] int? personality_Type_Code_ID,
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {
            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID, 
                start_Date, 
                personality_Type_Code_ID, 
                end_Date
                );

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Clear_Child_Codes")]
        public int Clear_Child_Codes(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "End_Date", DbType = "DateTime")] DateTime? end_Date)
        {

            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID, 
                end_Date
                );

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Delete_Enrollment")]
        public int Delete_Enrollment(
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), child_ID, user_ID);

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Delete_File")]
        public int Delete_File(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID, 
            [Parameter(DbType = "UniqueIdentifier")] Guid? stream_id,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "File_Type_ID", DbType = "Int")] int? file_Type_ID, 
            [Parameter(Name = "Content_Type_ID", DbType = "Int")] int? content_Type_ID, 
            [Parameter(Name = "Current", DbType = "Bit")] bool? current)
        {

            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                stream_id, 
                child_ID, 
                file_Type_ID, 
                content_Type_ID, 
                current
                );

            return ((int)(result.ReturnValue));
        }

        [FunctionAttribute(Name = "Enrollment.usp_Enroll_Child")]
        public ISingleResult<AddChildResult> Enroll_Child
            (
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID
            )
        {

            ;

            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID.Value, child_ID.Value);

            // object the_result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);

            // IExecuteResult result = (IExecuteResult)the_result;

            ;

            // Guid gd_test = (Guid)result;

            ;

            ;

            return ((ISingleResult<AddChildResult>)(result.ReturnValue));
        }


        /*
        [FunctionAttribute(Name = "Enrollment.usp_Enroll_Child")]
        public int Enroll_Child2
            (
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID
            )
        {

            ;

            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID.Value, child_ID.Value);

            // object the_result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);

            // IExecuteResult result = (IExecuteResult)the_result;

            ;

            // Guid gd_test = (Guid)result;

            ;

            return ((int)(result.ReturnValue));
        }
        */



        [Function(Name = "Enrollment.usp_Get_A_Saved_Enrollment")]
        public ISingleResult<GetASavedResult> Get_A_Saved_Enrollment(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);

            return ((ISingleResult<GetASavedResult>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_Child_Non_Duplicates")]
        public ISingleResult<GetEnrollmentNonDuplicateResult> Get_Child_Non_Duplicates(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? Child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, Child_ID);

            return ((ISingleResult<GetEnrollmentNonDuplicateResult>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_Enrollment_Chores")]
        public ISingleResult<GetEnrollmentChoresResult> Get_Enrollment_Chores(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);

            return ((ISingleResult<GetEnrollmentChoresResult>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_Enrollment_Favorite_Activities")]
        public ISingleResult<GetEnrollmentFavoriteActivitiesResult> Get_Enrollment_Favorite_Activities(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);

            return ((ISingleResult<GetEnrollmentFavoriteActivitiesResult>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_Enrollment_Personality_Type")]
        public ISingleResult<GetEnrollmentPersonalityTypeResult> Get_Enrollment_Personality_Type(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_ID);

            return ((ISingleResult<GetEnrollmentPersonalityTypeResult>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_Enrollment_Previously_Transmitted")]
        public ISingleResult<ChildDO> Get_Enrollment_Previously_Transmitted([Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);

            return ((ISingleResult<ChildDO>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_Enrollment_Ready_To_Transmit")]
        public ISingleResult<ChildDO> Get_Enrollment_Ready_To_Transmit([Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);

            return ((ISingleResult<ChildDO>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_File")]
        public ISingleResult<GetFileResult> Get_File(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(DbType = "UniqueIdentifier")] Guid? stream_id,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "File_Type_ID", DbType = "Int")] int? file_Type_ID,
            [Parameter(Name = "Content_Type_ID", DbType = "Int")] int? content_Type_ID,
            [Parameter(Name = "Current", DbType = "Bit")] bool? current)
        {
            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                stream_id, 
                child_ID, 
                file_Type_ID, 
                content_Type_ID, 
                current
                );

            return ((ISingleResult<GetFileResult>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_File_Data")]
        public ISingleResult<GetFileDataResult> Get_File_Data(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(DbType = "UniqueIdentifier")] Guid? stream_id,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "File_Type_ID", DbType = "Int")] int? file_Type_ID,
            [Parameter(Name = "Content_Type_ID", DbType = "Int")] int? content_Type_ID,
            [Parameter(Name = "Current", DbType = "Bit")] bool? current)
        {
            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                stream_id, 
                child_ID, 
                file_Type_ID, 
                content_Type_ID, 
                current
                );

            return ((ISingleResult<GetFileDataResult>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_Possible_Child_Duplicates")]
        public ISingleResult<GetPossibleChildDuplicatesResult> Get_Possible_Child_Duplicates(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "First_Name", DbType = "NVarChar(50)")] string first_Name,
            [Parameter(Name = "Middle_Name", DbType = "NVarChar(50)")] string middle_Name,
            [Parameter(Name = "Last_Name", DbType = "NVarChar(50)")] string last_Name,
            [Parameter(Name = "Nickname", DbType = "NVarChar(50)")] string nickname,
            [Parameter(Name = "Date_Of_Birth", DbType = "DateTime")] DateTime? date_Of_Birth,
            [Parameter(Name = "Gender_Code_ID", DbType = "Int")] int? gender_Code_ID,
            [Parameter(Name = "Location_Code_ID", DbType = "Int")] int? location_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                child_ID, 
                user_ID, 
                first_Name, 
                middle_Name, 
                last_Name, 
                nickname, 
                date_Of_Birth, 
                gender_Code_ID, 
                location_Code_ID
                );

            return ((ISingleResult<GetPossibleChildDuplicatesResult>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Get_Saved_Enrollments")]
        public ISingleResult<ChildDO> Get_Saved_Enrollments(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);

            return ((ISingleResult<ChildDO>)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Update_Child_Action")]
        public int Update_Child_Action(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID,
            [Parameter(Name = "Action_ID", DbType = "Int")] int? action_ID)
        {
            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID, 
                action_ID
                );

            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Enrollment.usp_Update_Child_File_Action")]
        public int Update_Child_File_Action(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_ID", DbType = "UniqueIdentifier")] Guid? child_ID, 
            [Parameter(Name = "Stream_ID", DbType = "UniqueIdentifier")] Guid? stream_ID, 
            [Parameter(Name = "Action_ID", DbType = "Int")] int? action_ID)
        {
            IExecuteResult result = ExecuteMethodCall
                (
                this, 
                ((MethodInfo)(MethodInfo.GetCurrentMethod())), 
                user_ID, 
                child_ID, 
                stream_ID, 
                action_ID
                );

            return ((int)(result.ReturnValue));
        }
    }
}

