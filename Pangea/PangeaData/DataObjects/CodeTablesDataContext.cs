﻿////////////////////////////////////////////////////////////////////////////
/// This is the Data Context used to get the all of the Code Tables Data.
////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Reflection;

namespace Pangea.Data.DataObjects
{
    // [Database(Name = "Pangea")]
    public partial class CodeTablesDataContext : DataContext
    {
        private static MappingSource mappingSource = new AttributeMappingSource();

        #region Extensibility Method Definitions
        partial void OnCreated();
        #endregion

        public CodeTablesDataContext(string connString) :
            base(connString, mappingSource)
        {
            OnCreated();
        }

        ~CodeTablesDataContext()
        {
        }

        [Function(Name = "Code.usp_Add_Action")]
        public ISingleResult<AddCodesResult> Add_Action(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Action_Name", DbType = "NVarChar(100)")] string action_Name,
            [Parameter(Name = "Action_ID", DbType = "Int")] int? action_ID)
        {

            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, action_Name, action_ID);

            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));

        }

        [Function(Name = "Code.usp_Get_User_Location_Codes")]
        public void Get_User_Location_Codes(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
        }

        [Function(Name = "Code.usp_Add_Action_Reason")]
        public ISingleResult<AddCodesResult> Add_Action_Reason(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Action_Reason", DbType = "NVarChar(100)")] string action_Reason,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Action_Reason_ID", DbType = "Int")] int? action_Reason_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, action_Reason, description, action_Reason_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Child_Record_Status")]
        public ISingleResult<AddCodesResult> Add_Child_Record_Status(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_Record_Status", DbType = "NVarChar(50)")] string child_Record_Status,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Min_Age", DbType = "Int")] int? min_Age,
            [Parameter(Name = "Child_Record_Status_Code_ID", DbType = "Int")] int? child_Record_Status_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_Record_Status, description, public_Description, min_Age, child_Record_Status_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Child_Remove_Reason")]
        public ISingleResult<AddCodesResult> Add_Child_Remove_Reason(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_Remove_Reason", DbType = "NVarChar(50)")] string child_Remove_Reason,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Min_Age", DbType = "Int")] int? min_Age,
            [Parameter(Name = "Child_Remove_Reason_Code_ID", DbType = "Int")] int? child_Remove_Reason_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_Remove_Reason, description, public_Description, min_Age, child_Remove_Reason_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Chore")]
        public ISingleResult<AddCodesResult> Add_Chore(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Chore", DbType = "NVarChar(50)")] string chore,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Min_Age", DbType = "Int")] int? min_Age,
            [Parameter(Name = "Chore_Code_ID", DbType = "Int")] int? chore_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, chore, description, public_Description, min_Age, chore_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Column_Rule")]
        public ISingleResult<AddCodesResult> Add_Column_Rule(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Column_Rules_Name", DbType = "NVarChar(50)")] string column_Rules_Name,
            [Parameter(Name = "Table_Name", DbType = "NVarChar(100)")] string table_Name,
            [Parameter(Name = "Module_Name", DbType = "NVarChar(100)")] string module_Name,
            [Parameter(Name = "Column_Name", DbType = "NVarChar(100)")] string column_Name,
            [Parameter(Name = "Default_String_Value", DbType = "NVarChar(250)")] string default_String_Value,
            [Parameter(Name = "Default_int_Value", DbType = "BigInt")] long? default_int_Value,
            [Parameter(Name = "Min_int_Allowed_Value", DbType = "BigInt")] long? min_int_Allowed_Value,
            [Parameter(Name = "Max_int_Allowed_Value", DbType = "BigInt")] long? max_int_Allowed_Value,
            [Parameter(Name = "Required", DbType = "Bit")] bool? required,
            [Parameter(Name = "Validation_Type", DbType = "NVarChar(255)")] string validation_Type,
            [Parameter(Name = "Column_Rules_Code_ID", DbType = "Int")] int? column_Rules_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, column_Rules_Name, table_Name, module_Name, column_Name, default_String_Value, default_int_Value, min_int_Allowed_Value, max_int_Allowed_Value, required, validation_Type, column_Rules_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Content_Type")]
        public ISingleResult<AddCodesResult> Add_Content_Type(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Content_Type", DbType = "NVarChar(50)")] string content_Type,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Required", DbType = "Bit")] bool? required,
            [Parameter(Name = "Content_Type_Code_ID", DbType = "Int")] int? content_Type_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, content_Type, description, public_Description, required, content_Type_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Country")]
        public ISingleResult<AddCodesResult> Add_Country(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Country_Code", DbType = "NVarChar(2)")] string country_Code,
            [Parameter(Name = "Region_Code_ID", DbType = "Int")] int? region_Code_ID,
            [Parameter(Name = "Country_Name", DbType = "NVarChar(50)")] string country_Name,
            [Parameter(Name = "Default_Language", DbType = "Int")] int? default_Language,
            [Parameter(Name = "Min_School_Age", DbType = "Int")] int? min_School_Age,
            [Parameter(Name = "Country_Code_ID", DbType = "Int")] int? country_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, country_Code, region_Code_ID, country_Name, default_Language, min_School_Age, country_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Favorite_Activity")]
        public ISingleResult<AddCodesResult> Add_Favorite_Activity(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Favorite_Activity", DbType = "NVarChar(50)")] string favorite_Activity,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Min_Age", DbType = "Int")] int? min_Age,
            [Parameter(Name = "Favorite_Activity_Code_ID", DbType = "Int")] int? favorite_Activity_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, favorite_Activity, description, public_Description, min_Age, favorite_Activity_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Favorite_Learning")]
        public ISingleResult<AddCodesResult> Add_Favorite_Learning(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Favorite_Learning", DbType = "NVarChar(50)")] string favorite_Learning,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Min_Age", DbType = "Int")] int? min_Age,
            [Parameter(Name = "Favorite_Learning_Code_ID", DbType = "Int")] int? favorite_Learning_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, favorite_Learning, description, public_Description, min_Age, favorite_Learning_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_File_Type")]
        public ISingleResult<AddCodesResult> Add_File_Type(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "File_Type", DbType = "NVarChar(50)")] string file_Type,
            [Parameter(Name = "File_Description", DbType = "NVarChar(255)")] string file_Description,
            [Parameter(Name = "File_Extension", DbType = "NVarChar(50)")] string file_Extension,
            [Parameter(Name = "File_Type_Code_ID", DbType = "Int")] int? file_Type_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, file_Type, file_Description, file_Extension, file_Type_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Gender")]
        public ISingleResult<AddCodesResult> Add_Gender(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Gender", DbType = "NVarChar(50)")] string gender,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Gender_Code_ID", DbType = "Int")] int? gender_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, gender, description, public_Description, gender_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Grade_Level")]
        public ISingleResult<AddCodesResult> Add_Grade_Level(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Grade_Level", DbType = "NVarChar(50)")] string grade_Level,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Min_Age", DbType = "Int")] int? min_Age,
            [Parameter(Name = "Grade_Level_Code_ID", DbType = "Int")] int? grade_Level_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, grade_Level, description, public_Description, min_Age, grade_Level_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Health_Status")]
        public ISingleResult<AddCodesResult> Add_Health_Status(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Health_Status", DbType = "NVarChar(50)")] string health_Status,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Min_Age", DbType = "Int")] int? min_Age,
            [Parameter(Name = "Health_Status_Code_ID", DbType = "Int")] int? health_Status_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, health_Status, description, public_Description, min_Age, health_Status_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Language")]
        public ISingleResult<AddCodesResult> Add_Language(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Language_Code", DbType = "NVarChar(5)")] string language_Code,
            [Parameter(Name = "Language_Desc", DbType = "NVarChar(255)")] string language_Desc,
            [Parameter(Name = "Language_Code_ID", DbType = "Int")] int? language_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, language_Code, language_Desc, language_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Lives_With")]
        public ISingleResult<AddCodesResult> Add_Lives_With(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Lives_With", DbType = "NVarChar(50)")] string lives_With,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Min_Age", DbType = "Int")] int? min_Age,
            [Parameter(Name = "Lives_With_Code_ID", DbType = "Int")] int? lives_With_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, lives_With, description, public_Description, min_Age, lives_With_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Location")]
        public ISingleResult<AddCodesResult> Add_Location(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Location_Name", DbType = "NVarChar(255)")] string location_Name,
            [Parameter(Name = "Country_Code_ID", DbType = "Int")] int? country_Code_ID,
            [Parameter(Name = "Default_Language", DbType = "Int")] int? default_Language,
            [Parameter(Name = "Sponsorship_Site", DbType = "Bit")] bool? sponsorship_Site,
            [Parameter(Name = "Location_Code_ID", DbType = "Int")] int? location_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, location_Name, country_Code_ID, default_Language, sponsorship_Site, location_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Add_Personality_Type")]
        public ISingleResult<AddCodesResult> Add_Personality_Type(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Personality_Type", DbType = "NVarChar(50)")] string personality_Type,
            [Parameter(Name = "Description", DbType = "NVarChar(255)")] string description,
            [Parameter(Name = "Public_Description", DbType = "NVarChar(255)")] string public_Description,
            [Parameter(Name = "Min_Age", DbType = "Int")] int? min_Age,
            [Parameter(Name = "Personality_Type_Code_ID", DbType = "Int")] int? personality_Type_Code_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, personality_Type, description, public_Description, min_Age, personality_Type_Code_ID);
            return ((ISingleResult<AddCodesResult>)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Chores")]
        public int Deactivate_All_Chores(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Column_Rules")]
        public int Deactivate_All_Column_Rules(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Content_Type")]
        public int Deactivate_All_Content_Type(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Country")]
        public int Deactivate_All_Country(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Favorite_Activity")]
        public int Deactivate_All_Favorite_Activity(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Favorite_Learning")]
        public int Deactivate_All_Favorite_Learning(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_File_Type")]
        public int Deactivate_All_File_Type(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Gender")]
        public int Deactivate_All_Gender(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Grade_Level")]
        public int Deactivate_All_Grade_Level(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Health_Status")]
        public int Deactivate_All_Health_Status(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Language")]
        public int Deactivate_All_Language(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Lives_With")]
        public int Deactivate_All_Lives_With(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_All_Location")]
        public int Deactivate_All_Location(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_Child_Record_Status")]
        public int Deactivate_Child_Record_Status(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_Child_Remove_Reason")]
        public int Deactivate_Child_Remove_Reason(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_Personality_Type")]
        public int Deactivate_Personality_Type(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Deactivate_Region")]
        public int Deactivate_Region(
            [Parameter(DbType = "UniqueIdentifier")] Guid? user_ID)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID);
            return ((int)(result.ReturnValue));
        }

        [Function(Name = "Code.usp_Delete_Child_Record_Status_Code")]
        public int Delete_Child_Record_Status_Code(
            [Parameter(Name = "User_ID", DbType = "UniqueIdentifier")] Guid? user_ID,
            [Parameter(Name = "Child_Record_Status", DbType = "NVarChar(50)")] string child_Record_Status)
        {
            IExecuteResult result = ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), user_ID, child_Record_Status);
            return ((int)(result.ReturnValue));
        }

        /// <summary>
        /// Retrieves all the Code Tables information int a Dictionary.
        /// </summary>
        /// <param name="user_ID">The User asking for the Tables.</param>
        /// <returns>All the Code Table Values.</returns>
        public Dictionary<String, ObservableCollection<ObservableCollection<Object>>> Get_Code_Tables(Guid? user_ID)
        {
            return Get_Code_Tables_Handler(user_ID, false);
        }

        public Dictionary<String, ObservableCollection<ObservableCollection<Object>>> Get_Code_Tables_With_Just_Active_Countries_And_Locations(Guid? user_ID)
        {
            return Get_Code_Tables_Handler(user_ID, true);
        }

        private static Dictionary<string, ObservableCollection<ObservableCollection<object>>> Get_Code_Tables_Handler
            (
            Guid? user_ID,
            bool Add_Just_Active_Countries_And_Locations
            )
        {

            Dictionary<String, ObservableCollection<ObservableCollection<Object>>> retVal = new Dictionary<String, ObservableCollection<ObservableCollection<Object>>>();

            ObservableCollection<String> _tableNames = new ObservableCollection<String>();

            ObservableCollection<Object> _items;

            using (SqlConnection conn = new SqlConnection(PangeaInfo.DBCon))
            {
                using (SqlCommand cmd = new SqlCommand("[Code].[usp_Get_Codes_Tables]", conn))
                {

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@User_ID", user_ID.ToString());

                    SqlDataAdapter adptr = new SqlDataAdapter();

                    adptr.SelectCommand = cmd;

                    conn.Open();

                    DataSet codeTables = new DataSet();

                    adptr.Fill(codeTables);

                    DataRowCollection tblNames = codeTables.Tables[0].Rows;

                    for (int tbl = 1; tbl < codeTables.Tables.Count; tbl++)
                    {
                        _tableNames.Add(tblNames[tbl - 1][0].ToString());

                        retVal.Add(tblNames[tbl - 1][0].ToString(), new ObservableCollection<ObservableCollection<Object>>());

                        string s_temp = tblNames[tbl - 1][0].ToString();

                        if
                            (
                            tblNames[tbl - 1][0].ToString() == "Country" ||
                            tblNames[tbl - 1][0].ToString() == "Location"
                            )
                        {

                            SqlCommand RetrieveCmd = new SqlCommand("[Code].[usp_Get_Country_Codes]", conn);

                            if (tblNames[tbl - 1][0].ToString() == "Location")
                                RetrieveCmd = new SqlCommand("[Code].[usp_Get_Location_Codes]", conn);

                            RetrieveCmd.CommandType = CommandType.StoredProcedure;

                            RetrieveCmd.Parameters.AddWithValue("@User_ID", user_ID.ToString());

                            SqlDataAdapter Adapter = new SqlDataAdapter();

                            Adapter.SelectCommand = RetrieveCmd;

                            DataSet table_data = new DataSet();

                            Adapter.Fill(table_data);

                            for (int rows = 0; rows < table_data.Tables[0].Rows.Count; rows++)
                            {

                                DataRow the_Data = table_data.Tables[0].Rows[rows];

                                bool add_record = false;

                                bool Is_Active = (bool)the_Data["Active"];

                                if
                                    (
                                    Add_Just_Active_Countries_And_Locations &&
                                    Is_Active == true
                                    )
                                {
                                    add_record = true;
                                }
                                else if (!Add_Just_Active_Countries_And_Locations)
                                {
                                    add_record = true;
                                }

                                if (add_record)
                                {

                                    _items = new ObservableCollection<Object>();

                                    for (int rowData = 0; rowData < table_data.Tables[0].Rows[rows].ItemArray.Length; rowData++)
                                    {
                                        _items.Add(table_data.Tables[0].Rows[rows].ItemArray[rowData]);
                                    }

                                    retVal[tblNames[tbl - 1][0].ToString()].Add(_items);

                                }

                            }
                        }
                        else
                        {
                            DataRowCollection tblData = codeTables.Tables[tbl].Rows;
                            for (int rows = 0; rows < tblData.Count; rows++)
                            {
                                _items = new ObservableCollection<Object>();
                                for (int rowData = 0; rowData < tblData[rows].ItemArray.Length; rowData++)
                                {
                                    _items.Add(tblData[rows][rowData]);
                                }
                                retVal[tblNames[tbl - 1][0].ToString()].Add(_items);
                            }
                        }
                    }

                    conn.Close();
                }
            }

            return retVal;
        }
    }
}