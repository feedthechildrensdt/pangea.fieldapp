﻿using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    ////////////////////////////////////////////////////////////////////////////////
    /// This is the Child Favorite Activities Information for a saved enrollment child.
    ////////////////////////////////////////////////////////////////////////////////
    public partial class GetEnrollmentFavoriteActivitiesResult
    {
        private int _Favorite_Activity_Code_ID;

        public GetEnrollmentFavoriteActivitiesResult()
        {
        }

        [Column(Storage = "_Favorite_Activity_Code_ID", DbType = "Int NOT NULL")]
        public int Favorite_Activity_Code_ID
        {
            get { return _Favorite_Activity_Code_ID; }
            set
            {
                if ((_Favorite_Activity_Code_ID != value))
                {
                    _Favorite_Activity_Code_ID = value;
                }
            }
        }
    }
}
