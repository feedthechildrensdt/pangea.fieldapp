﻿using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    public partial class GetDBVersionResult
    {
        private int _Major;

        private int _Minor;

        private int _Build;

        private int _Revision;

        private int _Min_App_Major;

        private int _Min_App_Minor;

        private int _Min_App_Build;

        private int _Min_App_Revision;

        private int _Max_App_Major;

        private int _Max_App_Minor;

        private int _Max_App_Build;

        private int _Max_App_Revision;

        private string _SQL_Update_Script;

        private string _Release_Notes;

        public GetDBVersionResult()
        {
        }

        [Column(Storage = "_Major", DbType = "Int NOT NULL")]
        public int Major
        {
            get
            {
                return _Major;
            }
            set
            {
                if ((_Major != value))
                {
                    _Major = value;
                }
            }
        }

        [Column(Storage = "_Minor", DbType = "Int NOT NULL")]
        public int Minor
        {
            get
            {
                return _Minor;
            }
            set
            {
                if ((_Minor != value))
                {
                    _Minor = value;
                }
            }
        }

        [Column(Storage = "_Build", DbType = "Int NOT NULL")]
        public int Build
        {
            get
            {
                return _Build;
            }
            set
            {
                if ((_Build != value))
                {
                    _Build = value;
                }
            }
        }

        [Column(Storage = "_Revision", DbType = "Int NOT NULL")]
        public int Revision
        {
            get
            {
                return _Revision;
            }
            set
            {
                if ((_Revision != value))
                {
                    _Revision = value;
                }
            }
        }

        [Column(Storage = "_Min_App_Major", DbType = "Int NOT NULL")]
        public int Min_App_Major
        {
            get
            {
                return _Min_App_Major;
            }
            set
            {
                if ((_Min_App_Major != value))
                {
                    _Min_App_Major = value;
                }
            }
        }

        [Column(Storage = "_Min_App_Minor", DbType = "Int NOT NULL")]
        public int Min_App_Minor
        {
            get
            {
                return _Min_App_Minor;
            }
            set
            {
                if ((_Min_App_Minor != value))
                {
                    _Min_App_Minor = value;
                }
            }
        }

        [Column(Storage = "_Min_App_Build", DbType = "Int NOT NULL")]
        public int Min_App_Build
        {
            get
            {
                return _Min_App_Build;
            }
            set
            {
                if ((_Min_App_Build != value))
                {
                    _Min_App_Build = value;
                }
            }
        }

        [Column(Storage = "_Min_App_Revision", DbType = "Int NOT NULL")]
        public int Min_App_Revision
        {
            get
            {
                return _Min_App_Revision;
            }
            set
            {
                if ((_Min_App_Revision != value))
                {
                    _Min_App_Revision = value;
                }
            }
        }

        [Column(Storage = "_Max_App_Major", DbType = "Int NOT NULL")]
        public int Max_App_Major
        {
            get
            {
                return _Max_App_Major;
            }
            set
            {
                if ((_Max_App_Major != value))
                {
                    _Max_App_Major = value;
                }
            }
        }

        [Column(Storage = "_Max_App_Minor", DbType = "Int NOT NULL")]
        public int Max_App_Minor
        {
            get
            {
                return _Max_App_Minor;
            }
            set
            {
                if ((_Max_App_Minor != value))
                {
                    _Max_App_Minor = value;
                }
            }
        }

        [Column(Storage = "_Max_App_Build", DbType = "Int NOT NULL")]
        public int Max_App_Build
        {
            get
            {
                return _Max_App_Build;
            }
            set
            {
                if ((_Max_App_Build != value))
                {
                    _Max_App_Build = value;
                }
            }
        }

        [Column(Storage = "_Max_App_Revision", DbType = "Int NOT NULL")]
        public int Max_App_Revision
        {
            get
            {
                return _Max_App_Revision;
            }
            set
            {
                if ((_Max_App_Revision != value))
                {
                    _Max_App_Revision = value;
                }
            }
        }

        [Column(Storage = "_SQL_Update_Script", DbType = "NVarChar(MAX)")]
        public string SQL_Update_Script
        {
            get
            {
                return _SQL_Update_Script;
            }
            set
            {
                if ((_SQL_Update_Script != value))
                {
                    _SQL_Update_Script = value;
                }
            }
        }

        [Column(Storage = "_Release_Notes", DbType = "NVarChar(MAX)")]
        public string Release_Notes
        {
            get
            {
                return _Release_Notes;
            }
            set
            {
                if ((_Release_Notes != value))
                {
                    _Release_Notes = value;
                }
            }
        }
    }
}
