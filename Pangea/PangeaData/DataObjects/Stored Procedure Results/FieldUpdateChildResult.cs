﻿using System;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    public partial class FieldUpdateChildResult
    {

        private long? _TSLONG;

        private long? _TSMAN;

        private bool? _Active;

        private DateTime? _BlackBaud_Uploaded;

        private long _ID;

        private Guid _Child_ID;

        private long? _Child_Number;

        private string _First_Name;

        private string _Middle_Name;

        private string _Last_Name;

        private DateTime? _Date_of_Birth;

        private string _Grade_Level;

        private string _Health_Status;

        private string _Lives_With;

        private string _Favorite_Learning;

        private string _Child_Record_Status;

        private string _Child_Remove_Reason;

        private string _Gender;

        private string _Action_Name;

        public FieldUpdateChildResult()
        {
        }

        [Column(Storage = "_TSLONG", DbType = "BigInt")]
        public long? TSLONG
        {
            get
            {
                return _TSLONG;
            }
            set
            {
                if ((_TSLONG != value))
                {
                    _TSLONG = value;
                }
            }
        }

        [Column(Storage = "_TSMAN", DbType = "BigInt")]
        public long? TSMAN
        {
            get
            {
                return _TSMAN;
            }
            set
            {
                if ((_TSMAN != value))
                {
                    _TSMAN = value;
                }
            }
        }

        [Column(Storage = "_Active", DbType = "Bit")]
        public bool? Active
        {
            get
            {
                return _Active;
            }
            set
            {
                if ((_Active != value))
                {
                    _Active = value;
                }
            }
        }

        [Column(Storage = "_BlackBaud_Uploaded", DbType = "DateTime")]
        public DateTime? BlackBaud_Uploaded
        {
            get
            {
                return _BlackBaud_Uploaded;
            }
            set
            {
                if ((_BlackBaud_Uploaded != value))
                {
                    _BlackBaud_Uploaded = value;
                }
            }
        }

        [Column(Storage = "_ID", DbType = "BigInt NOT NULL")]
        public long ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if ((_ID != value))
                {
                    _ID = value;
                }
            }
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL")]
        public Guid Child_ID
        {
            get
            {
                return _Child_ID;
            }
            set
            {
                if ((_Child_ID != value))
                {
                    _Child_ID = value;
                }
            }
        }

        [Column(Storage = "_Child_Number", DbType = "BigInt")]
        public long? Child_Number
        {
            get
            {
                return _Child_Number;
            }
            set
            {
                if ((_Child_Number != value))
                {
                    _Child_Number = value;
                }
            }
        }

        [Column(Storage = "_First_Name", DbType = "NVarChar(50)")]
        public string First_Name
        {
            get
            {
                return _First_Name;
            }
            set
            {
                if ((_First_Name != value))
                {
                    _First_Name = value;
                }
            }
        }

        [Column(Storage = "_Middle_Name", DbType = "NVarChar(50)")]
        public string Middle_Name
        {
            get
            {
                return _Middle_Name;
            }
            set
            {
                if ((_Middle_Name != value))
                {
                    _Middle_Name = value;
                }
            }
        }

        [Column(Storage = "_Last_Name", DbType = "NVarChar(50)")]
        public string Last_Name
        {
            get
            {
                return _Last_Name;
            }
            set
            {
                if ((_Last_Name != value))
                {
                    _Last_Name = value;
                }
            }
        }

        [Column(Storage = "_Date_of_Birth", DbType = "DateTime")]
        public DateTime? Date_of_Birth
        {
            get
            {
                return _Date_of_Birth;
            }
            set
            {
                if ((_Date_of_Birth != value))
                {
                    _Date_of_Birth = value;
                }
            }
        }

        [Column(Storage = "_Grade_Level", DbType = "NVarChar(50)")]
        public string Grade_Level
        {
            get
            {
                return _Grade_Level;
            }
            set
            {
                if ((_Grade_Level != value))
                {
                    _Grade_Level = value;
                }
            }
        }

        [Column(Storage = "_Health_Status", DbType = "NVarChar(50)")]
        public string Health_Status
        {
            get
            {
                return _Health_Status;
            }
            set
            {
                if ((_Health_Status != value))
                {
                    _Health_Status = value;
                }
            }
        }

        [Column(Storage = "_Lives_With", DbType = "NVarChar(50)")]
        public string Lives_With
        {
            get
            {
                return _Lives_With;
            }
            set
            {
                if ((_Lives_With != value))
                {
                    _Lives_With = value;
                }
            }
        }

        [Column(Storage = "_Favorite_Learning", DbType = "NVarChar(50)")]
        public string Favorite_Learning
        {
            get
            {
                return _Favorite_Learning;
            }
            set
            {
                if ((_Favorite_Learning != value))
                {
                    _Favorite_Learning = value;
                }
            }
        }

        [Column(Storage = "_Child_Record_Status", DbType = "NVarChar(50)")]
        public string Child_Record_Status
        {
            get
            {
                return _Child_Record_Status;
            }
            set
            {
                if ((_Child_Record_Status != value))
                {
                    _Child_Record_Status = value;
                }
            }
        }

        [Column(Storage = "_Child_Remove_Reason", DbType = "NVarChar(50)")]
        public string Child_Remove_Reason
        {
            get
            {
                return _Child_Remove_Reason;
            }
            set
            {
                if ((_Child_Remove_Reason != value))
                {
                    _Child_Remove_Reason = value;
                }
            }
        }

        [Column(Storage = "_Gender", DbType = "NVarChar(50)")]
        public string Gender
        {
            get
            {
                return _Gender;
            }
            set
            {
                if ((_Gender != value))
                {
                    _Gender = value;
                }
            }
        }

        [Column(Storage = "_Action_Name", DbType = "NVarChar(50) NOT NULL", CanBeNull = false)]
        public string Action_Name
        {
            get
            {
                return _Action_Name;
            }
            set
            {
                if ((_Action_Name != value))
                {
                    _Action_Name = value;
                }
            }
        }
    }
}
