﻿////////////////////////////////////////////////////////////////////////////////
/// This is the Object use for representing the Enrollment.Config
/// Database Table.  This was creating by Linq to SQL
////////////////////////////////////////////////////////////////////////////////

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    [Table(Name = "[Schema].Config")]
    public partial class ConfigDO
    {
        private bool? _Active;

        private int _Action_ID;

        private Guid _Action_User;

        private DateTime _Action_Date;

        private int? _Action_Reason;

        private Binary _TS;

        private long? _TSLONG;

        private long? _TSMAN;

        private int _Config_ID;

        private string _Name;

        private string _Description;

        private bool? _Bit_Value;

        private int? _Int_Value;

        private string _Text_Value;

        private Nullable<decimal> _Dec_Value;

        private Nullable<DateTime> _Datetime_Value;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(ChangeAction action);
        partial void OnCreated();
        partial void OnActiveChanging(bool? value);
        partial void OnActiveChanged();
        partial void OnAction_IDChanging(int value);
        partial void OnAction_IDChanged();
        partial void OnAction_UserChanging(Guid value);
        partial void OnAction_UserChanged();
        partial void OnAction_DateChanging(DateTime value);
        partial void OnAction_DateChanged();
        partial void OnAction_ReasonChanging(int? value);
        partial void OnAction_ReasonChanged();
        partial void OnTSChanging(Binary value);
        partial void OnTSChanged();
        partial void OnTSLONGChanging(long? value);
        partial void OnTSLONGChanged();
        partial void OnTSMANChanging(long? value);
        partial void OnTSMANChanged();
        partial void OnConfig_IDChanging(int value);
        partial void OnConfig_IDChanged();
        partial void OnNameChanging(string value);
        partial void OnNameChanged();
        partial void OnDescriptionChanging(string value);
        partial void OnDescriptionChanged();
        partial void OnBit_ValueChanging(bool? value);
        partial void OnBit_ValueChanged();
        partial void OnInt_ValueChanging(int? value);
        partial void OnInt_ValueChanged();
        partial void OnText_ValueChanging(string value);
        partial void OnText_ValueChanged();
        partial void OnDec_ValueChanging(Nullable<decimal> value);
        partial void OnDec_ValueChanged();
        partial void OnDatetime_ValueChanging(Nullable<DateTime> value);
        partial void OnDatetime_ValueChanged();
        #endregion

        public ConfigDO()
        {
            OnCreated();
        }

        [Column(Storage = "_Active", DbType = "Bit", UpdateCheck = UpdateCheck.Never)]
        public bool? Active
        {
            get
            {
                return _Active;
            }
            set
            {
                if ((_Active != value))
                {
                    OnActiveChanging(value);
                    _Active = value;
                    OnActiveChanged();
                }
            }
        }

        [Column(Storage = "_Action_ID", DbType = "Int NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public int Action_ID
        {
            get
            {
                return _Action_ID;
            }
            set
            {
                if ((_Action_ID != value))
                {
                    OnAction_IDChanging(value);
                    _Action_ID = value;
                    OnAction_IDChanged();
                }
            }
        }

        [Column(Storage = "_Action_User", DbType = "UniqueIdentifier NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public Guid Action_User
        {
            get
            {
                return _Action_User;
            }
            set
            {
                if ((_Action_User != value))
                {
                    OnAction_UserChanging(value);
                    _Action_User = value;
                    OnAction_UserChanged();
                }
            }
        }

        [Column(Storage = "_Action_Date", DbType = "DateTime NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public DateTime Action_Date
        {
            get
            {
                return _Action_Date;
            }
            set
            {
                if ((_Action_Date != value))
                {
                    OnAction_DateChanging(value);
                    _Action_Date = value;
                    OnAction_DateChanged();
                }
            }
        }

        [Column(Storage = "_Action_Reason", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Action_Reason
        {
            get
            {
                return _Action_Reason;
            }
            set
            {
                if ((_Action_Reason != value))
                {
                    OnAction_ReasonChanging(value);
                    _Action_Reason = value;
                    OnAction_ReasonChanged();
                }
            }
        }

        [Column(Storage = "_TS", AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)]
        public Binary TS
        {
            get
            {
                return _TS;
            }
            set
            {
                if ((_TS != value))
                {
                    OnTSChanging(value);
                    _TS = value;
                    OnTSChanged();
                }
            }
        }

        [Column(Storage = "_TSLONG", AutoSync = AutoSync.Always, DbType = "BigInt", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public long? TSLONG
        {
            get
            {
                return _TSLONG;
            }
            set
            {
                if ((_TSLONG != value))
                {
                    OnTSLONGChanging(value);
                    _TSLONG = value;
                    OnTSLONGChanged();
                }
            }
        }

        [Column(Storage = "_TSMAN", DbType = "BigInt", UpdateCheck = UpdateCheck.Never)]
        public long? TSMAN
        {
            get
            {
                return _TSMAN;
            }
            set
            {
                if ((_TSMAN != value))
                {
                    OnTSMANChanging(value);
                    _TSMAN = value;
                    OnTSMANChanged();
                }
            }
        }

        [Column(Storage = "_Config_ID", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public int Config_ID
        {
            get
            {
                return _Config_ID;
            }
            set
            {
                if ((_Config_ID != value))
                {
                    OnConfig_IDChanging(value);
                    _Config_ID = value;
                    OnConfig_IDChanged();
                }
            }
        }

        [Column(Storage = "_Name", DbType = "NVarChar(50) NOT NULL", CanBeNull = false, UpdateCheck = UpdateCheck.Never)]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if ((_Name != value))
                {
                    OnNameChanging(value);
                    _Name = value;
                    OnNameChanged();
                }
            }
        }

        [Column(Storage = "_Description", DbType = "NVarChar(255)", UpdateCheck = UpdateCheck.Never)]
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if ((_Description != value))
                {
                    OnDescriptionChanging(value);
                    _Description = value;
                    OnDescriptionChanged();
                }
            }
        }

        [Column(Storage = "_Bit_Value", DbType = "Bit", UpdateCheck = UpdateCheck.Never)]
        public bool? Bit_Value
        {
            get
            {
                return _Bit_Value;
            }
            set
            {
                if ((_Bit_Value != value))
                {
                    OnBit_ValueChanging(value);
                    _Bit_Value = value;
                    OnBit_ValueChanged();
                }
            }
        }

        [Column(Storage = "_Int_Value", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Int_Value
        {
            get
            {
                return _Int_Value;
            }
            set
            {
                if ((_Int_Value != value))
                {
                    OnInt_ValueChanging(value);
                    _Int_Value = value;
                    OnInt_ValueChanged();
                }
            }
        }

        [Column(Storage = "_Text_Value", DbType = "NVarChar(255)", UpdateCheck = UpdateCheck.Never)]
        public string Text_Value
        {
            get
            {
                return _Text_Value;
            }
            set
            {
                if ((_Text_Value != value))
                {
                    OnText_ValueChanging(value);
                    _Text_Value = value;
                    OnText_ValueChanged();
                }
            }
        }

        [Column(Storage = "_Dec_Value", DbType = "Decimal(18,0)", UpdateCheck = UpdateCheck.Never)]
        public Nullable<decimal> Dec_Value
        {
            get
            {
                return _Dec_Value;
            }
            set
            {
                if ((_Dec_Value != value))
                {
                    OnDec_ValueChanging(value);
                    _Dec_Value = value;
                    OnDec_ValueChanged();
                }
            }
        }

        [Column(Storage = "_Datetime_Value", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public Nullable<DateTime> Datetime_Value
        {
            get
            {
                return _Datetime_Value;
            }
            set
            {
                if ((_Datetime_Value != value))
                {
                    OnDatetime_ValueChanging(value);
                    _Datetime_Value = value;
                    OnDatetime_ValueChanged();
                }
            }
        }
    }
}
