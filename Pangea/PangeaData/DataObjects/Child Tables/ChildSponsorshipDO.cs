﻿////////////////////////////////////////////////////////////////////////////////
/// This is the Object use for representing the Enrollment.Child_Sponsorship
/// Database Table.  This was creating by Linq to SQL
////////////////////////////////////////////////////////////////////////////////

using System;
using System.Data.Linq;
using System.Data.Linq.Mapping;

namespace Pangea.Data.DataObjects
{
    [Table(Name = "[Schema].Child_Sponsorship")]
    public partial class ChildSponsorshipDO
    {
        private bool? _Active;

        private int _Action_ID;

        private Guid _Action_User;

        private DateTime _Action_Date;

        private int? _Action_Reason;

        private Binary _TS;

        private long? _TSLONG;

        private long? _TSMAN;

        private int _Child_Sponsorship_ID;

        private Guid _Child_ID;

        private Guid _Sponsor_ID;

        private DateTime? _Start_Date;

        private DateTime? _End_Date;

        #region Extensibility Method Definitions
        partial void OnLoaded();
        partial void OnValidate(ChangeAction action);
        partial void OnCreated();
        partial void OnActiveChanging(bool? value);
        partial void OnActiveChanged();
        partial void OnAction_IDChanging(int value);
        partial void OnAction_IDChanged();
        partial void OnAction_UserChanging(Guid value);
        partial void OnAction_UserChanged();
        partial void OnAction_DateChanging(DateTime value);
        partial void OnAction_DateChanged();
        partial void OnAction_ReasonChanging(int? value);
        partial void OnAction_ReasonChanged();
        partial void OnTSChanging(Binary value);
        partial void OnTSChanged();
        partial void OnTSLONGChanging(long? value);
        partial void OnTSLONGChanged();
        partial void OnTSMANChanging(long? value);
        partial void OnTSMANChanged();
        partial void OnChild_Sponsorship_IDChanging(int value);
        partial void OnChild_Sponsorship_IDChanged();
        partial void OnChild_IDChanging(Guid value);
        partial void OnChild_IDChanged();
        partial void OnSponsor_IDChanging(Guid value);
        partial void OnSponsor_IDChanged();
        partial void OnStart_DateChanging(DateTime? value);
        partial void OnStart_DateChanged();
        partial void OnEnd_DateChanging(DateTime? value);
        partial void OnEnd_DateChanged();
        #endregion

        public ChildSponsorshipDO()
        {
            OnCreated();
        }

        [Column(Storage = "_Active", DbType = "Bit", UpdateCheck = UpdateCheck.Never)]
        public bool? Active
        {
            get
            {
                return _Active;
            }
            set
            {
                if ((_Active != value))
                {
                    OnActiveChanging(value);
                    _Active = value;
                    OnActiveChanged();
                }
            }
        }

        [Column(Storage = "_Action_ID", DbType = "Int NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public int Action_ID
        {
            get
            {
                return _Action_ID;
            }
            set
            {
                if ((_Action_ID != value))
                {
                    OnAction_IDChanging(value);
                    _Action_ID = value;
                    OnAction_IDChanged();
                }
            }
        }

        [Column(Storage = "_Action_User", DbType = "UniqueIdentifier NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public Guid Action_User
        {
            get
            {
                return _Action_User;
            }
            set
            {
                if ((_Action_User != value))
                {
                    OnAction_UserChanging(value);
                    _Action_User = value;
                    OnAction_UserChanged();
                }
            }
        }

        [Column(Storage = "_Action_Date", DbType = "DateTime NOT NULL", UpdateCheck = UpdateCheck.Never)]
        public DateTime Action_Date
        {
            get
            {
                return _Action_Date;
            }
            set
            {
                if ((_Action_Date != value))
                {
                    OnAction_DateChanging(value);
                    _Action_Date = value;
                    OnAction_DateChanged();
                }
            }
        }

        [Column(Storage = "_Action_Reason", DbType = "Int", UpdateCheck = UpdateCheck.Never)]
        public int? Action_Reason
        {
            get
            {
                return _Action_Reason;
            }
            set
            {
                if ((_Action_Reason != value))
                {
                    OnAction_ReasonChanging(value);
                    _Action_Reason = value;
                    OnAction_ReasonChanged();
                }
            }
        }

        [Column(Storage = "_TS", AutoSync = AutoSync.Always, DbType = "rowversion NOT NULL", CanBeNull = false, IsDbGenerated = true, IsVersion = true, UpdateCheck = UpdateCheck.Never)]
        public Binary TS
        {
            get
            {
                return _TS;
            }
            set
            {
                if ((_TS != value))
                {
                    OnTSChanging(value);
                    _TS = value;
                    OnTSChanged();
                }
            }
        }

        [Column(Storage = "_TSLONG", AutoSync = AutoSync.Always, DbType = "BigInt", IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public long? TSLONG
        {
            get
            {
                return _TSLONG;
            }
            set
            {
                if ((_TSLONG != value))
                {
                    OnTSLONGChanging(value);
                    _TSLONG = value;
                    OnTSLONGChanged();
                }
            }
        }

        [Column(Storage = "_TSMAN", DbType = "BigInt", UpdateCheck = UpdateCheck.Never)]
        public long? TSMAN
        {
            get
            {
                return _TSMAN;
            }
            set
            {
                if ((_TSMAN != value))
                {
                    OnTSMANChanging(value);
                    _TSMAN = value;
                    OnTSMANChanged();
                }
            }
        }

        [Column(Storage = "_Child_Sponsorship_ID", AutoSync = AutoSync.OnInsert, DbType = "Int NOT NULL IDENTITY", IsPrimaryKey = true, IsDbGenerated = true, UpdateCheck = UpdateCheck.Never)]
        public int Child_Sponsorship_ID
        {
            get
            {
                return _Child_Sponsorship_ID;
            }
            set
            {
                if ((_Child_Sponsorship_ID != value))
                {
                    OnChild_Sponsorship_IDChanging(value);
                    _Child_Sponsorship_ID = value;
                    OnChild_Sponsorship_IDChanged();
                }
            }
        }

        [Column(Storage = "_Child_ID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        public Guid Child_ID
        {
            get
            {
                return _Child_ID;
            }
            set
            {
                if ((_Child_ID != value))
                {
                    OnChild_IDChanging(value);
                    _Child_ID = value;
                    OnChild_IDChanged();
                }
            }
        }

        [Column(Storage = "_Sponsor_ID", DbType = "UniqueIdentifier NOT NULL", IsPrimaryKey = true, UpdateCheck = UpdateCheck.Never)]
        public Guid Sponsor_ID
        {
            get
            {
                return _Sponsor_ID;
            }
            set
            {
                if ((_Sponsor_ID != value))
                {
                    OnSponsor_IDChanging(value);
                    _Sponsor_ID = value;
                    OnSponsor_IDChanged();
                }
            }
        }

        [Column(Storage = "_Start_Date", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public DateTime? Start_Date
        {
            get
            {
                return _Start_Date;
            }
            set
            {
                if ((_Start_Date != value))
                {
                    OnStart_DateChanging(value);
                    _Start_Date = value;
                    OnStart_DateChanged();
                }
            }
        }

        [Column(Storage = "_End_Date", DbType = "DateTime", UpdateCheck = UpdateCheck.Never)]
        public DateTime? End_Date
        {
            get
            {
                return _End_Date;
            }
            set
            {
                if ((_End_Date != value))
                {
                    OnEnd_DateChanging(value);
                    _End_Date = value;
                    OnEnd_DateChanged();
                }
            }
        }
    }
}
