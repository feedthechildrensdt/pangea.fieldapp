﻿using System;

namespace Pangea
{
    /// <summary>
    /// Messaging Structure for use with the Message Control
    /// </summary>
    public struct MessagesStruct
    {
        public MessagesStruct(String _msg = "", MessageTypes _mType = MessageTypes.Normal, String _msgID = "")
        {
            Message = _msg;
            MessageType = _mType;
            MessageID = _msgID;
        }

        public String Message { get; set; }

        public MessageTypes MessageType { get; set; }

        public String MessageID { get; set; }
    }
}
