﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using Pangea.Data.Collections;
using Pangea.Data.DataObjects;

namespace Pangea
{
    public class PangeaCollections
    {
        /// <summary>
        /// Resets all of the Table Collections so that they are forced to be
        /// repopulated.
        /// </summary>
        public static void ResetCollections()
        {
            _actionCol = null;
            _childRemoveReasonCol = null;
            _choresCol = null;
            _codeTablesData = null;
            _columnRulesCol = null;
            _contentTypeCol = null;
            _countryCol = null;
            _favoriteActivityCol = null;
            _favoriteLearningCol = null;
            _fileTypeCol = null;
            _genderCol = null;
            _gradeLevelCol = null;
            _healthStatusCol = null;
            _languageCol = null;
            _livesWithCol = null;
            _locationCol = null;
            _personalityTypeCol = null;
        }

        /// <summary>
        /// This is the Data from the Code Tables in the DB.
        /// </summary>
        private static Dictionary<String, ObservableCollection<ObservableCollection<Object>>> _codeTablesData;
        private static Dictionary<String, ObservableCollection<ObservableCollection<Object>>> CodeTablesData
        {
            get
            {
                if (_codeTablesData == null)
                {
                    CodeTablesDataContext codeDataContext = new CodeTablesDataContext(PangeaInfo.DBCon);

                    _codeTablesData = codeDataContext.Get_Code_Tables(PangeaInfo.User.UserID);
                }

                return _codeTablesData;
            }
        }

        private static Dictionary<String, ObservableCollection<ObservableCollection<Object>>> CodeTablesData_With_Just_Active_Countries_And_Locations
        {
            get
            {

                CodeTablesDataContext codeDataContext = new CodeTablesDataContext(PangeaInfo.DBCon);

                _codeTablesData = codeDataContext.Get_Code_Tables_With_Just_Active_Countries_And_Locations(PangeaInfo.User.UserID);

                return _codeTablesData;

            }
        }

        /// <summary>
        /// Collection of the Action Codes
        /// </summary>
        private static ActionCollection _actionCol;
        public static ActionCollection ActionCollection
        {
            get
            {
                if (_actionCol == null)
                    _actionCol = new ActionCollection(CodeTablesData["Action"]);

                return _actionCol;
            }
        }

        /// <summary>
        /// Collection of Child Remove Reasons
        /// </summary>
        private static ChildRemoveReasonCollection _childRemoveReasonCol;
        public static ChildRemoveReasonCollection ChildRemoveReasonCollection
        {
            get
            {
                if (_childRemoveReasonCol == null)
                    _childRemoveReasonCol = new ChildRemoveReasonCollection(CodeTablesData["Child_Remove_Reason"]);

                return _childRemoveReasonCol;
            }
        }

        /// <summary>
        /// Collection of Chores
        /// </summary>
        private static ChoresCollection _choresCol;
        public static ChoresCollection ChoresCollection
        {
            get
            {
                if (_choresCol == null)
                    _choresCol = new ChoresCollection(CodeTablesData["Chore"]);

                return _choresCol;
            }
        }

        /// <summary>
        /// Collection of Rules
        /// </summary>
        private static ColumnRulesCollection _columnRulesCol;
        public static ColumnRulesCollection ColumnRulesCollection
        {
            get
            {
                if (_columnRulesCol == null)
                    _columnRulesCol = new ColumnRulesCollection(CodeTablesData["Column_Rules"]);

                return _columnRulesCol;
            }
        }

        /// <summary>
        /// Collection of Content Type
        /// </summary>
        private static ContentTypeCollection _contentTypeCol;
        public static ContentTypeCollection ContentTypeCollection
        {
            get
            {
                if (_contentTypeCol == null)
                    _contentTypeCol = new ContentTypeCollection(CodeTablesData["Content_Type"]);

                return _contentTypeCol;
            }
        }

        /// <summary>
        /// Collecion of Countries
        /// </summary>
        private static CountryCollection _countryCol;
        public static CountryCollection CountryCollection
        {
            get
            {
                if (_countryCol == null)
                    _countryCol = new CountryCollection(CodeTablesData["Country"]);

                return _countryCol;
            }
        }

        private static CountryCollection _countryCol_With_Just_Active;
        public static CountryCollection CountryCollection_With_Just_Active
        {
            get
            {
                if (_countryCol_With_Just_Active == null)
                    _countryCol_With_Just_Active = new CountryCollection(CodeTablesData_With_Just_Active_Countries_And_Locations["Country"]);

                return _countryCol_With_Just_Active;
            }
        }


        /// <summary>
        /// Collection of Favorite Activities
        /// </summary>
        private static FavoriteActivityCollection _favoriteActivityCol;
        public static FavoriteActivityCollection FavoriteActivityCollection
        {
            get
            {
                if (_favoriteActivityCol == null)
                    _favoriteActivityCol = new FavoriteActivityCollection(CodeTablesData["Favorite_Activity"]);

                return _favoriteActivityCol;
            }
        }

        /// <summary>
        /// Collection of Favorite Learnings
        /// </summary>
        private static FavoriteLearningCollection _favoriteLearningCol;
        public static FavoriteLearningCollection FavoriteLearningCollection
        {
            get
            {
                if (_favoriteLearningCol == null)
                    _favoriteLearningCol = new FavoriteLearningCollection(CodeTablesData["Favorite_Learning"]);

                return _favoriteLearningCol;
            }
        }

        /// <summary>
        /// Collection of File Types
        /// </summary>
        private static FileTypeCollection _fileTypeCol;
        public static FileTypeCollection FileTypeCollection
        {
            get
            {
                if (_fileTypeCol == null)
                    _fileTypeCol = new FileTypeCollection(CodeTablesData["File_Type"]);

                return _fileTypeCol;
            }
        }

        /// <summary>
        /// Collection of Genders
        /// </summary>
        private static GenderCollection _genderCol;
        public static GenderCollection GenderCollection
        {
            get
            {
                if (_genderCol == null)
                    _genderCol = new GenderCollection(CodeTablesData["Gender"]);

                return _genderCol;
            }
        }

        /// <summary>
        /// Collection of Grade Levels
        /// </summary>
        private static GradeLevelCollection _gradeLevelCol;
        public static GradeLevelCollection GradeLevelCollection
        {
            get
            {
                if (_gradeLevelCol == null)
                    _gradeLevelCol = new GradeLevelCollection(CodeTablesData["Grade_Level"]);

                return _gradeLevelCol;
            }
        }

        /// <summary>
        /// Collection of Health Status
        /// </summary>
        private static HealthStatusCollection _healthStatusCol;
        public static HealthStatusCollection HealthStatusCollection
        {
            get
            {
                if (_healthStatusCol == null)
                    _healthStatusCol = new HealthStatusCollection(CodeTablesData["Health_Status"]);

                return _healthStatusCol;
            }
        }

        /// <summary>
        /// Collection of Languages
        /// </summary>
        private static LanguageCollection _languageCol;
        public static LanguageCollection LanguageCollection
        {
            get
            {
                if (_languageCol == null)
                    _languageCol = new LanguageCollection(CodeTablesData["Language"]);

                return _languageCol;
            }
        }

        /// <summary>
        /// Collection of Lives With Items
        /// </summary>
        private static LivesWithCollection _livesWithCol;
        public static LivesWithCollection LivesWithCollection
        {
            get
            {
                if (_livesWithCol == null)
                    _livesWithCol = new LivesWithCollection(CodeTablesData["Lives_With"]);

                return _livesWithCol;
            }
        }

        /// <summary>
        /// Collection of Locations
        /// </summary>
        private static LocationCollection _locationCol;

        public static LocationCollection LocationCollection
        {
            get
            {
                if (_locationCol == null)
                    _locationCol = new LocationCollection(CodeTablesData["Location"]);

                return _locationCol;
            }
        }

        /// <summary>
        /// Collection of Personality Types
        /// </summary>
        private static PersonalityTypeCollection _personalityTypeCol;
        public static PersonalityTypeCollection PersonalityTypeCollection
        {
            get
            {
                if (_personalityTypeCol == null)
                    _personalityTypeCol = new PersonalityTypeCollection(CodeTablesData["Personality_Type"]);

                return _personalityTypeCol;
            }
        }
    }
}
