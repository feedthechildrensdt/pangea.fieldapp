﻿namespace Pangea
{
    /// <summary>
    /// The Different Types of Messages we can have.
    /// Depending on the type of message, the circle at
    /// the begging of the message on the Message Box will
    /// be a different color.
    /// </summary>
    public enum MessageTypes
    {
        Information,  // If we want to give them information in the Message Window that may be helpful for them.
        ValidationError,
        Normal,
        Error,
        CriticalError, // This will be used when an exception is caught in the try catch statements
        SaveSucceeded,  // The Message is a Saved Sucessfully Message
        SaveFailed, // The Message is a Failed Saved Message
        TransferInfo,
        TransferFailed,
        TransferComplete,
        RetrieveData,
        RetrieveFailed,
        RetrieveComplete
    }
}
