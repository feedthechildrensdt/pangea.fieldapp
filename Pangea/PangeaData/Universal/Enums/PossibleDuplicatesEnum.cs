﻿namespace Pangea
{
    /// <summary>
    /// Possible Answers when there is possible duplicates
    /// </summary>
    public enum PossibleDuplicates
    {
        Yes,
        No,
        Continue
    }
}
