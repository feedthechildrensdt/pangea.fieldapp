﻿
using System;
using System.Configuration;
using System.Data.Linq;
using System.IO;
using System.Linq;
using System.Net;
using Pangea.Data.Collections;
using Pangea.Data.DataObjects;
using Pangea.Data.Model;
using Pangea.Data.Model.CodeTables;
using Pangea.Utilities.CustomControls.LanguageControl.Entities;
using Pangea.Utilities.Tools;
using PangeaData.Properties;

namespace Pangea
{
    /// <summary>
    /// This is where the Pangea Info will be stored during the Enrollment
    /// and updating of a chid.  This will also store the Current logged in
    /// User and the default location information.
    /// </summary>
    public class PangeaInfo
    {
        /// <summary>
        /// The Current Logged in User
        /// </summary>
        private static UserInfo _user;

        public static UserInfo User
        {
            get
            {
                if (_user == null)
                    _user = new UserInfo();

                return _user;
            }
        }

        /// <summary>
        /// The Child's Information.
        /// </summary>
        private static ChildInfo _child;

        public static ChildInfo Child
        {
            get
            {
                if (_child == null)
                    _child = new ChildInfo();

                return _child;
            }
        }

        private static ChildHistory _childHistory;

        public static ChildHistory ChildHistory
        {
            get
            {
                if (_childHistory == null)
                    _childHistory = new ChildHistory();

                return _childHistory;
            }
        }

        /// <summary>
        /// Collection of the Messages to be displayed on the Message Bar.
        /// </summary>
        private static MessageCollection _messages;

        public static MessageCollection Messages
        {
            get
            {
                if (_messages == null)
                    _messages = new MessageCollection();

                return _messages;
            }
        }

        /// <summary>
        /// Collection of the Messages to be displayed on the Message Bar when Transmitting Child Data.
        /// </summary>
        private static MessageCollection _transmitMessages;

        public static MessageCollection TransmitMessages
        {
            get
            {
                if (_transmitMessages == null)
                    _transmitMessages = new MessageCollection();

                return _transmitMessages;
            }
        }

        /// <summary>
        /// The Default Country that will be used for Children on Enrollment.
        /// /// This is only Used if the Location Select Screen is used to Select the Country
        /// </summary>
        public static Country DefaultCountry { get; set; }

        /// <summary>
        /// The Default Location that will be used for Children on Enrollment.
        /// This is only Used if the Location Select Screen is used to Select the Location
        /// </summary>
        public static Location DefaultLocation { get; set; }

        /// <summary>
        /// The Default transfer amount.  This will be set and stored.
        /// </summary>
        public static string DefaultTransferAmnt { get; set; }

        /// <summary>
        /// Variable used to determine if the Child loaded is being Enrolled.
        /// </summary>
        public static bool EnrollingChild { get; set; }

        /// <summary>
        /// Variable used to determine if the Child loaded is being Updated.
        /// </summary>
        public static bool UpdatingChild { get; set; }

        /// <summary>
        /// Variable used to determine if we are transfering data.
        /// </summary>
        public static bool TransferingData { get; set; }

        /// <summary>
        /// Variable to let us know that the transfer failed, so we can try
        /// to restart the transfer.
        /// </summary>
        public static bool TransferFailed { get; set; }

        /// <summary>
        /// Variable used to determine if we are transfering data.
        /// </summary>
        public static bool RetrievegData { get; set; }

        /// <summary>
        /// Variable to let us know that the transfer failed, so we can try
        /// to restart the transfer.
        /// </summary>
        public static bool RetrieveFailed { get; set; }

        /// <summary>
        /// Variable to let us know if a save is requred for the current child
        /// or not
        /// </summary>
        public static bool SaveRequired { get; set; }

        /// <summary>
        /// Variable used to determine if the child is a possible duplicate.
        /// </summary>
        public static PossibleDuplicates PossibleDuplicate { get; set; }

        /// <summary>
        /// This will be used to store the last directory used to look for
        /// images (profile, action, other)
        /// </summary>
        public static String LastDirectoryImages { get; set; }

        /// <summary>
        /// This will be used to store the last directory used to look for
        /// documents (pdf, docs, etx..)
        /// </summary>
        public static String LastDirectoryDocuments { get; set; }

        /// <summary>
        /// This will be used to store the last directory used to look for
        /// Optional Files.
        /// </summary>
        public static String LastDirectoryOptionalFiles { get; set; }

        public static bool TestVer
        {
            get
            {

                return Centraler.Database_Settingz.Is_Test_Version;

                /*
                bool retValue = false;
#if TESTVER || DEBUG
                retValue = true;
#endif
                return retValue;
                */
            }
        }
        /// <summary>
        /// The Pangea Enrollment Folder Location.
        /// </summary>
        public static String CommonAppPangeaEnrollmentLoc
        {
            get 
            { 
                return String.Format
                    (
                    "{0}\\{1}\\Uploaded\\Enrollment\\", 
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
                    
                    TestVer 
                    
                    ? 

                    Centraler.Database_Settingz.DB_Staging : 
                    Centraler.Database_Settingz.DB_Production
                    ); 
            }
        }

        /// <summary>
        /// The Pangea Update Folder Location.
        /// </summary>
        public static String CommonAppPangeaUpdateLoc
        {
            get 
            { 
                return String.Format
                    (
                "{0}\\{1}\\Uploaded\\Update\\", 
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
                
                TestVer 
                ? 

                Centraler.Database_Settingz.DB_Staging : 
                Centraler.Database_Settingz.DB_Production); 
            }
        }

        /// <summary>
        /// The Pangea Update History Folder Location.
        /// </summary>
        public static String CommonAppPangeaUpdateHistoryLoc
        {
            get 
            {
                return String.Format
                    (
                    "{0}\\{1}\\Uploaded\\Update\\History\\", 
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
                    TestVer 
                    
                    ? 

                    Centraler.Database_Settingz.DB_Staging : 
                    Centraler.Database_Settingz.DB_Production
                    ); 
            }
        }

        /// <summary>
        /// The Pangea Current Folder Location.
        /// </summary>
        public static String CommonAppPangeaCurrentLoc
        {
            get 
            { 
                return String.Format
                    (
                    "{0}\\{1}\\Current\\", 
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), 
                    TestVer 
                    
                    ? 
                    
                    Centraler.Database_Settingz.DB_Staging : 
                    Centraler.Database_Settingz.DB_Production); 
            }
        }

        /// <summary>
        /// Connection String for the Local DB
        /// </summary>
        public static String DBCon
        {
            get
            {

                // conn_admin_raw

                ;

                /*
                string connString_raw = Centraler.Settingz.svc_enroll_conn_LOCAL; // Settings.Default.Properties["svc_admin_conn"].ToString();   // System.Configuration. Properties.Settings.Default. ConfigurationManager.AppSettings["svc_admin_conn"].ToString();

                string Database_To_Use = Centraler.Settingz.Database_To_Use(TestVer);

                string stemp = String.Format(connString_raw, Database_To_Use);  //  String.Format(connString_raw, TestVer ? "Pangea_Test" : "Pangea"); 
                */

                string stemp = Centraler.Database_Settingz.Currently_Used_DB_Conn_String_svc_enroll_conn_LOCAL;

                return stemp;
            }
        }

        public static UILanguageDefn UILanguageDefMapping { get; set; }

        /// <summary>
        /// Used to reset everything to Default.
        /// </summary>
        public static void Reset()
        {
            LogManager.DebugLogManager.MethodBegin("PangeaInfo.Reset");

            EnrollingChild = false;

            UpdatingChild = false;

            PossibleDuplicate = PossibleDuplicates.Continue;

            DefaultCountry = null;

            DefaultLocation = null;

            Messages.Clear();

            Child.Copy(new ChildInfo());

            LogManager.DebugLogManager.MethodEnd("PangeaInfo.Reset");
        }

        /// <summary>
        /// Used to Start the Enrollment Process.
        /// </summary>
        public static void StartChildEnrollment()
        {
            LogManager.DebugLogManager.MethodBegin("PangeaInfo.StartChildEnrollment");

            EnrollingChild = true;

            UpdatingChild = false;

            PossibleDuplicate = PossibleDuplicates.Continue;

            Messages.Clear();

            Child.Copy(new ChildInfo());

            // Delete the Tempory File Saving location since we copied everything over to the correct directory.
            // Doing this to make sure nothing is left over from a previous enrollment.
            if (Directory.Exists(String.Format("{0}temp\\", PangeaInfo.CommonAppPangeaEnrollmentLoc)))
                Directory.Delete(String.Format("{0}temp\\", PangeaInfo.CommonAppPangeaEnrollmentLoc), true);

            LogManager.DebugLogManager.MethodEnd("PangeaInfo.StartChildEnrollment");
        }

        /// <summary>
        /// Used to Open a Child Enrollment that was saved or is incomplete to be modified.
        /// </summary>
        /// <param name="_child"></param>
        public static void ContinueChildEnrollment(Guid? _childID)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaInfo.ContinueChildEnrollment");

            if (_childID == null)
                return;

            ChildInfo _child = GetEnrollmentChildInfo(_childID);

            if (_child == null)
                return;

            EnrollingChild = true;

            UpdatingChild = false;

            PossibleDuplicate = _child.NonDuplicates.Count > 0 ? PossibleDuplicates.No : PossibleDuplicates.Continue;

            Child.Copy(_child);

            LogManager.DebugLogManager.MethodEnd("PangeaInfo.ContinueChildEnrollment");
        }

        /// <summary>
        /// Used to Open a Child Update.
        /// </summary>
        /// <param name="_updateChild"></param>
        public static void StartChildUpdating(Guid? _updateChildID)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaInfo.StartChildUpdating");

            EnrollingChild = false;

            UpdatingChild = true;

            PossibleDuplicate = PossibleDuplicates.No;

            ChildInfo _updateChild = GetPendingChildInfo(_updateChildID);

            ChildHistory.Copy(GetChildHistory(_updateChildID));

            string stemp = _updateChild.Action.Name.ToLower();

            // if (_updateChild.Action.Name.ToLower().Equals("field update"))
            if (stemp == "field update")
            {
                Child.Copy(GetChildHistory(_updateChildID));

                Child.FavLearning = null;

                Child.GradeLvl = null;

                Child.Personality_type = null;

                Child.Lives_With = null;

                Child.Health = null;

                Child.Chores = null;

                Child.FavActivity = null;

                Child.MajorLifeEvent = null;

                Child.AdditionalResources.Clear();
            }
            else
                Child.Copy(_updateChild);

            LogManager.DebugLogManager.MethodEnd("PangeaInfo.StartChildUpdating");
        }

        /// <summary>
        /// Checks to see if the current Child has any duplicates.
        /// </summary>
        /// <returns>True if the child has possible duplicates</returns>
        public static bool ChildHasPossibleDuplicates()
        {
            LogManager.DebugLogManager.MethodBegin("PangeaInfo.ChildHasPossibleDuplicates");

            bool retVal = false;

            if (Child.PossibleDuplicates != null)
                retVal = Child.PossibleDuplicates.Count() > 0;

            LogManager.DebugLogManager.MethodEnd("PangeaInfo.ChildHasPossibleDuplicates");

            return retVal;
        }

        /// <summary>
        /// Check to see if we have Valid Child Information 
        /// </summary>
        /// <param name="_enroll"></param>
        /// <returns>True if Valid; False if Not valid</returns>
        public static bool ValidateChildInfo(bool _enroll = false)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaInfo.ValidateChildInfo");

            // Make sure we have all the Data we need.
            // Tuple values are as follows : 
            // Item1 - Bool RetValSave - If it is ok to Save the child information
            // Item2 - Bool RetValEnroll - If it is ok to Save the Child Information for Enrollment
            Tuple<bool, bool> _validReturn = Child.ValidateChildInfo();

            LogManager.DebugLogManager.MethodEnd("PangeaInfo.ValidateChildInfo");

            ;

            bool breturn = _enroll ? _validReturn.Item2 : _validReturn.Item1;

            ;

            return breturn;
        }

        /// <summary>
        /// Saves the Child to the Local DB.
        /// </summary>
        /// <returns></returns>
        public static bool SaveProgress(bool _readyToTransmit = false)
        {
            LogManager.DebugLogManager.MethodBegin("PangeaInfo.SaveEnrollmentProgress");

            bool retVal = Child.SaveChild(_readyToTransmit);

            if (retVal)
                SaveRequired = false;

            LogManager.DebugLogManager.MethodEnd("PangeaInfo.SaveEnrollmentProgress");

            return retVal;
        }

        /// <summary>
        /// THis gets all the Enrollment information for a Child from the DB.
        /// </summary>
        /// <param name="_childID">Child ID we are looking to load from the Enrollment Tables</param>
        /// <returns>The Childs information from the Enrollment DBs</returns>
        public static ChildInfo GetEnrollmentChildInfo(Guid? _childID)
        {
            ChildInfo _child = null;

            EnrollmentTablesDataContext _enrollDataContext = new EnrollmentTablesDataContext(DBCon);

            ISingleResult<GetASavedResult> _chRes = _enrollDataContext.Get_A_Saved_Enrollment(User.UserID, _childID);

            foreach (GetASavedResult _ch in _chRes)
            {
                _child = new ChildInfo(_childID, _ch);
            }

            if (_child == null)
                return null;

            ISingleResult<GetEnrollmentChoresResult> _choresRes = _enrollDataContext.Get_Enrollment_Chores(User.UserID, _childID);

            foreach (GetEnrollmentChoresResult _chore in _choresRes)
            {
                _child.Chores.Add(PangeaCollections.ChoresCollection.Find(_chore.Chore_Code_ID));
            }

            ISingleResult<GetEnrollmentNonDuplicateResult> _nonDupRes = _enrollDataContext.Get_Child_Non_Duplicates(User.UserID, _childID);

            foreach (GetEnrollmentNonDuplicateResult _dup in _nonDupRes)
            {
                _child.NonDuplicates.Add(new ChildDuplicate(_dup));
            }

            ISingleResult<GetEnrollmentFavoriteActivitiesResult> _favActsRes = _enrollDataContext.Get_Enrollment_Favorite_Activities(User.UserID, _childID);

            foreach (GetEnrollmentFavoriteActivitiesResult _favAct in _favActsRes)
            {
                _child.FavActivity.Add(PangeaCollections.FavoriteActivityCollection.Find(_favAct.Favorite_Activity_Code_ID));
            }

            ISingleResult<GetEnrollmentPersonalityTypeResult> _personTypeRes = _enrollDataContext.Get_Enrollment_Personality_Type(User.UserID, _childID);

            foreach (GetEnrollmentPersonalityTypeResult _personalityType in _personTypeRes)
            {
                _child.Personality_type = PangeaCollections.PersonalityTypeCollection.Find(_personalityType.Personality_Type_Code_ID);
            }

            ISingleResult<GetFileDataResult> _filedataRes = _enrollDataContext.Get_File_Data(User.UserID, null, _childID, null, null, true);

            foreach (GetFileDataResult _filedata in _filedataRes)
            {
                FileData _fData = new FileData(_filedata);

                ISingleResult<GetFileResult> _fileResults = _enrollDataContext.Get_File(User.UserID, _filedata.stream_id, _childID, null, null, true);

                foreach (GetFileResult _file in _fileResults)
                {
                    String fullFileName = String.Format("{0}{1}\\{2}", PangeaInfo.CommonAppPangeaEnrollmentLoc, _childID, _file.name);

                    ContentType ctype = PangeaCollections.ContentTypeCollection.Find(_filedata.Content_Type_ID);

                    if
                        (
                        ctype == null &&
                        Centraler.Database_Settingz.Is_Special_Handling_For_Bad_Data
                        )
                    {
                        ctype = PangeaCollections.ContentTypeCollection.Find(4);
                    }


                    FileType fType = PangeaCollections.FileTypeCollection.Find(_filedata.File_Type_ID);

                    if (!Directory.Exists(String.Format("{0}{1}", PangeaInfo.CommonAppPangeaEnrollmentLoc, _childID)))
                        Directory.CreateDirectory(String.Format("{0}{1}", PangeaInfo.CommonAppPangeaEnrollmentLoc, _childID));

                    if (
                        !File.Exists(fullFileName) &&
                        _file.file_stream != null
                        )
                    {
                        MemoryStream ms = new MemoryStream(_file.file_stream.ToArray());

                        FileStream fs = new FileStream(fullFileName, FileMode.OpenOrCreate);

                        ms.WriteTo(fs);

                        fs.Close();

                        ms.Close();
                    }

                    // Bug 384 - Was setting the Optional file flag incorrectly.
                    PangeaFileStruct _rfs = new PangeaFileStruct
                        (
                        _filedata.stream_id, 
                        _file.file_stream, 
                        !ctype.Required, 
                        _file.name, 
                        fullFileName, 
                        ctype, 
                        fType, 
                        _fData
                        );

                    if (!_rfs.IsEmpty())
                        _child.AdditionalResources.Add(_rfs);
                }
            }


            return _child;
        }

        /// <summary>
        /// This tries to get the Child from the Pending tables, this is the children scheduled for Updates
        /// </summary>
        /// <param name="_updateChildID">Child ID we are looking to load from the Update Tables</param>
        /// <param name="_updateStarted">If the Child's update has been started already. Mainly used for making sure
        /// the Correct Information is copied to the child information.</param>
        /// <returns>The Childs information from the Enrollment DBs</returns>
        public static ChildInfo GetPendingChildInfo(Guid? _updateChildID)
        {
            ChildInfo _child = null;

            PendingTablesDataContext _pendingDataContext = new PendingTablesDataContext(DBCon);

            ISingleResult<GetASavedPendingResult> _chRes = _pendingDataContext.Get_A_Saved_Pending(User.UserID, _updateChildID);

            foreach (GetASavedPendingResult _ch in _chRes)
            {
                _child = new ChildInfo(_ch);

                if (_ch.Child_Remove_Reason_Code_ID != null)
                {
                    ChildRemoveReason the_child_remove_reason = PangeaCollections.ChildRemoveReasonCollection.Find(_ch.Child_Remove_Reason_Code_ID);

                    _child.Remove_Reason = the_child_remove_reason;
                }
            }

            int the_field_update_ID = PangeaCollections.ActionCollection.Find("field update").ID;

            if (_child.Action.ID != the_field_update_ID)
            {
                ISingleResult<GetEnrollmentChoresResult> _choresRes = _pendingDataContext.Get_Pending_Chores(User.UserID, _updateChildID);

                foreach (GetEnrollmentChoresResult _chore in _choresRes)
                {
                    _child.Chores.Add(PangeaCollections.ChoresCollection.Find(_chore.Chore_Code_ID));
                }

                ISingleResult<GetEnrollmentFavoriteActivitiesResult> _favActsRes = _pendingDataContext.Get_Pending_Favorite_Activities(User.UserID, _updateChildID);

                foreach (GetEnrollmentFavoriteActivitiesResult _favAct in _favActsRes)
                {
                    _child.FavActivity.Add(PangeaCollections.FavoriteActivityCollection.Find(_favAct.Favorite_Activity_Code_ID));
                }

                ISingleResult<GetEnrollmentPersonalityTypeResult> _personTypeRes = _pendingDataContext.Get_Pending_Personality_Type(User.UserID, _updateChildID);

                foreach (GetEnrollmentPersonalityTypeResult _personalityType in _personTypeRes)
                {
                    _child.Personality_type = PangeaCollections.PersonalityTypeCollection.Find(_personalityType.Personality_Type_Code_ID);
                }

                ISingleResult<GetMajorLifeEventResult> _majorLifeEvents = _pendingDataContext.Get_Pending_Major_Life_Event(User.UserID, _updateChildID);

                foreach (GetMajorLifeEventResult _majorLifeEvent in _majorLifeEvents)
                {
                    _child.MajorLifeEvent = _majorLifeEvent.Description;
                }

                ISingleResult<GetFileDataResult> _filedataRes = _pendingDataContext.Get_File_Data(User.UserID, null, _updateChildID, null, null, true);

                foreach (GetFileDataResult _filedata in _filedataRes)
                {
                    FileData _fData = new FileData(_filedata);

                    ISingleResult<GetFileResult> _fileResults = _pendingDataContext.Get_File(User.UserID, _filedata.stream_id, _updateChildID, null, null, true);

                    foreach (GetFileResult _file in _fileResults)
                    {
                        String fullFileName = String.Format("{0}{1}\\{2}", PangeaInfo.CommonAppPangeaUpdateLoc, _updateChildID, _file.name);

                        ContentType ctype = PangeaCollections.ContentTypeCollection.Find(_filedata.Content_Type_ID);

                        FileType fType = PangeaCollections.FileTypeCollection.Find(_filedata.File_Type_ID);

                        if (!Directory.Exists(String.Format("{0}{1}", PangeaInfo.CommonAppPangeaUpdateLoc, _updateChildID)))
                            Directory.CreateDirectory(String.Format("{0}{1}", PangeaInfo.CommonAppPangeaUpdateLoc, _updateChildID));

                        // Bug 317 - Tries to recreate the file even if it is there and sometimes gets an error when trying to create the file if it is already there.
                        if (!File.Exists(fullFileName))
                        {
                            MemoryStream ms = new MemoryStream(_file.file_stream.ToArray());

                            FileStream fs = new FileStream(fullFileName, FileMode.OpenOrCreate);

                            ms.WriteTo(fs);

                            fs.Close();

                            ms.Close();
                        }

                        // Bug 384 - Was setting the Optional file flag incorrectly.
                        PangeaFileStruct _rfs = new PangeaFileStruct
                            (
                            _filedata.stream_id, 
                            _file.file_stream, 
                            !ctype.Required, 
                            _file.name, 
                            fullFileName, 
                            ctype, 
                            fType, 
                            _fData
                            );

                        if (!_rfs.IsEmpty())
                            _child.AdditionalResources.Add(_rfs);
                    }
                }
            }

            return _child;
        }

        /// <summary>
        /// This goes and gets the Child History information from the DBO tables in the Database
        /// This will create the Profile Image of the Child if it hasn't been created yet.
        /// </summary>
        /// <param name="_childID">Child We want to get from the DB</param>
        /// <returns>The Child Information</returns>
        private static ChildInfo GetChildHistory(Guid? _childID)
        {
            ChildInfo _child = GetDBOChildInfo(_childID);

            PangeaFileStruct pfs = _child.AdditionalResources.Find(PangeaCollections.ContentTypeCollection.Find("Profile")).FirstOrDefault();

            if (!pfs.IsEmpty())
            {
                if (String.IsNullOrEmpty(pfs.FullFileLoc))
                    pfs.FullFileLoc = String.Format("{0}{1}\\{2}", PangeaInfo.CommonAppPangeaUpdateHistoryLoc, _childID, pfs.ShortFileName);

                if (!Directory.Exists(String.Format("{0}{1}", PangeaInfo.CommonAppPangeaUpdateHistoryLoc, _childID)))
                    Directory.CreateDirectory(String.Format("{0}{1}", PangeaInfo.CommonAppPangeaUpdateHistoryLoc, _childID));

                if (!File.Exists(pfs.FullFileLoc))
                {

                    bool Is_Not_NULL = pfs.BinaryFile != null;

                    if (Is_Not_NULL)
                    {
                        MemoryStream ms = new MemoryStream(pfs.BinaryFile.ToArray());

                        FileStream fs = new FileStream(pfs.FullFileLoc, FileMode.OpenOrCreate);

                        ms.WriteTo(fs);

                        fs.Close();

                        ms.Close();
                    }
                    else
                    {
                        WebClient web_client = new WebClient();
                                                
                        byte[] data = web_client.DownloadData(pfs.FileDataInfo.URI);                        

                        using (MemoryStream mem = new MemoryStream(data))
                        {

                            FileStream fs = new FileStream(pfs.FullFileLoc, FileMode.OpenOrCreate);

                            mem.WriteTo(fs);

                            fs.Close();

                            pfs.BinaryFile = new Binary(mem.ToArray());

                            mem.Close();

                        }

                        web_client.Dispose();

                    }

                }

                _child.AdditionalResources.Replace(pfs);
            }

            return _child;
        }

        /// <summary>
        /// This tries to get the Child from the DBO tables, 
        /// </summary>
        /// <param name="_childID">Child ID we are looking to load from the DBO Tables</param>
        /// <returns>The Childs information from the DBO tables</returns>
        public static ChildInfo GetDBOChildInfo(Guid? _childID)
        {
            ChildInfo _child = null;

            DBOTablesDataContext _dboDataContext = new DBOTablesDataContext
                (
                PangeaInfo.TestVer 
                
                ? 
                
                Centraler.Database_Settingz.DB_Staging : 
                Centraler.Database_Settingz.DB_Production
                );

            ISingleResult<GetASavedResult> _chRes = _dboDataContext.Get_A_Saved_Child(User.UserID, _childID);

            foreach (GetASavedResult _ch in _chRes)
            {
                _child = new ChildInfo(_childID, _ch);
            }

            ISingleResult<GetEnrollmentChoresResult> _choresRes = _dboDataContext.Get_Chores(User.UserID, _childID);

            foreach (GetEnrollmentChoresResult _chore in _choresRes)
            {
                Chore the_Chore = PangeaCollections.ChoresCollection.Find(_chore.Chore_Code_ID);

                _child.Chores.Add(the_Chore);
            }

            ISingleResult<GetEnrollmentFavoriteActivitiesResult> _favActsRes = _dboDataContext.Get_Favorite_Activities(User.UserID, _childID);

            foreach (GetEnrollmentFavoriteActivitiesResult _favAct in _favActsRes)
            {
                FavoriteActivity the_Favorite_Activity = PangeaCollections.FavoriteActivityCollection.Find(_favAct.Favorite_Activity_Code_ID);

                _child.FavActivity.Add(the_Favorite_Activity);
            }

            ISingleResult<GetEnrollmentPersonalityTypeResult> _personTypeRes = _dboDataContext.Get_Personality_Type(User.UserID, _childID);

            foreach (GetEnrollmentPersonalityTypeResult _personalityType in _personTypeRes)
            {
                PersonalityType the_Personality_Type = PangeaCollections.PersonalityTypeCollection.Find(_personalityType.Personality_Type_Code_ID);

                _child.Personality_type = the_Personality_Type;
            }

            ISingleResult<GetMajorLifeEventResult> _majorLifeEvents = _dboDataContext.Get_Major_Life_Event(User.UserID, _childID);

            foreach (GetMajorLifeEventResult _majorLifeEvent in _majorLifeEvents)
            {
                _child.MajorLifeEvent = _majorLifeEvent.Description;
            }

            ISingleResult<GetFileDataResult> _filedataRes = 
                _dboDataContext
                .Get_File_Data
                (
                User.UserID, 
                null, 
                _childID, 
                null, 
                null, 
                true
                );

            foreach (GetFileDataResult _filedata in _filedataRes)
            {

                FileData _fData = new FileData(_filedata);

                ISingleResult<GetFileResult> _fileResults = 
                    _dboDataContext
                    .Get_File
                    (
                    User.UserID, 
                    _filedata.stream_id, 
                    _childID, 
                    null, 
                    null, 
                    true
                    );

                foreach (GetFileResult _file in _fileResults)
                {
                    ContentType cType = PangeaCollections.ContentTypeCollection.Find(_filedata.Content_Type_ID);

                    FileType fType = PangeaCollections.FileTypeCollection.Find(_filedata.File_Type_ID);

                    // Bug 384 - Was setting the Optional file flag incorrectly.
                    PangeaFileStruct _rfs = new PangeaFileStruct
                        (
                        _filedata.stream_id, 
                        _file.file_stream, 
                        !cType.Required, 
                        _file.name, 
                        String.Empty, 
                        cType, 
                        fType, 
                        _fData
                        );

                    if (!_rfs.IsEmpty())
                        _child.AdditionalResources.Add(_rfs);
                }
            }

            return _child;
        }

    }

}
