﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Centraler
{

    public class CodeTables_StandardData
    {

        public CodeTables_StandardData() { }

        public CodeTables_StandardData
            (
            bool _Active,
            Int32 _CodeID,
            string _Name,
            string _Description,
            string _PublicDescription,
            int _MinAge
            )
        {
            Active = _Active; //  Convert.ToBoolean(_gender[0]);
            CodeID = _CodeID; //  Convert.ToInt32(_gender[1]);
            Name = _Name; // !_gender[2].Equals(DBNull.Value) ? Convert.ToString(_gender[2]) : String.Empty;
            Description = _Description; // !_gender[3].Equals(DBNull.Value) ? Convert.ToString(_gender[3]) : String.Empty;
            PublicDescription = _PublicDescription; // !_gender[4].Equals(DBNull.Value) ? Convert.ToString(_gender[4]) : String.Empty;
            MinAge = _MinAge;
        }


        public bool Active { get; set; }

        public int CodeID { get; set; }

        public String Name { get; set; }

        public String Description { get; set; }

        public String PublicDescription { get; set; }

        public int MinAge { get; set; }



        public static CodeTables_StandardData std_Data = new CodeTables_StandardData
                (
                true,
                1,
                "the name",
                "the description",
                "the public description",
                6
                );

    }
}
