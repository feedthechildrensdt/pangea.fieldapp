﻿using System;

namespace Centraler
{
    public class Settingz
    {

        //
        // PERMISSIONS
        //

        public static bool Do_Login = true;

        
        public static bool Use_Microsoft_Account_Login_And_Not_Mock_Data = true;
                
        public static bool Use_FEEDServiceAPI_UserPermissions = true;

        public static bool Use_API_Login_Data_And_Not_Mocked_Login_Data = false;


        public static double minimum_Internet_Connection_Speed = -300;


        public const string Pangea_Field_App_File_Location 
            = @"C:\Data\Work\Work Places\Cosby Harrison\Clients\Feed The Children\Azure\Pangea\Pangea\PangeaFieldApplication\bin\Debug\PangeaFieldApplication.exe";



        //
        // BINARY IMAGE HANDLING
        //
        public static int ImageDisplay_Img_Width = 621;

        public static int ImageDisplay_Img_Height = 346;

        public static int CroppenImageImg_Width = 226;

        public static int Web_Browser_Width = ImageDisplay_Img_Width + CroppenImageImg_Width + 3;

        public static string Default_Location_JSON_FileName = "defaultlocation.json";



        public static bool Allow_Records_Downloaded_DDL_Option = true;


        public static string Incomplete_Record_Saved_Message = "Child Information saved. Current data not enough to submit Child.";

        public static string Complete_Record_Saved_Message = "Child Enrollment Data Saved."; // "Child Enrollment Progress has been Saved." "Child Information saved. Current data not enough to submit Child.";


    }

}
