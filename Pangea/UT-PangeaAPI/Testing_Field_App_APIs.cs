﻿using System;
using System.Collections.ObjectModel;
using Centraler;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Pangea.Data.Model;
using Pangea.Data.Model.CodeTables;
using UT_PangeaAPI.PangeaAPI_FieldServices_FieldServices;

using Pangea;

using Pangea.API.FieldServices;

using System.Net;

namespace UT_PangeaAPI
{
    [TestClass]
    public class Testing_Field_App_APIs
    {

        string _my_User_ID = UT_PangeaAPI.Properties.Settings.Default.my_User_ID;  


        [TestMethod]
        public void SaveEnrollmentChild_APIer()
        {

            ;

            int ii = 0;

            ii += 1;

            Child a_child = Get_Child_Data_Details();

            ChildInfo child_info = new ChildInfo(a_child);

            bool Is_Operation_Successful = Pangea.API.ChildAPI.SaveEnrollmentChild(ref child_info);

            ;

            /*
            EnrollChild the_enroll_Child = new EnrollChild();

            // zz.SaveEnrollment(new EnrollChild(new ChildInfo(a_child)), "0123");

            // the_enroll_Child

            zz.SaveEnrollment(new EnrollChild(new ChildInfo(a_child)), "0123");
            */


            // Assert.AreEqual(ii, 18);

            Assert.IsTrue(Is_Operation_Successful);

        }

        private static Child Get_Child_Data_Details()
        {

            DateTime child_DOB = new DateTime(2012, 2, 11);

            // _fieldAppApiConnection = new BasicHttpsBinding_IFieldServices();
            FieldServicesClient field_APIer = new FieldServicesClient();


            Child a_child = new Child();

            int is_Active = 1;

            bool Is_Active = true;

            a_child.Action = new Pangea.Data.Model.CodeTables.Action(5, "Enroll");

            a_child.AdditionalResources = null;


            // NAME
            string Child_LastName = "Smithy";

            string Child_MiddleName = "Freddddy";

            string Child_NickName = "Alfredoo";


            string Child_MajorLifeEvent = "major life event..";


            // BROTHERS AND SISTERS
            int Child_NumberBrothers = 2;

            int Child_NumberSisters = 3;




            int RegionCode_ID = 4;

            int Min_School_Age = 2190;

            // int Location_Code_ID = 2000;

            // LOCATION
            // string Location_Name = "Santa Ana";


            // CHORE
            int chore_code_ID = 1;

            string chore_name = "the chore";

            string chore_description = "the chore description";

            string chore_public_description = "the chore public description";

            int chore_min_age = 4;


            // FavoriteActivity
            int FavoriteActivity_code_ID = 1;

            string FavoriteActivity_name = "the FavoriteActivity";

            string FavoriteActivity_description = "the FavoriteActivity description";

            string FavoriteActivity_public_description = "the FavoriteActivity public description";

            int FavoriteActivity_min_age = 4;


            // FavoriteLearning
            int FavoriteLearning_code_ID = 1;

            string FavoriteLearning_name = "the FavoriteLearning";

            string FavoriteLearning_description = "the FavoriteLearning description";

            string FavoriteLearning_public_description = "the FavoriteLearning public description";

            int FavoriteLearning_min_age = 4;


            // Gender
            int Gender_code_ID = 1;

            string Gender_name = "the Gender";

            string Gender_description = "the Gender description";

            string Gender_public_description = "the Gender public description";

            // int Gender_min_age = 4;


            // GradeLevel
            int GradeLevel_code_ID = 1;

            string GradeLevel_name = "the GradeLevel";

            string GradeLevel_description = "the GradeLevel description";

            string GradeLevel_public_description = "the GradeLevel public description";

            int GradeLevel_min_age = 4;


            // ChildCountry
            int Child_country_code = 1;

            string child_Country = "El Salvador";

            // int ChildCountry_code_ID = 1;

            string ChildCountry_name = "the ChildCountry";

            string ChildCountry_description = "the ChildCountry description";

            string ChildCountry_public_description = "the ChildCountry public description";

            int ChildCountry_min_age = 4;

            int ChildCountry_Region_ID = 4;


            // ChildLocation            
            int ChildLocation_code_ID = 1;

            string ChildLocation_name = "the GradeLevel";

            string ChildLocation_description = "the GradeLevel description";

            string ChildLocation_public_description = "the GradeLevel public description";

            int ChildLocation_min_age = 4;

            bool ChildLocation_Sponsor_Site = false;


            // Health 
            int Health_code_ID = 1;

            string Health_name = "the Health";

            string Health_description = "the Health description";

            string Health_public_description = "the Health public description";

            int Health_min_age = 4;


            // PersonalityType
            int PersonalityType_code_ID = 1;

            string PersonalityType_name = "the Health";

            string PersonalityType_description = "the Health description";

            string PersonalityType_public_description = "the Health public description";

            int PersonalityType_min_age = 4;


            
            CodeTables_StandardData grade_Level_data = new CodeTables_StandardData
                ( 
                Is_Active,
                GradeLevel_code_ID, // CodeID
                GradeLevel_name,
                GradeLevel_description,
                GradeLevel_public_description,
                GradeLevel_min_age
                );

            GradeLevel the_GradeLvl = new GradeLevel(CodeTables_StandardData.std_Data);




            a_child.ChildCountry =
                new Country
                (
                    Is_Active, // true,
                    Child_country_code,  // 1
                    "SV",
                    ChildCountry_name,  //child_Country,
                    ChildCountry_Region_ID, // RegionCode_ID, // 4,
                    ChildCountry_min_age // Min_School_Age
                    );

            a_child.ChildID = Guid.NewGuid();

            a_child.ChildLocation =
                new Location
                (
                    Is_Active,
                    ChildLocation_code_ID, // Location_Code_ID, // 2000,
                    ChildLocation_name,  // Location_Name,
                    Child_country_code, // 1,
                    ChildLocation_Sponsor_Site // false
                    );

            a_child.ChildNumber = null;

            ObservableCollection<Chore> oc_Chores = new ObservableCollection<Chore>();

            oc_Chores.Add
                (
                new Chore
                (
                    is_Active, // 1,
                    chore_code_ID, // 1,
                    chore_name, // "Jim Bobb",
                    chore_description, // "here is a description",
                    chore_public_description, // "here is a public description",
                    chore_min_age // 4 // Min Age
                    )
                );

            a_child.Chores = oc_Chores;
            //  = new ObservableCollection<int>(myList);


            a_child.DateOfBirth = child_DOB;



            FavoriteActivity the_FavActivity = new FavoriteActivity
                (
                Is_Active, // true,
                FavoriteActivity_code_ID, // 1,
                FavoriteActivity_name,  // "the name",
                FavoriteActivity_description, // "the description",
                FavoriteActivity_public_description, // "the public description",
                FavoriteActivity_min_age // Min Age
                );





            a_child.FavActivity.Add(the_FavActivity);


            FavoriteLearning fav_learning = new FavoriteLearning
                (
                Is_Active, // true,
                FavoriteLearning_code_ID, // 1,
                FavoriteLearning_name, // "fav learning name",
                FavoriteLearning_description, // "the description",
                FavoriteLearning_public_description, // "the public description",
                FavoriteLearning_min_age // 6 // Min Age
                );







            a_child.FavLearning = fav_learning;

            a_child.FirstName = "Jimm";

            Gender the_gender = new Gender
                (
                Is_Active, // true,
                Gender_code_ID, // 1,
                Gender_name, // "the gender name",
                Gender_description, // "the gender description",
                Gender_public_description // "the gender public description"
                );


            a_child.Gender = the_gender;



            // GradeLevel the_GradeLvl = new GradeLevel(CodeTables_StandardData.std_Data);

            a_child.GradeLvl = the_GradeLvl;

            a_child.HasDisability = false;



            CodeTables_StandardData Health_data = new CodeTables_StandardData
                (
                Is_Active,
                Health_code_ID,
                Health_name,
                Health_description,
                Health_public_description,
                Health_min_age
                );

            a_child.Health = new HealthStatus(Health_data); // new HealthStatus(CodeTables_StandardData.std_Data);


            a_child.LastName = Child_LastName; // "Smithy";

            a_child.Lives_With = new LivesWith(CodeTables_StandardData.std_Data);

            a_child.MajorLifeEvent = Child_MajorLifeEvent; // "major life event..";

            a_child.MiddleName = Child_MiddleName; // "Freddddy";

            a_child.NickName = Child_NickName; //  "Alfredoo";

            ChildDuplicate child_duplicate = new ChildDuplicate();

            a_child.NonDuplicates.Add(child_duplicate);

            a_child.NumberBrothers = Child_NumberBrothers; // 2;

            a_child.NumberSisters = Child_NumberSisters; // 3;


            
            CodeTables_StandardData PersonalityType_data = new CodeTables_StandardData
                (
                Is_Active,
                PersonalityType_code_ID,
                PersonalityType_name,
                PersonalityType_description,
                PersonalityType_public_description,
                PersonalityType_min_age
                );

            PersonalityType personality_type = new PersonalityType(PersonalityType_data);

            a_child.Personality_type = personality_type;


            /*
            ChildRemoveReason child_remove_reason = new ChildRemoveReason(CodeTables_StandardData.std_Data);

            a_child.Remove_Reason = child_remove_reason;
            */

            a_child.Remove_Reason = null;

            return a_child;

        }


        [TestMethod]
        public void Update_EnrollmentChild_APIer()
        {

            Child a_child = Get_Child_Data_Details();

            ChildInfo child_info = new ChildInfo(a_child);

            bool Is_Operation_Successful = Pangea.API.ChildAPI.UpdateSaveChild(ref child_info);

            Assert.IsTrue(Is_Operation_Successful);

        }


        [TestMethod]
        public void Get_Child_Data_by_Location()
        {

            BasicHttpsBinding_IFieldServices the_API = Pangea.API.ChildAPI.Get_FieldServices_API_Connection();

            // the_API.Url = "https://staging.feedthechildren.org/PanMainAPI/Services.svc";

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            int Location_ID = 4621;

            Pangea.API.FieldServices.AdminChild[] _retrieveUpdae =
                the_API
                .GetFieldAppUpdateChildren
                (
                    _my_User_ID,
                    Convert.ToString(Location_ID)
                    );

            ;

            if(_retrieveUpdae.Length >= 1)
            {
                var a_child = _retrieveUpdae[0];
            }

            Assert.IsTrue(_retrieveUpdae.Length > 0);

        }



        /// <summary>
        /// Allows for the removing of a Child which is currently in the local database
        /// </summary>
        [TestMethod]        
        public void Remove_Child()
        {

            bool retVal = false;

            string test_message = "";

            // 136677
            string Child_ID = "D90AC968-EFC0-E811-80E7-005056BF313F";

            Guid the_childID = Guid.Parse(Child_ID);

            ChildInfo the_child = Pangea.PangeaInfo.GetDBOChildInfo(the_childID);

            if(the_child != null)
            {

                CodeTables_StandardData std_data =
                    new CodeTables_StandardData
                    {
                        Active = true,
                        CodeID = 2,
                        Description = "Aged Out",
                        MinAge = 0,
                        Name = "Aged Out",
                        PublicDescription = "Aged Out"
                    };

                ChildRemoveReason the_ChildRemoveReason = new ChildRemoveReason(std_data);

                the_child.Remove_Reason = the_ChildRemoveReason;

                Pangea.API.FieldServices.RemoveChild the_child_to_remove_data = Pangea.API.ChildAPI.CreatAPIRemoveChild(the_child);



                BasicHttpsBinding_IFieldServices the_API = Pangea.API.ChildAPI.Get_FieldServices_API_Connection();

                // the_API.Url = "https://staging.feedthechildren.org/PanMainAPI/Services.svc";

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;


                Pangea.API.FieldServices.ReturnMessage _retMsg =
                    the_API
                    .RemoveAChild
                    (
                        the_child_to_remove_data,
                        _my_User_ID
                        );
                ;

                if (_retMsg.Message == "ERROR")
                    retVal = false;
                else
                {
                    retVal = true;
                }

            }
            else
            {
                test_message = "Child not found in local database";
            }

            Assert.IsTrue(retVal, test_message);

        }




    }
}
