﻿using Microsoft.Identity.Client;
using Pangea.Utilities.Tools;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Pangea.FieldApplication
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        // Below are the clientId (Application Id) of your app registration and the tenant information. 
        // You have to replace:
        // - the content of ClientID with the Application Id for your app registration
        // - Te content of Tenant by the information about the accounts allowed to sign-in in your application:
        //   - For Work or School account in your org, use your tenant ID, or domain
        //   - for any Work or School accounts, use organizations
        //   - for any Work or School acounts, or Microsoft personal account, use common
        //   - for Microsoft Personal account, use consumers
        
        private static string ClientId = "edb0e7ea-30c8-4c69-b35f-f701c29bc40b";

        private static string Tenant = "common";

        private static PublicClientApplication _clientApp;

        public static PublicClientApplication PublicClientApp { get { return _clientApp; } }

        static App()
        {
            String authority = $"https://login.microsoftonline.com/{Tenant}";

            _clientApp = new PublicClientApplication
                (
                ClientId, 
                authority, 
                TokenCacheHelper.GetUserCache()
                );
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("App.Application_Startup");

            try
            {
                Views.MainWindow _mainWin = new Views.MainWindow();

                _mainWin.Show();
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError(String.Empty, ex);

                throw ex;
            }

            LogManager.DebugLogManager.MethodEnd("App.Application_Startup");
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            // Select the text in a TextBox when it receives focus.
            EventManager.RegisterClassHandler
                (
                typeof(TextBox), 
                TextBox.PreviewMouseLeftButtonDownEvent,
                new MouseButtonEventHandler(SelectivelyIgnoreMouseButton)
                );

            EventManager.RegisterClassHandler
                (
                typeof(TextBox), 
                TextBox.GotKeyboardFocusEvent,
                new RoutedEventHandler(SelectAllText)
                );

            EventManager.RegisterClassHandler
                (
                typeof(TextBox), 
                TextBox.MouseDoubleClickEvent,
                new RoutedEventHandler(SelectAllText)
                );

            base.OnStartup(e);

        }

        private void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e)
        {
            // Find the TextBox
            DependencyObject parent = e.OriginalSource as UIElement;

            while 
                (
                parent != null 
                && !
                (parent is TextBox)
                )
                parent = VisualTreeHelper.GetParent(parent);

            if (parent != null)
            {
                var textBox = (TextBox)parent;

                if (!textBox.IsKeyboardFocusWithin)
                {
                    // If the text box is not yet focused, give it the focus and
                    // stop further processing of this click event.
                    textBox.Focus();

                    e.Handled = true;
                }
            }
        }

        private void SelectAllText(object sender, RoutedEventArgs e)
        {
            var textBox = e.OriginalSource as TextBox;

            if (textBox != null)
                textBox.SelectAll();
        }
    }
}
