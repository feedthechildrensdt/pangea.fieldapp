﻿using Pangea.Data.DataObjects;
using Pangea.Utilities.Tools;

using System;
using System.Data;
using System.Data.Linq;
using System.Net;

namespace PangeaFieldApplication.Installer
{
    partial class Installer1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

#region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
        }

        #endregion

        /// <summary>
        /// Get the DB Update Scripts.
        /// </summary>
        /// <param name="userID">User Guid</param>
        /// <param name="testVer">Is this the Test Version</param>
        /// <param name="dbVer">Current Version of DB</param>
        /// <returns></returns>
        private DataSet GetDBVersion(Guid userID, bool testVer, GetDBVersionResult dbVer)
        {
            DataSet _dbUpdate = new DataSet();

            if (testVer)
            {
                LogManager.EventLogManager.WriteEventLog("GetDBVersion");

                Pangea.API.FieldServices.BasicHttpsBinding_IFieldServices _fieldServices = new Pangea.API.FieldServices.BasicHttpsBinding_IFieldServices();

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                Pangea.API.FieldServices.DBVersion curDbVer = new Pangea.API.FieldServices.DBVersion();

                curDbVer.UserID = userID.ToString();

                if (dbVer != null)
                {
                    curDbVer.Major = dbVer.Major;
                    curDbVer.MajorSpecified = true;
                    curDbVer.Minor = dbVer.Minor;
                    curDbVer.MinorSpecified = true;
                    curDbVer.Revision = dbVer.Revision;
                    curDbVer.RevisionSpecified = true;
                    curDbVer.Build = dbVer.Build;
                    curDbVer.BuildSpecified = true;
                }
                else
                {
                    // This means our DB is original and we need to start updating.
                    curDbVer.Major = 0;
                    curDbVer.MajorSpecified = true;
                    curDbVer.Minor = 1;
                    curDbVer.MinorSpecified = true;
                    curDbVer.Revision = 1;
                    curDbVer.RevisionSpecified = true;
                    curDbVer.Build = 1;
                    curDbVer.BuildSpecified = true;
                }

                _dbUpdate = _fieldServices.GetDBversion(curDbVer);
            }
            else
            {
                LogManager.EventLogManager.WriteEventLog("GetDBVersion");
                Pangea.API.FieldServices.BasicHttpsBinding_IFieldServices _fieldServices = new Pangea.API.FieldServices.BasicHttpsBinding_IFieldServices();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                Pangea.API.FieldServices.DBVersion curDbVer = new Pangea.API.FieldServices.DBVersion();
                curDbVer.UserID = userID.ToString();

                if (dbVer != null)
                {
                    curDbVer.Major = dbVer.Major;
                    curDbVer.MajorSpecified = true;
                    curDbVer.Minor = dbVer.Minor;
                    curDbVer.MinorSpecified = true;
                    curDbVer.Revision = dbVer.Revision;
                    curDbVer.RevisionSpecified = true;
                    curDbVer.Build = dbVer.Build;
                    curDbVer.BuildSpecified = true;
                }
                else
                {
                    // This means our DB is original and we need to start updating.
                    curDbVer.Major = 0;
                    curDbVer.MajorSpecified = true;
                    curDbVer.Minor = 1;
                    curDbVer.MinorSpecified = true;
                    curDbVer.Revision = 1;
                    curDbVer.RevisionSpecified = true;
                    curDbVer.Build = 1;
                    curDbVer.BuildSpecified = true;
                }

                _dbUpdate = _fieldServices.GetDBversion(curDbVer);
            }

            return _dbUpdate;
        }

        /// <summary>
        /// This gets the DB update Scripts and starts the DB update process.
        /// </summary>
        /// <returns></returns>
        private void UpdateDB()
        {
            try
            {
                bool testVer = Convert.ToBoolean(Context.Parameters["TestVer"]);
                string dbName = Context.Parameters["DBName"];
                Guid userID = new Guid("CA2D393E-324B-4514-AE72-BBBC21A402A5"); //new Guid("0DD3FED7-9451-4FC2-A528-7C1805D55363");

                LogManager.EventLogManager.WriteEventLog(String.Format("Connect to Computer DB - {0} {1}", dbName, testVer));
                DBOTablesDataContext _dboDataContext = new DBOTablesDataContext(dbName);

                bool _verFound = false;
                GetDBVersionResult dbVer = null;

                try
                {
                    LogManager.EventLogManager.WriteEventLog(String.Format("Checking DBVersion - {0}", dbName));
                    ISingleResult<GetDBVersionResult> _curDBVer = _dboDataContext.Get_DB_Version(userID, 0, 1, 1, 1);
                        
                    foreach (GetDBVersionResult _gDBVerRes in _curDBVer)
                    {
                        dbVer = _gDBVerRes;
                        _verFound = true;
                    }
                }
                catch (Exception ex)
                {
                    LogManager.EventLogManager.WriteEventLog(string.Format("{0} - Get DB Version Not Found", ex.Message));
                    _verFound = false;
                }

                if (!_verFound)
                    dbVer = null;

                LogManager.EventLogManager.WriteEventLog("Attempt to get DB Scripts from API");
                DataSet _dbUpdate = GetDBVersion(userID, testVer, dbVer);

                LogManager.EventLogManager.WriteEventLog("Got DB Scripts Attempting to run them.");
                DataTable _tableData = _dbUpdate.Tables[0];
                foreach (DataRow _rowData in _tableData.Rows)
                {
                    if (_rowData[13] != null && Convert.ToString(_rowData[13]) != String.Empty)
                    {
                        if (UpdateDB(Convert.ToString(_rowData[13]), dbName))
                        {
                            _dboDataContext.Add_DB_Version(userID, (int?)_rowData[1], (int?)_rowData[2], (int?)_rowData[3], (int?)_rowData[4], (int?)_rowData[5],
                                                                (int?)_rowData[6], (int?)_rowData[7], (int?)_rowData[8], (int?)_rowData[9], (int?)_rowData[10],
                                                                (int?)_rowData[11], (int?)_rowData[12], Convert.ToString(_rowData[13]), null);//Convert.ToString(_rowData[13]));
                        }
                    }
                }

                if (!_verFound)
                {
                    try
                    {
                        LogManager.EventLogManager.WriteEventLog("Reset DB called.");
                        _dboDataContext.Reset_DB();
                        LogManager.EventLogManager.WriteEventLog("Reset DB call Finished.");
                    }
                    catch (Exception ex)
                    {
                        LogManager.EventLogManager.WriteEventLog(string.Format("{0} - Reset DB Failed.", ex.Message));
                    }
                }

                //Pangea.API.DBUpdates.UpdateDBConfig(dbName);
                //Pangea.API.DBUpdates.UpdateCodeTables(dbName);

                LogManager.EventLogManager.WriteEventLog("Finished running the scripts.");
            }
            catch (Exception ex)
            {
                LogManager.EventLogManager.WriteEventLog(string.Format("{0} - UpdateDB Failed.", ex.Message));
            }
        }

        /// <summary>
        /// This takes the different DB Update Scripts and parses the commands out
        /// and then executes the DB commands on the local DB.
        /// </summary>
        /// <param name="script"></param>
        private bool UpdateDB(string script, string dbName)
        {
            bool retVal = true;
            String updateString = String.Empty;

            try
            {   
                DBOTablesDataContext _dboDataContextLocal = new DBOTablesDataContext(dbName);

                while (script.Length > 0)
                {
                    int indexGO = script.IndexOf("\r\nGO");
                    if (indexGO < 0)
                    {
                        updateString = script;
                        script = String.Empty;
                    }
                    else
                    {
                        updateString = script.Substring(0, (indexGO + 4));
                        script = script.Remove(0, (indexGO + 4));
                    }
                    updateString = updateString.Replace("\r\nGO", "");

                    if (updateString.Length > 0)
                    {
                        if (updateString.Contains("[Pangea]"))
                            updateString = updateString.Replace("[Pangea]", String.Format("[{0}]", dbName));
                        else if (updateString.Contains("FUSOKHSRVSQL101.Pangea"))
                            updateString = updateString.Replace("FUSOKHSRVSQL101.Pangea", String.Format("[{0}]", dbName));

                        //LogManager.InfoLogManager.WriteLog(updateString, true);

                        _dboDataContextLocal.ExecuteCommand(updateString);
                    }
                }

                _dboDataContextLocal.SubmitChanges();
            }
            catch (Exception ex)
            {
                LogManager.EventLogManager.WriteEventLog(string.Format("Failed to Run DB Scripts - {0}", ex.Message));

                //LogManager.InfoLogManager.WriteLog("****************************************************");
                //LogManager.InfoLogManager.WriteLog(string.Format("Failed to Run DB Scripts - {0}", ex.Message));
                //LogManager.InfoLogManager.WriteLog(updateString);
                //LogManager.InfoLogManager.WriteLog("****************************************************");

                retVal = false;
            }

            return retVal;
        }
    }
}