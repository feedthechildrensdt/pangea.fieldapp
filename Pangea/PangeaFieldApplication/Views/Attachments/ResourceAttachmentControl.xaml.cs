﻿using System.Windows.Controls;

using Pangea.FieldApplication.ViewModels;
using Pangea.Utilities.Tools;

////////////////////////////////////////////////////////////////////////////////////////////////
/// This will be the Control that allows the user to Attach different resources
/// to the child.
////////////////////////////////////////////////////////////////////////////////////////////////

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for ResourceAttachmentControl.xaml
    /// </summary>
    public partial class ResourceAttachmentControl : UserControl
    {
        private ResourceAttachmentViewModel _resourceViewModel;

        public ResourceAttachmentControl()
        {
            LogManager.DebugLogManager.MethodBegin("ResourceAttachmentControl");

            InitializeComponent();

            _resourceViewModel = new ResourceAttachmentViewModel();
            DataContext = _resourceViewModel;

            UploadFileBtn.Click += _resourceViewModel.UploadFileBtn_Click;

            SizeChanged += ResourceAttachmentControl_SizeChanged;

            LogManager.DebugLogManager.MethodEnd("ResourceAttachmentControl");
        }

        private void ResourceAttachmentControl_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            ReqDocItemControl.Height = e.NewSize.Height - ReqDocumentsTI.ActualHeight - UploadFileBtn.ActualHeight;
        }

        // Bug 371 - Lets us know if we are looking at the Required Tab or Optional Tab.
        private void ReqDocumentsTI_Selected(object sender, System.Windows.RoutedEventArgs e)
        {
            _resourceViewModel.ReqDocSelected = true;
        }

        // Bug 371 - Lets us know if we are looking at the Required Tab or Optional Tab.
        private void SupportingFilesTI_Selected(object sender, System.Windows.RoutedEventArgs e)
        {
            _resourceViewModel.ReqDocSelected = false;
        }
    }
}
