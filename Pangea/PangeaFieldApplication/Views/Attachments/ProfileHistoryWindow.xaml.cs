﻿using Pangea.FieldApplication.ViewModels;
using System.Windows;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for ProfileHistoryWindow.xaml
    /// </summary>
    public partial class ProfileHistoryWindow : Window
    {
        private ProfileHistoryViewModel _profileHistoryVM;

        public ProfileHistoryWindow(PangeaFileStruct _pfs)
        {
            InitializeComponent();

            _profileHistoryVM = new ProfileHistoryViewModel(_pfs);
            DataContext = _profileHistoryVM;
        }
    }
}
