﻿using Pangea.FieldApplication.ViewModels;
using System.Windows;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for ViewChildTableWindow.xaml
    /// </summary>
    public partial class ViewChildTableWindow : Window
    {
        private ViewChildTableViewModel _viewChildTableVM;

        public ViewChildTableWindow()
        {
            InitializeComponent();

            _viewChildTableVM = new ViewChildTableViewModel();
            DataContext = _viewChildTableVM;

            Loaded += _viewChildTableVM.ViewChildTableWindow_Loaded;

            ChildTableDataGrid.SelectionChanged += _viewChildTableVM.ChildTableDataGrid_SelectionChanged;
        }
    }
}
