﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Newtonsoft.Json;
using Pangea.Data.Model;
using Pangea.FieldApplication.ViewModels;
using PangeaData.Model.DataExport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Controls;
using System.Windows.Forms;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for EnrollmentChildSearchControl.xaml
    /// </summary>
    public partial class ChildDataListControl : System.Windows.Controls.UserControl
    {
        private ChildDataListControlViewModel _childDataListVM;

        private Pangea.Data.Model.ChildInfo the_Current_Grid_Row_Data;

        private bool _Is_enrollment;

        private int ActionTypeComboBox_Selected_Index;

        public ChildDataListControl(bool enrollment)
        {
            InitializeComponent();

            ;

            _Is_enrollment = enrollment;

            Refresh_Grid_Data(_Is_enrollment);

            SizeChanged += ChildDataListControl_SizeChanged;

            ChildDataGrid.SelectionChanged += _childDataListVM.ChildDataGrid_SelectionChanged;

            ChildDataGrid.SelectionChanged += ChildDataGrid_SelectionChanged;

            SearchBtn.Click += _childDataListVM.SearchBtn_Click;

            TransmitChildBtn.Click += _childDataListVM.TransmitChildBtn_Click;

            ActionTypeComboBox.SelectionChanged += ActionTypeComboBox_SelectionChanged;
        }

        private void Refresh_Grid_Data(bool enrollment)
        {
            _childDataListVM = new ChildDataListControlViewModel(enrollment);

            DataContext = _childDataListVM;
        }

        public ChildDataListControlViewModel ChildDataListVM
        {
            get
            {
                return _childDataListVM;
            }
        }

        /// <summary>
        /// Called when the control is resized.
        /// This tries to adjust where the Controls on the Control is and 
        /// keep them from going off the screen, or ontop of each other.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChildDataListControl_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {

            double _vertSpacing = (ActionTypeSelRD.ActualHeight - Math.Max(ActionTypeLbl.ActualHeight, ActionTypeComboBox.ActualHeight)) / 2.0;

            ActionTypeLbl.Margin = new System.Windows.Thickness(5, _vertSpacing, 0, 0);

            ActionTypeComboBox.Margin = new System.Windows.Thickness(5, _vertSpacing, 0, 0);

            TransmitChildBtn.Margin = new System.Windows.Thickness(5, _vertSpacing, 0, 0);

            _vertSpacing = (SearchRD.ActualHeight -
                Math.Max(UpdateInfoLbl.ActualHeight, Math.Max(EnrollmentInfoLbl.ActualHeight, Math.Max(SearchTextBox.ActualHeight, Math.Max(SearchBtn.ActualHeight, ExportToExcellBtn.ActualHeight))))) / 2.0;

            double _horzSpacing = (e.NewSize.Width -
                (_childDataListVM.UpdateInfomationVis == System.Windows.Visibility.Visible ? UpdateInfoLbl.ActualWidth : EnrollmentInfoLbl.ActualWidth) -
                SearchTextBox.ActualWidth - SearchBtn.ActualWidth - ExportToExcellBtn.ActualWidth) / 2.0;


            UpdateInfoLbl.Margin = new System.Windows.Thickness(5, _vertSpacing, 0, 0);

            EnrollmentInfoLbl.Margin = new System.Windows.Thickness(5, _vertSpacing, 0, 0);

            SearchTextBox.Margin = new System.Windows.Thickness(_horzSpacing, _vertSpacing, 0, 0);

            SearchBtn.Margin = new System.Windows.Thickness(5, _vertSpacing, 0, 0);

            ExportToExcellBtn.Margin = new System.Windows.Thickness(5, _vertSpacing, 0, 0);

            ChildDataGrid.Width = e.NewSize.Width - 10;

            ChildDataGrid.Height = DataGridRD.ActualHeight - 10;

            ChildDataGrid.Margin = new System.Windows.Thickness(5, 5, 0, 0);
        }

        private void ActionTypeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            ActionTypeComboBox_Selected_Index = ActionTypeComboBox.SelectedIndex;

            No_Current_Grid_Record_Set();

        }

        private void ChildDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Current_Grid_Record_Set();

        }


        private void No_Current_Grid_Record_Set()
        {
            the_Current_Grid_Row_Data = null;
        }

        private void Current_Grid_Record_Set()
        {
            ChildInfo the_child_info = Grid_Row_Data(ChildDataGrid.CurrentItem);

            the_Current_Grid_Row_Data = the_child_info;
        }

        private ChildInfo Grid_Row_Data(object the_data_item)
        {

            ;

            Pangea.Data.Model.ChildInfo the_child_info = null;

            if(the_data_item != null)
            {
                Type the_type = the_data_item.GetType();

                switch (the_type.Name)
                {
                    case "Child":

                        the_child_info = new Pangea.Data.Model.ChildInfo((Pangea.Data.Model.Child)the_data_item);

                        break;

                    default:

                        the_child_info = (Pangea.Data.Model.ChildInfo)the_data_item;

                        break;

                }
            }

            return the_child_info;
        }


        private void btn_Remove_Click(object sender, System.Windows.RoutedEventArgs e)
        {

            if (the_Current_Grid_Row_Data != null)
            {

                DialogResult dialogResult = MessageBox.Show
                    (
                    "Are you sure you want to remove the currently selected child from the system?",
                    "Remove Child?",
                    MessageBoxButtons.YesNo
                    );

                if (dialogResult == DialogResult.Yes)
                {

                    int the_ActionTypeComboBox_Selected_Index = ActionTypeComboBox.SelectedIndex;

                    Pangea
                        .FieldApplication
                        .ViewModels
                        .ActionBarViewModel
                        .Delete_Child_Instance
                        (
                        the_Current_Grid_Row_Data.ChildID,
                        PangeaInfo.User.UserID
                        );

                    ActionTypeComboBox.SelectedIndex = -1;

                    ActionTypeComboBox.SelectedIndex = the_ActionTypeComboBox_Selected_Index;

                    System.Windows.MessageBox.Show("Remove Operation Completed");

                }
                else if (dialogResult == DialogResult.No)
                {
                    System.Windows.MessageBox.Show("Remove Operation Cancelled");
                }

            }

        }




        private void btn_Export_Grid_To_Excel_Click(object sender, System.Windows.RoutedEventArgs e)
        {

            GridData_Details the_spreadsheet_Data = new GridData_Details();


            // for (int i = 0; i < ChildDataGrid.Columns.Count; i++)
            for (int i = 0; i < ChildDataGrid.Items.Count; i++)
            {

                // string text = ChildDataGrid.Items[0] // [ [rowIndex, i].Text;

                Pangea.Data.Model.ChildInfo the_data = Grid_Row_Data(ChildDataGrid.Items[i]);

                ;

                // the_Current_Grid_Row_Data = (Pangea.Data.Model.ChildInfo)ChildDataGrid.CurrentItem;
                // the_Current_Grid_Row_Data = Grid_Row_Data(ChildDataGrid.CurrentItem);

                ;

                gridData data_row = new gridData();

                data_row.Location_Name = the_data.ChildLocation.Name; // "loc name";

                data_row.Location_Code = the_data.ChildLocation.CodeID.ToString(); // "loc code";

                data_row.Child_ID = the_data.ChildID.HasValue ? the_data.ChildID.Value.ToString() : ""; //  "child id";

                data_row.Child_Number = the_data.ChildNumber.HasValue ? the_data.ChildNumber.Value.ToString() : ""; //  "child num";

                data_row.First_Name = the_data.FirstName;  // "first name";

                data_row.Middle_Name = the_data.MiddleName; // "middle name";

                data_row.Last_Name = the_data.LastName; // "last name";

                data_row.Other_Name = the_data.NickName; // "other name";

                data_row.Date_Of_Birth = the_data.DOBStr; // "DOB name";

                data_row.Sex = the_data.Gender.Description; // "sx";

                the_spreadsheet_Data.GridData.Add(data_row);

                ;

            }

            ;

            /*
            List<PangeaFieldApplication.Utils.UserDetails> persons = new List<PangeaFieldApplication.Utils.UserDetails>()
           {
               new PangeaFieldApplication.Utils.UserDetails() {ID="1001", Name="ABCD", City ="City1", Country="USA"},
               new PangeaFieldApplication.Utils.UserDetails() {ID="1002", Name="PQRS", City ="City2", Country="INDIA"},
               new PangeaFieldApplication.Utils.UserDetails() {ID="1003", Name="XYZZ", City ="City3", Country="CHINA"},
               new PangeaFieldApplication.Utils.UserDetails() {ID="1004", Name="LMNO", City ="City4", Country="UK"},
          };
            */


            ;

            PangeaFieldApplication.Utils.Utilities.Export_Spreadsheet(the_spreadsheet_Data);

            ;

        }

        private void SearchTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
