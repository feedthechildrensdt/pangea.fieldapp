﻿using Pangea.FieldApplication.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for RetrieveChildRecordsWindow.xaml
    /// </summary>
    public partial class RetrieveChildRecordsWindow : Window
    {
        private RetrieveChildRecordsViewModel _retrieveChildRecVM;

        public RetrieveChildRecordsWindow()
        {

            InitializeComponent();


            List<int> the_Approved_Country_IDs = new List<int>();

            string Pangea_User_ID = PangeaInfo.User.UserID.ToString();

            the_Approved_Country_IDs = PangeaFieldApplication.Utils.Utilities.Get_Approved_Country_IDs_For_Current_User(Pangea_User_ID);
            
            _retrieveChildRecVM = new RetrieveChildRecordsViewModel(the_Approved_Country_IDs);

            DataContext = _retrieveChildRecVM;

            this.WindowStartupLocation = WindowStartupLocation.CenterOwner;

        }
    }
}
