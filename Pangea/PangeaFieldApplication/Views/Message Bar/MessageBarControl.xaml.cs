﻿using System.Windows;
using System.Windows.Controls;

using Pangea.FieldApplication.ViewModels;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for MessageBarControl.xaml
    /// </summary>
    public partial class MessageBarControl : UserControl
    {
        private MessageBarViewModel _messageBarVM;

        public MessageBarControl()
        {
            InitializeComponent();

            _messageBarVM = new MessageBarViewModel();
            DataContext = _messageBarVM;
        }

        public MessageBarViewModel MessageBarVM
        {
            get { return _messageBarVM; }
        }
    }
}
