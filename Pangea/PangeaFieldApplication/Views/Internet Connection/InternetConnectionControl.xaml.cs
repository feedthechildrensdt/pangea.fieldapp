﻿using Pangea.FieldApplication.ViewModels;
using System.Windows.Controls;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for InternetConnectionControl.xaml
    /// </summary>
    public partial class InternetConnectionControl : UserControl
    {
        private InternetConnectionViewModel _internetConnectionVM;

        public InternetConnectionControl()
        {
            InitializeComponent();

            _internetConnectionVM = new InternetConnectionViewModel();
            DataContext = _internetConnectionVM;
        }

        public double DownloadSpeed
        {
            get { return _internetConnectionVM.DownloadSpeed; }
            set { _internetConnectionVM.DownloadSpeed = value; }
        }
    }
}
