﻿using Pangea.FieldApplication.ViewModels;
using System.Windows;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for MajorLifeEventsWindow.xaml
    /// </summary>
    public partial class MajorLifeEventsWindow : Window
    {
        private MajorLifeEventsViewModel _majorLifeEventVM;

        public MajorLifeEventsWindow()
        {
            InitializeComponent();

            _majorLifeEventVM = new MajorLifeEventsViewModel();
            DataContext = _majorLifeEventVM;

            // BUG 311 - Added the Mouse Click Functions for viewing the Majore Life Events Rules.
            MajorLifeEventQuestionTextBlock.MouseLeftButtonDown += _majorLifeEventVM.MajorLifeEventQuestionTextBlock_MouseLeftButtonDown;
            MajorLifeEventQuestionTextBlock.MouseLeftButtonUp += _majorLifeEventVM.MajorLifeEventQuestionTextBlock_MouseLeftButtonUp;
            MajorLifeEventQuestionTextBlock.MouseEnter += _majorLifeEventVM.MajorLifeEventQuestionTextBlock_MouseEnter;
            MajorLifeEventQuestionTextBlock.MouseLeave += _majorLifeEventVM.MajorLifeEventQuestionTextBlock_MouseLeave;
        }
    }
}
