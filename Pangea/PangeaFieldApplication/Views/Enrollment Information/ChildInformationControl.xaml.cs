﻿//////////////////////////////////////////////////////////////////////////
/// Control for Collecting the General Information of the Child when 
/// Enrolling the Child.
/////////////////////////////////////////////////////////////////////////

using System;
using System.Windows.Controls;

using Pangea.FieldApplication.ViewModels;
using Pangea.Utilities.Tools;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for ChildBaseInformationControl.xaml
    /// </summary>
    public partial class ChildInformationControl : UserControl
    {
        private ChildInformationViewModel _childInfoVM;

        public ChildInformationControl()
        {
            LogManager.DebugLogManager.MethodBegin("GeneralInformationControl");

            InitializeComponent();

            _childInfoVM = new ChildInformationViewModel();

            DataContext = _childInfoVM;

            Loaded += _childInfoVM.GeneralInformationControl_Loaded;

            SizeChanged += ChildInformationControl_SizeChanged;

            CountryQuestionLbl.MouseLeftButtonDown += _childInfoVM.CountryQuestionLbl_MouseLeftButtonDown;

            CountryQuestionLbl.MouseLeftButtonUp += _childInfoVM.CountryQuestionLbl_MouseLeftButtonUp;

            LocNameQuestionLbl.MouseLeftButtonDown += _childInfoVM.LocNameQuestionLbl_MouseLeftButtonDown;

            LocNameQuestionLbl.MouseLeftButtonUp += _childInfoVM.LocNameQuestionLbl_MouseLeftButtonUp;

            LocCodeQuestionLbl.MouseLeftButtonDown += _childInfoVM.LocCodeQuestionLbl_MouseLeftButtonDown;

            LocCodeQuestionLbl.MouseLeftButtonUp += _childInfoVM.LocCodeQuestionLbl_MouseLeftButtonUp;

            FNameQuestionLbl.MouseLeftButtonDown += _childInfoVM.FNameQuestionLbl_MouseLeftButtonDown;

            FNameQuestionLbl.MouseLeftButtonUp += _childInfoVM.FNameQuestionLbl_MouseLeftButtonUp;

            MNameQuestionLbl.MouseLeftButtonDown += _childInfoVM.MNameQuestionLbl_MouseLeftButtonDown;

            MNameQuestionLbl.MouseLeftButtonUp += _childInfoVM.MNameQuestionLbl_MouseLeftButtonUp;

            LNameQuestionLbl.MouseLeftButtonDown += _childInfoVM.LNameQuestionLbl_MouseLeftButtonDown;

            LNameQuestionLbl.MouseLeftButtonUp += _childInfoVM.LNameQuestionLbl_MouseLeftButtonUp;

            NNameQuestionLbl.MouseLeftButtonDown += _childInfoVM.NNameQuestionLbl_MouseLeftButtonDown;

            NNameQuestionLbl.MouseLeftButtonUp += _childInfoVM.NNameQuestionLbl_MouseLeftButtonUp;

            GenderQuestionLbl.MouseLeftButtonDown += _childInfoVM.GenderQuestionLbl_MouseLeftButtonDown;

            GenderQuestionLbl.MouseLeftButtonUp += _childInfoVM.GenderQuestionLbl_MouseLeftButtonUp;

            DOBQuestionLbl.MouseLeftButtonDown += _childInfoVM.DOBQuestionLbl_MouseLeftButtonDown;

            DOBQuestionLbl.MouseLeftButtonUp += _childInfoVM.DOBQuestionLbl_MouseLeftButtonUp;

            CountryQuestionLbl.MouseEnter += _childInfoVM.MouseEnter;

            CountryQuestionLbl.MouseLeave += _childInfoVM.MouseLeave;

            LocNameQuestionLbl.MouseEnter += _childInfoVM.MouseEnter;

            LocNameQuestionLbl.MouseLeave += _childInfoVM.MouseLeave;

            LocCodeQuestionLbl.MouseLeave += _childInfoVM.MouseLeave;

            LocCodeQuestionLbl.MouseEnter += _childInfoVM.MouseEnter;

            FNameQuestionLbl.MouseLeave += _childInfoVM.MouseLeave;

            FNameQuestionLbl.MouseEnter += _childInfoVM.MouseEnter;

            MNameQuestionLbl.MouseLeave += _childInfoVM.MouseLeave;

            MNameQuestionLbl.MouseEnter += _childInfoVM.MouseEnter;

            LNameQuestionLbl.MouseLeave += _childInfoVM.MouseLeave;

            LNameQuestionLbl.MouseEnter += _childInfoVM.MouseEnter;

            NNameQuestionLbl.MouseLeave += _childInfoVM.MouseLeave;

            NNameQuestionLbl.MouseEnter += _childInfoVM.MouseEnter;

            GenderQuestionLbl.MouseLeave += _childInfoVM.MouseLeave;

            GenderQuestionLbl.MouseEnter += _childInfoVM.MouseEnter;

            DOBQuestionLbl.MouseLeave += _childInfoVM.MouseLeave;

            DOBQuestionLbl.MouseEnter += _childInfoVM.MouseEnter;

            LogManager.DebugLogManager.MethodEnd("GeneralInformationControl");
        }

        private void ChildInformationControl_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("GeneralInformationControl.ChildInformationControl_SizeChanged");

            double _horizSpacing = cImageCD.ActualWidth / 6;

            double _VertSpacing = (e.NewSize.Height - (e.NewSize.Height / 2.44) - ChildIDHeaderTextBlock.ActualHeight - ChildNumberTextBlock.ActualHeight) / 2;

            PangeaImage.Width = cImageCD.ActualWidth - (_horizSpacing * 2);

            PangeaImage.Height = (e.NewSize.Height / 2.44);

            PangeaImage.Margin = new System.Windows.Thickness(_horizSpacing, _VertSpacing, 0, 0);

            ChildIDTextBlock.Width = cImageCD.ActualWidth - ChildIDHeaderTextBlock.ActualWidth;

            ChildNumberTextBlock.Width = cImageCD.ActualWidth - ChildNumberHeaderTextBlock.ActualWidth;

            //LocationCD
            _VertSpacing = (e.NewSize.Height - ChildInfoLbl.ActualHeight - CountryInfoStackPanel.ActualHeight - LocationNameStackPanel.ActualHeight - LocCodeStackPanel.ActualHeight) / 3.5;

            double _controlWidth = LocationCD.ActualWidth - 15;


            LocCountryLbl.Width = _controlWidth - 
                (_childInfoVM.CoutryAstrikVis == System.Windows.Visibility.Visible ? CountryAstrik.ActualWidth : 0.0 ) - 
                (_childInfoVM.CountryQuestionVis == System.Windows.Visibility.Visible ? CountryQuestionLbl.ActualWidth : 0.0);

            LocCountryCBox.Width = _controlWidth;


            LocNameLbl.Width = _controlWidth - 
                (_childInfoVM.LocAstrikVis == System.Windows.Visibility.Visible ? LocNameAstrik.ActualWidth : 0.0 ) - 
                (_childInfoVM.LocNameQuestionVis == System.Windows.Visibility.Visible ? LocNameQuestionLbl.ActualWidth : 0.0);

            LocNameCBox.Width = _controlWidth;


            LocCodeLbl.Width = _controlWidth - 
                (_childInfoVM.LocAstrikVis == System.Windows.Visibility.Visible ? LocCodeAstrik.ActualWidth : 0.0 ) - 
                (_childInfoVM.LocCodeQuestionVis == System.Windows.Visibility.Visible ? LocCodeQuestionLbl.ActualWidth : 0.0);

            LocCodeCBox.Width = _controlWidth;


            CountryInfoStackPanel.Margin = new System.Windows.Thickness(5, _VertSpacing, 0, 0);

            LocationNameStackPanel.Margin = new System.Windows.Thickness(5, _VertSpacing, 0, 0);

            LocCodeStackPanel.Margin = new System.Windows.Thickness(5, _VertSpacing, 0, 0);

            //GenInfoCD
            _controlWidth = (GenInfoCD.ActualWidth / 2.07) - 15;

            _horizSpacing = (GenInfoCD.ActualWidth - (_controlWidth * 2)) / 3.5;


            ChildInfoFNameLbl.Width = _controlWidth - 
                (_childInfoVM.FNameAstrikVis == System.Windows.Visibility.Visible ? FNameAstrik.ActualWidth : 0.0 ) -
                (_childInfoVM.FNameQuestionVis == System.Windows.Visibility.Visible ? FNameQuestionLbl.ActualWidth : 0.0);

            ChildInfoFNameTBox.Width = _controlWidth;


            ChildInfoMNameLbl.Width = _controlWidth -
                (_childInfoVM.MNameAstrikVis == System.Windows.Visibility.Visible ? MNameAstrik.ActualWidth : 0.0) -
                (_childInfoVM.MNameQuestionVis == System.Windows.Visibility.Visible ? MNameQuestionLbl.ActualWidth : 0.0 );

            ChildInfoMNameTBox.Width = _controlWidth;


            ChildInfoLNameLbl.Width = _controlWidth -
                (_childInfoVM.LNameAstrikVis == System.Windows.Visibility.Visible ? LNameAstrik.ActualWidth : 0.0) -
                (_childInfoVM.LNameQuestionVis == System.Windows.Visibility.Visible ? LNameQuestionLbl.ActualWidth : 0.0);

            ChildInfoLNameTBox.Width = _controlWidth;

            ChildInfoNNameLbl.Width = _controlWidth -
                (_childInfoVM.NNameAstrikVis == System.Windows.Visibility.Visible ? NNameAstrik.ActualWidth : 0.0) -
                (_childInfoVM.NNameQuestionVis == System.Windows.Visibility.Visible ? NNameQuestionLbl.ActualWidth : 0.0);

            ChildInfoNNameTBox.Width = _controlWidth;

            
            ChildInfoGenderLbl.Width = _controlWidth - 
                (_childInfoVM.GenderAstrikVis == System.Windows.Visibility.Visible ? GenderAstrik.ActualWidth : 0.0) -
                (_childInfoVM.GenderQuestionVis == System.Windows.Visibility.Visible ? GenderQuestionLbl.ActualWidth : 0.0);

            ChildInfoGenderCBox.Width = _controlWidth;


            ChildInfoDateOfBirthLbl.Width = _controlWidth - 
                (_childInfoVM.DOBAstrikVis == System.Windows.Visibility.Visible ? DOBAstrik.ActualWidth : 0.0) -
                (_childInfoVM.DOBQuestionVis == System.Windows.Visibility.Visible ? DOBQuestionLbl.ActualWidth : 0.0);

            ChildInfoDateofBirthTBox.Width = _controlWidth;

            ChildInfoDateOfBirthDtPkr.Width = _controlWidth;

            ChildInfoFirstRow.Margin = new System.Windows.Thickness(5, _VertSpacing, 0, 0);

            ChildInfoSecondRow.Margin = new System.Windows.Thickness(5, _VertSpacing, 0, 0);

            ChildInfoThirdRow.Margin = new System.Windows.Thickness(5, _VertSpacing, 0, 0);


            MiddleNameSP.Margin = new System.Windows.Thickness(_horizSpacing, 0, 0, 0);

            NickNameSP.Margin = new System.Windows.Thickness(_horizSpacing, 0, 0, 0);

            DOBSP.Margin = new System.Windows.Thickness(_horizSpacing, 0, 0, 0);

            LogManager.DebugLogManager.MethodEnd("GeneralInformationControl.ChildInformationControl_SizeChanged");
        }
    }
}
