﻿using Pangea.FieldApplication.ViewModels;
using System;
using System.Windows;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for RemoveUpdateChildWindow.xaml
    /// </summary>
    public partial class RemoveUpdateChildWindow : Window
    {
        private RemoveUpdateChildViewModel _removeUpdateChildVM;

        public RemoveUpdateChildWindow()
        {
            InitializeComponent();

            _removeUpdateChildVM = new RemoveUpdateChildViewModel();

            DataContext = _removeUpdateChildVM;

            _removeChildWindowTitle = String.Format("Remove Child {0}", PangeaInfo.Child.ChildNumber);
        }

        private String _removeChildWindowTitle;
        public String RemoveChildWindowTitle
        {
            get
            {
                return _removeChildWindowTitle;
            }
        }
    }
}
