﻿using Pangea.FieldApplication.ViewModels;
using Pangea.Utilities.Tools;
using System.Windows.Controls;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for HomeScreenControl.xaml
    /// </summary>
    public partial class HomeScreenControl : UserControl
    {
        private HomeScreenViewModel _homeScreenVM;
        public HomeScreenControl()
        {
            InitializeComponent();

            _homeScreenVM = new HomeScreenViewModel();
            DataContext = _homeScreenVM;

            SizeChanged += HomeScreenControl_SizeChanged;
        }

        private void HomeScreenControl_SizeChanged(object sender, System.Windows.SizeChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("HomeScreenControl.HomeScreenControl_SizeChanged");

            double horizSpacing = (HomeScreenCtrl.ActualWidth - (EnrollmentTitleLbl.ActualWidth - UpdateTitleLbl.ActualWidth)) / 4.0;

            EnrollmentInfoSP.Margin = new System.Windows.Thickness(horizSpacing, 10, 0, 0);
            UpdateInfoSP.Margin = new System.Windows.Thickness(horizSpacing, 10, 0, 0);
            

            LogManager.DebugLogManager.MethodEnd("HomeScreenControl.HomeScreenControl_SizeChanged");
        }

        public void Refresh()
        {
            _homeScreenVM.Refresh();
        }
    }
}
