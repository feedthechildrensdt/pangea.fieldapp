﻿using Pangea.FieldApplication.ViewModels;
using System.Windows;

namespace Pangea.FieldApplication.Views
{
    /// <summary>
    /// Interaction logic for PossibleDuplicateWindow.xaml
    /// </summary>
    public partial class PossibleDuplicateWindow : Window
    {
        private PossibleDuplicateWindowVM _possibleDuplicateWindowVM;

        public PossibleDuplicateWindow()
        {
            InitializeComponent();

            _possibleDuplicateWindowVM = new PossibleDuplicateWindowVM();
            DataContext = _possibleDuplicateWindowVM;
        }

        public PossibleDuplicateWindowVM ViewModel
        {
            get { return _possibleDuplicateWindowVM; }
        }
    }
}
