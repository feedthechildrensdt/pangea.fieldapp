﻿using Pangea.Utilities.Base.Commands;
using Pangea.Utilities.Tools;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Pangea.FieldApplication.ViewModels
{
    public class MessageBarViewModel : PangeaBaseViewModel
    {
        public MessageBarViewModel()
        {
            PangeaInfo.Messages.CollectionChanged += Messages_CollectionChanged;
            PangeaInfo.TransmitMessages.CollectionChanged += Messages_CollectionChanged;
        }

        
        /// <summary>
        /// These are the different messages that have happened in the Application.
        /// This Takes those messages and displays them.
        /// </summary>
        public ObservableCollection<StackPanel> Messages
        {
            get
            {
                LogManager.DebugLogManager.MethodBegin("MessageBarViewModel.Messages");

                ObservableCollection<StackPanel> _messages = new ObservableCollection<StackPanel>();

                try
                {
                    // I want to always have the Transfer Dialog ontop.
                    if 
                        (

                        PangeaInfo.TransmitMessages.Contains(MessageTypes.TransferInfo) || 
                        PangeaInfo.TransmitMessages.Contains(MessageTypes.TransferFailed) 
                        
                        || 
                        
                        PangeaInfo.TransmitMessages.Contains(MessageTypes.TransferComplete) ||
                        PangeaInfo.TransmitMessages.Contains(MessageTypes.RetrieveData) || 
                        PangeaInfo.TransmitMessages.Contains(MessageTypes.RetrieveFailed) || 
                        PangeaInfo.TransmitMessages.Contains(MessageTypes.RetrieveComplete)

                        )

                    {
                        foreach (MessagesStruct ms in PangeaInfo.TransmitMessages)
                        {
                            _messages.Add(BuildMessage(ms));
                        }
                    }
                    // BUG 267 - Removed all of the Else from these If Statements so that id doesnt skip over some of the Messages like it was doing.
                    // I want to put all the Critical Error Messages
                    if (PangeaInfo.Messages.Contains(MessageTypes.CriticalError))
                    {
                        foreach 
                            (
                            MessagesStruct ms in 

                            PangeaInfo
                            .Messages
                            .Where
                            (
                                msg => 
                                msg.MessageType == MessageTypes.CriticalError
                                )
                            )
                        {
                            _messages.Add(BuildMessage(ms));
                        }
                    }
                    // I want the Save Succeeded or Save Failed Messages
                    if 
                        (
                        PangeaInfo.Messages.Contains(MessageTypes.SaveFailed) || 
                        PangeaInfo.Messages.Contains(MessageTypes.SaveSucceeded)
                        )
                    {
                        foreach 
                            (
                            MessagesStruct ms in 
                            
                            PangeaInfo
                            .Messages
                            .Where
                            (
                                msg => 
                                (
                                msg.MessageType == MessageTypes.SaveFailed || 
                                msg.MessageType == MessageTypes.SaveSucceeded
                                )
                                )
                            )
                        {
                            _messages.Add(BuildMessage(ms));
                        }
                    }
                    // I want to put all the Information messages next
                    if (PangeaInfo.Messages.Contains(MessageTypes.Information))
                    {
                        foreach 
                            (
                            MessagesStruct ms in 
                            
                            PangeaInfo
                            .Messages
                            .Where
                            (
                                msg => 
                                msg.MessageType == MessageTypes.Information
                                )
                            )
                        {

                            _messages.Add(BuildMessage(ms));

                        }
                    }

                    // Now i want everything else.
                    foreach 
                        (
                        MessagesStruct ms in 
                        
                        PangeaInfo
                        .Messages
                        .Where
                        (
                            msg => 
                            (
                            msg.MessageType != MessageTypes.Information && 
                            msg.MessageType != MessageTypes.CriticalError && 
                            msg.MessageType != MessageTypes.SaveFailed && 
                            msg.MessageType != MessageTypes.SaveSucceeded
                            )
                            )
                        )
                    {

                        _messages.Add(BuildMessage(ms));

                    }
                }
                catch (Exception ex)
                {
                    LogManager.ErrorLogManager.WriteError("MessageBarViewModel.Messages", ex);
#if DEBUG
                    MessageBox.Show(ex.Message, "MessageBarViewModel.Messages");
#endif
                }

                LogManager.DebugLogManager.MethodEnd("MessageBarViewModel.Messages");

                return _messages;
            }
        }

        /// <summary>
        /// Builds the Message Control that will be displayed on the Message Bar.
        /// </summary>
        /// <param name="_msg">The Message Structure to be turned into a Message Control</param>
        /// <returns>The Message Control that will be displayed in the Message Bar</returns>
        private StackPanel BuildMessage(MessagesStruct _msg)
        {
            StackPanel _sPanel = new StackPanel();

            _sPanel.Orientation = Orientation.Horizontal;

            Ellipse _ellipse = new Ellipse();

            _ellipse.Width = 10;

            _ellipse.Height = 10;

            // TODO : Change this depending on the MessageType.
            if (_msg.MessageType == MessageTypes.ValidationError)
                _ellipse.Fill = Brushes.PaleVioletRed;
            else if (_msg.MessageType == MessageTypes.CriticalError)
                _ellipse.Fill = Brushes.DarkRed;
            else if (_msg.MessageType == MessageTypes.Information)
                _ellipse.Fill = Brushes.LightGoldenrodYellow;
            else if (_msg.MessageType == MessageTypes.Error)
                _ellipse.Fill = Brushes.Red;
            else if (_msg.MessageType == MessageTypes.SaveFailed)
                _ellipse.Fill = Brushes.MediumVioletRed;
            else if (_msg.MessageType == MessageTypes.SaveSucceeded)
                _ellipse.Fill = Brushes.MediumSpringGreen;

            TextBlock _tBlock = new TextBlock();

            _tBlock.TextWrapping = TextWrapping.Wrap;

            _tBlock.Text = String.Format( "{0}: {1}", DateTime.Now.ToString("g"), _msg.Message);

            _tBlock.Width = 240;

            _tBlock.Margin = new Thickness(5, 0, 0, 0);

            if 
                (
                _msg.MessageType == MessageTypes.TransferInfo || 
                _msg.MessageType == MessageTypes.TransferFailed || 
                _msg.MessageType == MessageTypes.TransferComplete ||
                _msg.MessageType == MessageTypes.RetrieveData || 
                _msg.MessageType == MessageTypes.RetrieveFailed || 
                _msg.MessageType == MessageTypes.RetrieveComplete 
                )
            {
                Image _transferImage = new Image();

                _transferImage.Width = 20;

                _transferImage.Height = 20;

                _transferImage.Source = 
                    (
                    _msg.MessageType == MessageTypes.TransferInfo || 
                    _msg.MessageType == MessageTypes.RetrieveData
                    ) 
                    
                    ? 
                    
                    (
                    _msg.MessageType == MessageTypes.RetrieveData 
                    ? 
                    DefaultImages.RetrieveArrow 
                    : 
                    DefaultImages.TransferArrow
                    ) 
                    
                    : 
                    
                    (
                    (
                    _msg.MessageType == MessageTypes.TransferComplete || 
                    _msg.MessageType == MessageTypes.RetrieveComplete
                    ) 
                    ? 
                    DefaultImages.TransferComplete 
                    : 
                    DefaultImages.TransferFailed
                    );

                _sPanel.Children.Add(_transferImage);
            }
            else if (_msg.MessageType != MessageTypes.Normal)
                _sPanel.Children.Add(_ellipse);

            _sPanel.Children.Add(_tBlock);

            if (
                _msg.MessageType == MessageTypes.TransferInfo ||
                _msg.MessageType == MessageTypes.RetrieveData 
                )
            {
                Image _transferImage = new Image();

                _transferImage.Width = 20;

                _transferImage.Height = 20;

                _transferImage.Source = DefaultImages.StopTransfer;

                Button _transferButton = new Button();

                _transferButton.Content = _transferImage;

                _transferButton.BorderBrush = Brushes.Transparent;
                _transferButton.Background = Brushes.Transparent;

                _transferButton.Command = 
                    _msg.MessageType == MessageTypes.TransferInfo 
                    ? 
                    StopButtonClickedTransfer 
                    : 
                    StopButtonClickedRetrieve;

                _transferButton.ToolTip = 
                    (
                    _msg.MessageType == MessageTypes.RetrieveData 
                    ? 
                    "Stop Retrieve" 
                    : 
                    "Stop Transfer"
                    );

                _sPanel.Children.Add(_transferButton);
            }
            else if 
                (
                _msg.MessageType == MessageTypes.TransferFailed ||
                _msg.MessageType == MessageTypes.RetrieveFailed 
                )
            {
                Image _transferRetryImage = new Image();

                _transferRetryImage.Width = 20;

                _transferRetryImage.Height = 20;

                _transferRetryImage.Source = DefaultImages.RetryTransfer;

                Button _transferRetryBtn = new Button();

                _transferRetryBtn.Content = _transferRetryImage;

                _transferRetryBtn.BorderBrush = Brushes.Transparent;

                _transferRetryBtn.Background = Brushes.Transparent;

                _transferRetryBtn.Command = 
                    _msg.MessageType == 
                    MessageTypes.TransferFailed 
                    ? 
                    RetryButtonClickedTransfer 
                    : 
                    RetryButtonClickedRetrieve;

                _transferRetryBtn.ToolTip = 
                    (
                    _msg.MessageType == MessageTypes.RetrieveFailed 
                    ? 
                    "Retry Retrieve" 
                    : 
                    "Retry Transfer"
                    );

                _sPanel.Children.Add(_transferRetryBtn);

            }

            return _sPanel;

        }

        /// <summary>
        /// This is a custom Button Click command. This allows us to
        /// call the button click command from the view model instead
        /// of the code behind the View.
        /// </summary>
        private ICommand _stopButtonClickedTransfer;

        public ICommand StopButtonClickedTransfer
        {
            get
            {
                return _stopButtonClickedTransfer 
                    ?? 
                    (
                    _stopButtonClickedTransfer = new RelayCommand(param => StopCommandTransfer())
                    );
            }
        }

        /// <summary>
        /// This is called when the Stop Button Next to the Transfer Text is clicked.
        /// </summary>
        private void StopCommandTransfer()
        {
            LogManager.DebugLogManager.MethodBegin("MessageBarViewModel.Messages_CollectionChanged");

            SendPropertyChanged("StopTansfer");

            LogManager.DebugLogManager.MethodEnd("MessageBarViewModel.Messages_CollectionChanged");
        }

        /// <summary>
        /// This is a custom Button Click command. This allows us to
        /// call the button click command from the view model instead
        /// of the code behind the View.
        /// </summary>
        private ICommand _stopButtonClickedRetrieve;

        public ICommand StopButtonClickedRetrieve
        {
            get
            {
                return _stopButtonClickedRetrieve ?? 
                    (
                    _stopButtonClickedRetrieve = new RelayCommand(param => StopCommandRetrieve())
                    );
            }
        }

        /// <summary>
        /// This is called when the Stop Button Next to the Transfer Text is clicked.
        /// </summary>
        private void StopCommandRetrieve()
        {
            LogManager.DebugLogManager.MethodBegin("MessageBarViewModel.Messages_CollectionChanged");

            SendPropertyChanged("StopRetrieve");

            LogManager.DebugLogManager.MethodEnd("MessageBarViewModel.Messages_CollectionChanged");
        }

        /// <summary>
        /// This is a custom Button Click command. This allows us to
        /// call the button click command from the view model instead
        /// of the code behind the View.
        /// </summary>
        private ICommand _retryButtonClickedTransfer;

        public ICommand RetryButtonClickedTransfer
        {
            get
            {
                return _retryButtonClickedTransfer ?? 
                    (
                    _retryButtonClickedTransfer = new RelayCommand(param => RetryCommandTransfer())
                    );
            }
        }

        /// <summary>
        /// This is called when the Retry Button Next to the Transfer Text is clicked.
        /// </summary>
        private void RetryCommandTransfer()
        {
            LogManager.DebugLogManager.MethodBegin("MessageBarViewModel.Messages_CollectionChanged");

            SendPropertyChanged("RetryTransfer");

            LogManager.DebugLogManager.MethodEnd("MessageBarViewModel.Messages_CollectionChanged");
        }

        /// <summary>
        /// This is a custom Button Click command. This allows us to
        /// call the button click command from the view model instead
        /// of the code behind the View.
        /// </summary>
        private ICommand _retryButtonClickedRetrieve;

        public ICommand RetryButtonClickedRetrieve
        {
            get
            {
                return _retryButtonClickedRetrieve ?? 
                    (
                    _retryButtonClickedRetrieve = new RelayCommand(param => RetryCommandRetrieve())
                    );
            }
        }

        /// <summary>
        /// This is called when the Retry Button Next to the Transfer Text is clicked.
        /// </summary>
        private void RetryCommandRetrieve()
        {
            LogManager.DebugLogManager.MethodBegin("MessageBarViewModel.Messages_CollectionChanged");

            SendPropertyChanged("RetryRetrieve");

            LogManager.DebugLogManager.MethodEnd("MessageBarViewModel.Messages_CollectionChanged");
        }

        /// <summary>
        /// Message Listener for when the Message Collection Changes.
        /// Changes can be adding too, removing from, or clearing.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Messages_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("MessageBarViewModel.Messages_CollectionChanged");

            SendPropertyChanged("Messages");

            LogManager.DebugLogManager.MethodEnd("MessageBarViewModel.Messages_CollectionChanged");
        }
    }
}
