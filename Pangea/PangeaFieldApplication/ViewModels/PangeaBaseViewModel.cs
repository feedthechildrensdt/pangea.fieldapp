﻿/// This is the Base View Model for all of the Pangea View Models.
/// This is inherited by all of the view models and sets up the base
/// notification functionality and DependcyObject so that they dont
/// have to be set up in every view model.
/// This will also contain any other items that will be needed in all
/// view models.


using System;
using System.ComponentModel;
using System.Windows;

namespace Pangea.FieldApplication.ViewModels
{
    public class PangeaBaseViewModel : DependencyObject, INotifyPropertyChanging, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Properties
        /// Needed to implement INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Used to Raise the Property has changed event.
        /// </summary>
        /// <param name="prop">Property that has Changed.</param>
        public void SendPropertyChanged(string propertyName)
        {
            // Will not be invoked if PropertyChanged is NULL.
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        # region INotifyPropertyChanging Properties
        /// <summary>
        /// Needed to Raise the Property Changing event.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Used to Raise the Property is Changing event. Which allows anything
        /// listening to the preview event to see this property is about to change.
        /// </summary>
        public void SendPropertyChanging()
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(String.Empty));
        }
        #endregion
    }
}
