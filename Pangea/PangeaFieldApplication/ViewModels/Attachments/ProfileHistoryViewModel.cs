﻿
using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Pangea.Utilities.Base.Commands;

namespace Pangea.FieldApplication.ViewModels
{
    /// <summary>
    /// This is the View Model that is used to control the Profile History Window
    /// that will be used to show the previous profile picture of the child and 
    /// the  new profile picture being added by the User.
    /// </summary>
    public class ProfileHistoryViewModel : PangeaBaseViewModel
    {
        private PangeaFileStruct _pangeaFileStruct;

        public ProfileHistoryViewModel(PangeaFileStruct _pfs)
        {
            _pangeaFileStruct = _pfs;
        }

        /// <summary>
        /// The Previous Image
        /// </summary>
        public ImageSource HistoryImageSource
        {
            get
            {
                ImageSource _imageSrc = null;

                var the_data = PangeaInfo.ChildHistory.AdditionalResources.Find(PangeaCollections.ContentTypeCollection.Find("Profile")).FirstOrDefault().FullFileLoc;

                if (
                    the_data != String.Empty &&
                    the_data != null
                    )
                    _imageSrc = new BitmapImage(new Uri(PangeaInfo.ChildHistory.AdditionalResources.Find(PangeaCollections.ContentTypeCollection.Find("Profile")).FirstOrDefault().FullFileLoc));
                else
                    _imageSrc = DefaultImages.DefaultProfileImageSrc;

                return _imageSrc;
            }
        }

        /// <summary>
        /// The New profile image
        /// </summary>
        public ImageSource ProfileImageSource
        {
            get
            {
                ImageSource _imageSrc = null;

                if (_pangeaFileStruct.FullFileLoc != String.Empty)
                    _imageSrc = new BitmapImage(new Uri(_pangeaFileStruct.FullFileLoc));
                else
                    _imageSrc = DefaultImages.DefaultProfileImageSrc;

                return _imageSrc;
            }
        }

        /// <summary>
        /// Command used to Close the Window when the Click the Yes Button.
        /// </summary>
        private ICommand _closeWindowYesCommand;
        public ICommand CloseWindowYesCommand
        {
            get { return _closeWindowYesCommand ?? (_closeWindowYesCommand = new RelayCommand(CloseWindowYes)); }
        }

        /// <summary>
        /// Called when the Yes Button is clicked, will perform everything that needs to happen when 
        /// the Yes button is clicked.
        /// </summary>
        /// <param name="_window">>The Window being closed</param>
        private void CloseWindowYes(object _window)
        {
            Window curWindow = _window as Window;

            if (curWindow != null )
            {
                curWindow.DialogResult = true;
                curWindow.Close();
            }
        }

        /// <summary>
        /// Command used to Close the Window when the Click the No Button.
        /// </summary>
        private ICommand _closeWindowNoCommand;
        public ICommand CloseWindowNoCommand
        {
            get { return _closeWindowNoCommand ?? (_closeWindowNoCommand = new RelayCommand(CloseWindowNo)); }
        }

        /// <summary>
        /// Called when the No Button is clicked, will perform everything that needs to happen when 
        /// the No button is clicked.
        /// </summary>
        /// <param name="_window">The Window being closed</param>
        private void CloseWindowNo(object _window)
        {
            Window curWindow = _window as Window;

            if (curWindow != null)
            {
                PangeaInfo.Child.AdditionalResources.Remove(PangeaCollections.ContentTypeCollection.Find("Profile"));
                curWindow.DialogResult = false;
                curWindow.Close();
            }
        }
    }
}
