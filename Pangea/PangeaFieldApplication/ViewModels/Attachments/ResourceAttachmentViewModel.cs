﻿
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Pangea.FieldApplication.Views;
using Pangea.Utilities.Base.Commands;
using Pangea.Utilities.Tools;
using Pangea.Data.DataObjects;

namespace Pangea.FieldApplication.ViewModels
{
    /// <summary>
    /// View Model for Resource Attachment Control.
    /// The main functionality and the items visible on the control
    /// will be populated and controled from here.
    /// </summary>
    public class ResourceAttachmentViewModel : PangeaBaseViewModel
    {
        public ResourceAttachmentViewModel()
        {
            PangeaInfo.Child.AdditionalResources.CollectionChanged += AdditionalResources_CollectionChanged;

            PangeaInfo.Child.PropertyChanged += Child_PropertyChanged;

            _reqDocSelected = true;
        }

        // Bug 371 - To know which Tab we are currently on.
        // BUG 384 - Refresh of the Request Document List
        private bool _reqDocSelected;
        public bool ReqDocSelected
        {
            get { return _reqDocSelected; }
            set
            {
                if (_reqDocSelected != value)
                {
                    SendPropertyChanging();

                    _reqDocSelected = value;

                    SendPropertyChanged("ReqDocSelected");

                    SendPropertyChanged("ReqDocList");
                }
            }
        }

        /// <summary>
        /// The Click Command for when a the user wants to Upload a docuent type.
        /// </summary>
        /// <param name="sender">The Button Being Clicked</param>
        /// <param name="e">The Arguments from the button being clicked</param>
        public void UploadFileBtn_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("ResourceAttachmentViewModel.UploadFileBtn_Click");

            try
            {
                ResourceTypeSelectionWindow resourceTypeSel = new ResourceTypeSelectionWindow(!_reqDocSelected);

                resourceTypeSel.ShowDialog();
            }
            catch (Exception ex)
            {
                LogManager.DebugLogManager.LogSection("ResourceAttachmentViewModel.UploadFileBtn_Click", ex.Message);

                LogManager.ErrorLogManager.WriteError("ResourceAttachmentViewModel.UploadFileBtn_Click", ex);
#if DEBUG
                MessageBox.Show(ex.Message, "ResourceAttachmentViewModel.UploadFileBtn_Click");
#endif
            }

            LogManager.DebugLogManager.MethodEnd("ResourceAttachmentViewModel.UploadFileBtn_Click");
        }

        /// <summary>
        /// This takes the Additional Resources Collection and turns them into a custon
        /// Control to be added to a ListBox so that a small Image of the item is shown
        /// the Short File Name of the Item is shown and a button that looks like a trash
        /// can to remove the Additional Resource from the list.
        /// </summary>
        private ObservableCollection<StackPanel> _reqDocList;
        public ObservableCollection<StackPanel> ReqDocList
        {
            get
            {
                LogManager.DebugLogManager.MethodBegin("ResourceAttachmentViewModel.ReqDocList");

                if (_reqDocList == null)
                    _reqDocList = new ObservableCollection<StackPanel>();
                else
                    _reqDocList.Clear();

                foreach (PangeaFileStruct rds in PangeaInfo.Child.AdditionalResources.GetOptionalFiles(!_reqDocSelected))
                {
                    StackPanel tempSP = new StackPanel();

                    tempSP.Orientation = Orientation.Horizontal;

                    tempSP.MouseDown += DocListMouseLeftDoubleClick;

                    Image tempI = new Image();

                    tempI.Width = 40;

                    tempI.Height = 40;

                    if (PangeaCollections.FileTypeCollection.Find(rds.FileTypeInfo.CodeID).Name.ToLower().Equals("pdf"))
                    {
                        tempI.Source = DefaultImages.DocumentImageSrc;
                    }
                    else
                    {
                        System.Windows.Media.Imaging.BitmapImage tempSrc = null;

                        try
                        {
                            MemoryStream ms = new MemoryStream();

                            System.Drawing.Bitmap image = null;

                            if (rds.FullFileLoc.Length > 1)
                            {
                                image = new System.Drawing.Bitmap(rds.FullFileLoc);
                            }
                            else
                            {

                                MemoryStream ms_image = new MemoryStream(rds.BinaryFile.ToArray());

                                image = new System.Drawing.Bitmap(ms_image);
                            }
                            

                            image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);

                            ms.Seek(0, SeekOrigin.Begin);

                            tempSrc = new System.Windows.Media.Imaging.BitmapImage();

                            tempSrc.BeginInit();

                            tempSrc.StreamSource = ms;

                            tempSrc.EndInit();
                        }
                        catch (Exception ex)
                        {
                            LogManager.ErrorLogManager.WriteError("ResourceAttachmentViewModel.ReqDocList", ex);
#if DEBUG
                            MessageBox.Show(ex.Message, "ResourceAttachmentViewModel.ReqDocList");
#endif
                        }
                        finally
                        {
                            tempI.Source = tempSrc;
                        }

                    }

                    tempSP.Children.Add(tempI);

                    TextBlock tempT = new TextBlock();

                    tempT.TextAlignment = System.Windows.TextAlignment.Center;

                    tempT.Height = 40;

                    tempT.Text = rds.ShortFileName;

                    tempSP.Children.Add(tempT);

                    Image tempBImage = new Image();

                    tempBImage.Width = 20;

                    tempBImage.Height = 20;

                    tempBImage.Source = DefaultImages.TrashImageSrc;

                    Button tempB = new Button();

                    tempB.Content = tempBImage;

                    tempB.BorderBrush = System.Windows.Media.Brushes.Transparent;

                    tempB.Background = System.Windows.Media.Brushes.Transparent;

                    tempB.Command = TrashButtonClicked;

                    tempB.CommandParameter = rds;

                    tempSP.Children.Add(tempB);

                    _reqDocList.Add(tempSP);
                }

                LogManager.DebugLogManager.MethodEnd("ResourceAttachmentViewModel.ReqDocList");

                return _reqDocList;
            }
        }

        /// <summary>
        /// Opens the File that has been left mouse clicked on multiple times.
        /// </summary>
        /// <param name="sender">Stack Panel Object</param>
        /// <param name="e">The Mouse Button Events that have been triggered.</param>
        private void DocListMouseLeftDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
                System.Diagnostics.Process.Start(((PangeaFileStruct)((Button)((StackPanel)sender).Children[2]).CommandParameter).FullFileLoc);
        }

        /// <summary>
        /// This is a custom Button Click command. This allows us to
        /// call the button click command from the view model instead
        /// of the code behind the View.
        /// </summary>
        private ICommand _trashButtonClicked;
        public ICommand TrashButtonClicked
        {
            get
            {
                return _trashButtonClicked ?? (_trashButtonClicked = new RelayCommand<PangeaFileStruct>(RemoveAdditionalResourceItem));
            }
        }

        /// <summary>
        /// When the Trash Can is clicked in the Document list, this is called to remove
        /// the item associated with the button press.
        /// We then tell the view to update the Control associatd with ReqDocList
        /// </summary>
        /// <param name="item"></param>
        private void RemoveAdditionalResourceItem(PangeaFileStruct item)
        {
            LogManager.DebugLogManager.MethodBegin("ResourceAttachmentViewModel.RemoveAdditionalResourceItem");

            PangeaInfo.Child.AdditionalResources.Remove(item);

            try
            {
                EnrollmentTablesDataContext _etdc = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                _etdc.Delete_File(PangeaInfo.User.UserID, item.StreamID, PangeaInfo.Child.ChildID, item.FileTypeInfo.CodeID, item.ContentTypeInfo.CodeID, null);

                File.Delete(item.FullFileLoc);
            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("ResourceAttachmentViewModel.RemoveAdditionalResourceItem", ex);
#if DEBUG
                MessageBox.Show(ex.Message, "ResourceAttachmentViewModel.RemoveAdditionalResourceItem");
#endif
            }

            SendPropertyChanged("ReqDocList");

            LogManager.DebugLogManager.MethodEnd("ResourceAttachmentViewModel.RemoveAdditionalResourceItem");
        }

        /// <summary>
        /// This listens to the Addition Resources Collection of the Child Class for Changes.  It then throws a Property
        /// Changed event for the Corresponding View Model Property so that the changes will be reflectted
        /// to the user on the screen.
        /// </summary>
        /// <param name="sender">Additional Resources Collection</param>
        /// <param name="e">The type of Change that happened to the Collection Class. </param>
        private void AdditionalResources_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("ResourceAttachmentViewModel.AdditionalResources_CollectionChanged");

            // We do not care what happen to the collection class, we just want to refresh the ReqDocList no matter what.
            SendPropertyChanged("ReqDocList");

            PangeaInfo.SaveRequired = true;// Bug 334

            LogManager.DebugLogManager.MethodEnd("ResourceAttachmentViewModel.AdditionalResources_CollectionChanged");
        }

        /// <summary>
        /// This listens to the ChildInfo Class for Changes in its Properties.  It then throws a Property
        /// Changed event for the Corresponding View Model Property so that the changes will be reflectted
        /// to the user on the screen.
        /// </summary>
        /// <param name="sender">Child Info Class</param>
        /// <param name="e">The Property Changed Event Information.  Includes the Name of the Property that
        /// Threw the Event. </param>
        private void Child_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("ResourceAttachmentViewModel.Child_PropertyChanged");

            switch (e.PropertyName)
            {
                case "AdditionalResources":
                    PangeaInfo.Child.AdditionalResources.CollectionChanged += AdditionalResources_CollectionChanged;

                    SendPropertyChanged("ReqDocList");

                    break;
            }

            LogManager.DebugLogManager.MethodEnd("ResourceAttachmentViewModel.Child_PropertyChanged");

        }
    }
}
