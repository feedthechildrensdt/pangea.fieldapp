﻿using System.Windows;
using System.Windows.Input;

using Pangea.Data.Collections;
using Pangea.Data.Model.CodeTables;
using Pangea.Utilities.Base.Commands;

using System.Linq;
using System.Collections.Generic;


namespace Pangea.FieldApplication.ViewModels
{
    public class RetrieveChildRecordsViewModel : PangeaBaseViewModel
    {
        public RetrieveChildRecordsViewModel()
        {

        }

        public RetrieveChildRecordsViewModel(List<int> the_Approved_Country_IDs)
        {
            _the_Approved_Country_IDs = the_Approved_Country_IDs;
        }


        private List<int> _the_Approved_Country_IDs;



        /// <summary>
        /// This is the Property used to fill in the Location ComboBoxes.
        /// The Location Collection is based off of the Country Code ID
        /// </summary>
        public LocationCollection Locations
        {
            get 
            {
               
                if(_the_Approved_Country_IDs == null)
                {
                    return PangeaCollections.LocationCollection;
                }
                else
                {

                    return new LocationCollection
                        (
                        PangeaCollections
                        .LocationCollection
                        .Where
                        (
                            xx => 
                            _the_Approved_Country_IDs.Contains(xx.CountryCodeID)
                            )
                        );
                }

            }

        }

        /// <summary>
        /// This is used to Display and Update the Child Selected Location
        /// </summary>
        private Location _selectedLocation;

        public Location SelectedLocation
        {
            get 
            { 
                return _selectedLocation; 
            }
            set
            {
                if (_selectedLocation != value)
                {
                    SendPropertyChanging();

                    _selectedLocation = value;

                    SendPropertyChanged("SelectedLocation");
                }
            }
        }

        /// <summary>
        /// Command used to Close the Possible Duplicate Windows when the Click the Yes Button.
        /// </summary>
        private ICommand _closeWindowRetrieveCommand;

        public ICommand CloseWindowRetrieveCommand
        {
            get 
            { 

                return _closeWindowRetrieveCommand 
                    ?? 
                    (_closeWindowRetrieveCommand = new RelayCommand(CloseWindowRetrieve)); 

            }
        }

        /// <summary>
        /// Command used to Close the Possible Duplicate Windows when the Click the Yes Button.
        /// </summary>
        private ICommand _closeWindowCancelCommand;

        public ICommand CloseWindowCancelCommand
        {
            get 
            { 
                return _closeWindowCancelCommand 
                    ?? 
                    (_closeWindowCancelCommand = new RelayCommand(CloseWindowCancel)); 
            }
        }

        /// <summary>
        /// Called when the RETRIEVE Button is clicked, will perform everything that needs to happen when 
        /// the RETRIEVE button is clicked.
        /// </summary>
        /// <param name="_window">The Source Window</param>
        private void CloseWindowRetrieve(object _window)
        {
            // This child is a duplicate, delete it from db if it has been added and start a new child enrollment.
            Window curWindow = _window as Window;

            if (curWindow != null)
            {
                curWindow.DialogResult = true;

                curWindow.Close();
            }
        }

        /// <summary>
        /// Called when the CANCEL Button is clicked, will perform everything that needs to happen when 
        /// the CANCEL button is clicked.
        /// </summary>
        /// <param name="_window">The Source Window</param>
        private void CloseWindowCancel(object _window)
        {
            // It is not a Duplicate and we want to associate the possible duplicates with this child.
            Window curWindow = _window as Window;

            if (curWindow != null)
            {
                curWindow.DialogResult = false;

                curWindow.Close();
            }
        }
    }
}
