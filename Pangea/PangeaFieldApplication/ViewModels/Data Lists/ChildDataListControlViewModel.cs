﻿
using Pangea.Data.DataObjects;
using Pangea.Data.Model;

using System;
using System.Collections.ObjectModel;
using System.Data.Linq;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Pangea.FieldApplication.ViewModels
{
    /// <summary>
    /// This is the View Model that will control what is in the 
    /// Drop Down on the Child Data List screen and what gets 
    /// displayed in the Data Grid on the screen.
    /// </summary>
    public class ChildDataListControlViewModel : PangeaBaseViewModel
    {
        public static bool _childIsSelected;

        private bool _enrollmentListSel;

        private bool _updateListSel;

        public ChildDataListControlViewModel(bool enrollment)
        {
            _enrollmentListSel = enrollment;

            _updateListSel = !enrollment;

            _childIsSelected = false;
        }

        /// <summary>
        /// Collection of the Different Data commands
        /// for viewing different saved child records
        /// </summary>
        private ObservableCollection<string> _dataLists;

        public ObservableCollection<string> DataLists
        {
            get
            {
                if (_dataLists == null)
                {
                    _dataLists = new ObservableCollection<string>();

                    if 
                        (
                        _updateListSel &&
                        Centraler.Settingz.Allow_Records_Downloaded_DDL_Option
                        )
                        _dataLists.Add("Records Downloaded");

                    _dataLists.Add("Records in progress");

                    _dataLists.Add("Records ready to transmit");

                    _dataLists.Add("Records previously transmitted");
                }

                return _dataLists;
            }
        }

        /// <summary>
        /// The Current Data List that the user wants to view.
        /// </summary>
        private string _listSelected;

        public string ListSelected
        {
            get
            {
                return _listSelected;
            }
            set
            {
                if (_listSelected != value)
                {
                    _listSelected = value;
                    SearchText = null;
                }

                SendPropertyChanged("ListSelected");

                SendPropertyChanged("DataVis");

                SendPropertyChanged("ChildDataList");

                SendPropertyChanged("TransmitChildInfoVis");

                SendPropertyChanged("TransmitChildEnable");
            }
        }


        /// <summary>
        /// This is the Text they have Typed into the Search textbox.
        /// </summary>
        private string _searchText;

        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if (_searchText != value)
                {
                    _searchText = value;

                    SendPropertyChanged("SearchText");
                }
            }
        }

        /// <summary>
        /// The Current Selected Child in the Data Grid
        /// </summary>
        private Child _selectedChild;

        public Child SelectedChild
        {
            get { return _selectedChild; }
            set
            {
                if (_selectedChild != value)
                {
                    SendPropertyChanging();

                    _selectedChild = value;

                    SendPropertyChanged("SelectedChild");
                }
            }
        }

        /// <summary>
        /// The Child Data that is Displayed on the 
        /// Child Data Grid.
        /// Will Be Different Depending on if its Enrollment Information
        /// or Update Information
        /// </summary>
        public ObservableCollection<Child> ChildDataList
        {
            get
            {
                ObservableCollection<Child> _childDataList = new ObservableCollection<Child>();

                ISingleResult<ChildDO> _recordsInProgress = null;

                ISingleResult<GetSavedPendingsResult> _pendingSavedResults = null; // Bug 333 : Moved out of the if Statement

                Guid _tempSearchGuid;

                if 
                    (
                    _enrollmentListSel && 
                    !string.IsNullOrEmpty(_listSelected) && 
                    !string.IsNullOrWhiteSpace(_listSelected)
                    )
                {
                    EnrollmentTablesDataContext _enrollmentDataContext = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                    if (_listSelected.Equals("Records in progress"))
                        _recordsInProgress = _enrollmentDataContext.Get_Saved_Enrollments(PangeaInfo.User.UserID);

                    else if (_listSelected.Equals("Records ready to transmit"))
                        _recordsInProgress = _enrollmentDataContext.Get_Enrollment_Ready_To_Transmit(PangeaInfo.User.UserID);

                    else if (_listSelected.Equals("Records previously transmitted"))
                        _recordsInProgress = _enrollmentDataContext.Get_Enrollment_Previously_Transmitted(PangeaInfo.User.UserID);

                }
                else if 
                    (
                    _updateListSel && 
                    !string.IsNullOrEmpty(_listSelected) && 
                    !string.IsNullOrWhiteSpace(_listSelected)
                    )
                {

                    if (_listSelected.Equals("Records Downloaded"))
                    {
                        DBOTablesDataContext _dboDataContext = new DBOTablesDataContext
                            (
                            PangeaInfo.TestVer 
                            ? 
                            Centraler.Database_Settingz.DB_Staging 
                            : 
                            Centraler.Database_Settingz.DB_Production
                            );

                        ISingleResult<FieldUpdateChildResult> _tempRes = _dboDataContext.Field_Update_Child(PangeaInfo.User.UserID, null); // TODO : Add location ID from the User information

                        foreach (FieldUpdateChildResult _ch in _tempRes)
                        {
                            ISingleResult<GetASavedResult> _aSavedRes = _dboDataContext.Get_A_Saved_Child(PangeaInfo.User.UserID, _ch.Child_ID);

                            foreach (GetASavedResult _child in _aSavedRes)
                            {
                                _childDataList.Add(new Child(_ch.Child_ID, _child));

                                if (!string.IsNullOrEmpty(SearchText) && !string.IsNullOrWhiteSpace(SearchText))
                                {

                                    ObservableCollection<Child> filteredCollection = new ObservableCollection<Child>(
                                        from item in _childDataList
                                        where
                                        item.FirstName.ToLower().Contains(SearchText.ToLower())
                                        || item.LastName.ToLower().Contains(SearchText.ToLower())
                                        || item.MiddleName.ToLower().Contains(SearchText.ToLower())
                                        || item.NickName.ToLower().Contains(SearchText.ToLower())
                                        || item.ChildNumber.ToString().Contains(SearchText.ToLower())
                                        select item
                                        );

                                    _childDataList = filteredCollection;
                                }

                            }
                        }

                        _recordsInProgress = null;
                    }
                    else
                    {
                        PendingTablesDataContext _pendingDataContext = new PendingTablesDataContext(PangeaInfo.DBCon);

                        if (_listSelected.Equals("Records in progress"))
                            _pendingSavedResults = _pendingDataContext.Get_Saved_Pendings(PangeaInfo.User.UserID);

                        else if (_listSelected.Equals("Records ready to transmit"))
                            _recordsInProgress = _pendingDataContext.Get_Pending_Ready_To_Transmit(PangeaInfo.User.UserID);

                        else if (_listSelected.Equals("Records previously transmitted"))
                            _recordsInProgress = _pendingDataContext.Get_Pending_Previously_Transmitted(PangeaInfo.User.UserID);
                    }
                }

                // Bug 333 : Changed the Collection add to get the child Pending Child Info and Enrollment Child Info fully.
                if (_pendingSavedResults != null)
                {
                    foreach (GetSavedPendingsResult _recInProg in _pendingSavedResults)
                    {
                        _childDataList.Add(PangeaInfo.GetPendingChildInfo(_recInProg.Child_ID));

                        if (!string.IsNullOrEmpty(SearchText) && !string.IsNullOrWhiteSpace(SearchText))
                        {

                            ObservableCollection<Child> filteredCollection = new ObservableCollection<Child>(
                                from item in _childDataList
                                where
                                item.FirstName.ToLower().Contains(SearchText.ToLower())
                                || item.LastName.ToLower().Contains(SearchText.ToLower())
                                || item.MiddleName.ToLower().Contains(SearchText.ToLower())
                                || item.NickName.ToLower().Contains(SearchText.ToLower())
                                || item.ChildNumber.ToString().Contains(SearchText.ToLower())
                                select item
                                );

                            _childDataList = filteredCollection;
                        }
                    }
                }

                if (_recordsInProgress != null)
                {
                    foreach (ChildDO _recInProg in _recordsInProgress)
                    {
                        _childDataList.Add
                                         (
                                         _enrollmentListSel
                                         ?
                                         PangeaInfo.GetEnrollmentChildInfo(_recInProg.Child_ID)
                                         :
                                         PangeaInfo.GetPendingChildInfo(_recInProg.Child_ID)
                                         );

                        if (!string.IsNullOrEmpty(SearchText) && !string.IsNullOrWhiteSpace(SearchText))
                        {

                            ObservableCollection<Child> filteredCollection = new ObservableCollection<Child>(
                                from item in _childDataList
                                where
                                item.FirstName.ToLower().Contains(SearchText.ToLower()) 
                                || item.LastName.ToLower().Contains(SearchText.ToLower()) 
                                || item.MiddleName.ToLower().Contains(SearchText.ToLower()) 
                                || item.NickName.ToLower().Contains(SearchText.ToLower())
                                || item.ChildNumber.ToString().Contains(SearchText.ToLower())
                                select item
                                );

                            _childDataList = filteredCollection;
                        }
                    }
                }

                return _childDataList;
            }
        }

        /// <summary>
        /// Reference to the Magnifing glass of the Default Images to be
        /// populated on the Search Button.
        /// </summary>
        public ImageSource SearchBtnImgSrc
        {
            get 
            { 
                return DefaultImages.SearchImageSrc; 
            }
        }

        /// <summary>
        /// This is thrown when the selected child on the data grid changes.
        /// </summary>
        /// <param name="sender">Data Grid Object</param>
        /// <param name="e">The different selection Passed Arguments, including the selected Item</param>
        public void ChildDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                SelectedChild = e.AddedItems[0] as Child;
            }
        }

        /// <summary>
        /// This is thrown when the search button has been clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            //if (string.IsNullOrEmpty(SearchText) || string.IsNullOrWhiteSpace(SearchText))
            //    return;

            SendPropertyChanged("ChildDataList");
        }

        public void SearchTextBox_TextChanged(object sender, RoutedEventArgs e)
        {
            //if (string.IsNullOrEmpty(SearchText) || string.IsNullOrWhiteSpace(SearchText))
            //    return;

            SendPropertyChanged("ChildDataList");
        }

        /// <summary>
        /// This is thrown when the Transmit Button has been clicked.
        /// Opens the Select Child To Transmit Dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void TransmitChildBtn_Click(object sender, RoutedEventArgs e)
        {
            if (_updateListSel)
                SendPropertyChanged("TransmitButtonClicked_Update");
            else
                SendPropertyChanged("TransmitButtonClicked_Enrollment");
        }

        /// <summary>
        /// This updates the Selected Data List on the Grid after the
        /// user presses the Delete button for the Enrollment Items.
        /// </summary>
        public void UpdateDataListAfterDelete()
        {
            SelectedChild = null;

            SendPropertyChanged("ChildDataList");
        }

        /// <summary>
        /// Used to know if we shold display the Update Textbox or not.
        /// </summary>
        public Visibility UpdateInfomationVis
        {
            get
            {
                if (_updateListSel)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Used to know if we shold display the Enrollment Textbox or not.
        /// </summary>
        public Visibility EnrollmentInformationVis
        {
            get
            {
                if (_enrollmentListSel)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Shows the Transmit Button when the User has selected the Ready to Transmit drop down
        /// </summary>
        public Visibility TransmitChildInfoVis
        {
            get
            {
                if (!String.IsNullOrEmpty(ListSelected))
                {
                    if (ListSelected.Equals("Records ready to transmit"))
                        return Visibility.Visible;
                }

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Enables the Transit Button to be clicked.
        /// </summary>
        public bool TransmitChildEnable
        {
            get
            {
                if (!String.IsNullOrEmpty(ListSelected))
                    return ListSelected.Equals("Records ready to transmit");

                return false;
            }
        }

        public void MainWinVM_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch(e.PropertyName)
            {
                case "RetrieveComplete":

                    if (_updateListSel)
                        SendPropertyChanged("ChildDataList");

                    break;
                case "TransmitComplete":

                    if (_enrollmentListSel)
                        SendPropertyChanged("ChildDataList");

                    break;
            }
        }
    }
}
