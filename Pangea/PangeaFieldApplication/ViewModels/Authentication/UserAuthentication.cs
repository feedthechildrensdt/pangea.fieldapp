﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PangeaFieldApplication.ViewModels.Authentication
{
    public class AzureADUserVm
    {
        public int Id { get; set; }
        public string AccountId { get; set; }
        public string UserName { get; set; }
        public string UniqueId { get; set; }
        public string TenantId { get; set; }
        public List<string> Scopes { get; set; }
        public List<string> Permissions { get; set; }
        public bool IsExtendedLifeTimeToken { get; set; }
        public string AccessToken { get; set; }
        public string IdToken { get; set; }
        public DateTimeOffset ExtendedExpiresOn { get; set; }
        public DateTimeOffset ExpiresOn { get; set; }
        public DateTime ExpiresOnDate { get; set; }
        public int DaysToExpiry { get; set; }
        public string Environment { get; set; }
        public bool LoggedIn { get; set; }
        public DateTime FirstLoginOn { get; set; }
        public DateTime LastLoginOn { get; set; }
        public bool CanEnroll { get; set; }
        public bool CanUpdate { get; set; }
    }
}
