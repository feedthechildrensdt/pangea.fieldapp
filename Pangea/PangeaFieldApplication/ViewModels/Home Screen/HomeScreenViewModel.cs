﻿
using Pangea.Data.DataObjects;

using System.Data.Linq;
using System.Linq;

namespace Pangea.FieldApplication.ViewModels
{
    public class HomeScreenViewModel : PangeaBaseViewModel
    {
        public HomeScreenViewModel()
        { }

        /// <summary>
        /// The Number of Enrollment Children In Progress
        /// </summary>
        public int EnrollmentNumInProgress
        {
            get
            {
                EnrollmentTablesDataContext _enrollmentDataContext = new EnrollmentTablesDataContext(PangeaInfo.DBCon);

                ISingleResult<ChildDO> getSavedResults = _enrollmentDataContext.Get_Saved_Enrollments(PangeaInfo.User.UserID);

                return getSavedResults.Count();
            }
        }

        /// <summary>
        /// The number of Enrollment Ready to Be Submitted
        /// </summary>
        public int EnrollmentNumReadytoBeSubmitted
        {
            get
            {
                EnrollmentTablesDataContext _enrollmentDataContext = new EnrollmentTablesDataContext(PangeaInfo.DBCon);
                ISingleResult<ChildDO> getReadyToSubmitResults = _enrollmentDataContext.Get_Enrollment_Ready_To_Transmit(PangeaInfo.User.UserID);
                return getReadyToSubmitResults.Count();
            }
        }

        /// <summary>
        /// The Number of Updates not Started.
        /// </summary>
        public int UpdateNumNotStarted
        {
            get
            {
                                

                DBOTablesDataContext _dboDataContext = new DBOTablesDataContext
                    (
                    PangeaInfo.TestVer 
                    
                    ? 
                    Centraler.Database_Settingz.DB_Staging : 
                    Centraler.Database_Settingz.DB_Production
                    );

                ISingleResult<FieldUpdateChildResult> _tempRes = _dboDataContext.Field_Update_Child(PangeaInfo.User.UserID, null);

                return _tempRes.Count();
            }
        }

        /// <summary>
        /// The Number of Updates in Progress
        /// </summary>
        public int UpdateNumInProgress
        {
            get
            {
                PendingTablesDataContext _pendingDataContext = new PendingTablesDataContext(PangeaInfo.DBCon);
                ISingleResult<GetSavedPendingsResult> getSavedResults = _pendingDataContext.Get_Saved_Pendings(PangeaInfo.User.UserID);
                return getSavedResults.Count();
            }
        }

        /// <summary>
        /// The Number of Updates Ready to Transmit
        /// </summary>
        public int UpdateNumReadyToTransmit
        {
            get
            {
                PendingTablesDataContext _pendingDataContext = new PendingTablesDataContext(PangeaInfo.DBCon);
                ISingleResult<ChildDO> _recordsReadyToTransmit = _pendingDataContext.Get_Pending_Ready_To_Transmit(PangeaInfo.User.UserID);
                return _recordsReadyToTransmit.Count();
            }
        }

        public void Refresh()
        {
            SendPropertyChanged("EnrollmentNumInProgress");
            SendPropertyChanged("EnrollmentNumReadytoBeSubmitted");
            SendPropertyChanged("UpdateNumNotStarted");
            SendPropertyChanged("UpdateNumInProgress");
            SendPropertyChanged("UpdateNumReadyToTransmit");
        }
    }
}
