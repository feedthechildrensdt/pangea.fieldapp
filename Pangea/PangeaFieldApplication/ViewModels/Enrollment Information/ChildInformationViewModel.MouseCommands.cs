﻿using System;
using System.Windows.Controls;

namespace Pangea.FieldApplication.ViewModels
{
    /// <summary>
    /// This Class is a Partial class to pull out the Mouse Functions
    /// for the Question Mark Labels and History Labels
    /// I did this to try and keep the ChildINformationViewModel class
    /// wouldn't get to large and we could seperate out some of the 
    /// functionality.
    /// </summary>
    partial class ChildInformationViewModel
    {
        private ToolTip _toolTip;

        private bool _tooltipViewable = false;

        public void CountryQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Country is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void CountryQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void LocNameQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Location Name is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void LocNameQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void LocCodeQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Location Code is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void LocCodeQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void FNameQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "First Name is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void FNameQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void MNameQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Middle Name is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void MNameQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void LNameQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Last Name is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void LNameQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void NNameQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Other Name Child Goes by is a Required Field." };
                _toolTip.IsOpen = true;
            }
        }

        public void NNameQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void GenderQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                _toolTip = new ToolTip { Content = "Sex is a Required Field" };
                _toolTip.IsOpen = true;
            }
        }

        public void GenderQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void DOBQuestionLbl_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_tooltipViewable)
            {
                int _minAgeInDays = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMinAllowed("Age Range"));
                int _maxAgeInDays = Convert.ToInt32(PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetMaxAllowed("Age Range"));

                int _maxAgeYears = _maxAgeInDays / 365;
                _maxAgeInDays -= (_maxAgeYears * 365);

                String _maxAgeStr;

                if (_maxAgeInDays > 0)
                    _maxAgeStr = String.Format("Child can't be more than {0} years and {1} days old to enroll.", _maxAgeYears, _maxAgeInDays);
                else
                    _maxAgeStr = String.Format("Child can't be more than {0} years old to enroll.", _maxAgeYears);

                _toolTip = new ToolTip { Content = String.Format("Child must be at least {0} days old to enroll. \n {1}", _minAgeInDays, _maxAgeStr) };
                _toolTip.IsOpen = true;
            }
        }

        public void DOBQuestionLbl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_toolTip != null)
                _toolTip.IsOpen = false;
        }

        public void MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            _tooltipViewable = true;
        }

        public void MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            _tooltipViewable = false;
            if (_toolTip != null)
            {
                if (_toolTip.IsOpen)
                    _toolTip.IsOpen = false;
            }
        }
    }
}
