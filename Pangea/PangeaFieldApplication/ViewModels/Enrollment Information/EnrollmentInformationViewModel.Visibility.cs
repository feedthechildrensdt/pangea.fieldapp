﻿using System.Windows;

namespace Pangea.FieldApplication.ViewModels
{
    public partial class EnrollmentInformationViewModel
    {
        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility FavLearnAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Favorite Learning") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Favorite Learning"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility FavLearnQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Favorite Learning") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Favorite Learning"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility FavLearnHistoryVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility LivesWithAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Lives With") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Lives With"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility LivesWithQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Lives With") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Lives With"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility LivesWithHistoryVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility FavActivAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Favorite Activity") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Favorite Activity"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility FavActivQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Favorite Activity") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Favorite Activity"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility FavActivHistoryVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility SchoolGradeAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("GradeLevel") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("GradeLevel"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility SchoolGradeQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("GradeLevel") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("GradeLevel"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility SchoolGradeHistoryVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// This is always collapsed because they decided to not show it on the screen
        /// i have made it this way to it is easy to turn it back on.
        /// </summary>
        public Visibility HealthLabelVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Health Status") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Health Status"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// This is always collapsed because they decided to not show it on the screen
        /// i have made it this way to it is easy to turn it back on.
        /// </summary>
        public Visibility HealthAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Health Status") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Health Status"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// This is always collapsed because they decided to not show it on the screen
        /// i have made it this way to it is easy to turn it back on.
        /// </summary>
        public Visibility HealthQuestionkVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Health Status") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Health Status"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// This is always collapsed because they decided to not show it on the screen
        /// i have made it this way to it is easy to turn it back on.
        /// </summary>
        public Visibility HealthHistoryVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility PersonalityAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Personality Type") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Personality Type"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility PersonalityQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Personality Type") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Personality Type"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility PersonalityHistoryVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility ChoresAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Chore Range") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Chore Range"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility ChoresQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Chore Range") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Chore Range"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility ChoresHistoryVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility DisabilityAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Disability Required") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Disability Required"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility DisabilityQuestionVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Disability Required") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Disability Required"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility DisabilityHistoryVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility NumberSiblingsAstrikVis
        {
            get
            {
                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility NumberSiblingsQuestionVis
        {
            get
            {
                if ((PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Number of Brothers") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Number of Brothers")) || 
                    (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Number of Sisters") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Number of Sisters")))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// </summary>
        public Visibility NumberSiblingsHistoryVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determins if the Child number Lable Should Be Visible or not.
        /// </summary>
        public Visibility ChildNumberLblVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Child Numbers Value should be visible.
        /// </summary>
        public Visibility ChildNumberValueLblVis
        {
            get
            {
                if (PangeaInfo.UpdatingChild)
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Used to determine if we need to show the Major Life Events Labels and Buttons
        /// </summary>
        public Visibility MajorLifeEventsVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Major Life Event Required") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Major Life Event Required"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// This is always collapsed because they decided to not show it on the screen
        /// i have made it this way to it is easy to turn it back on.
        /// </summary>
        public Visibility MajorLifeAstrikVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Major Life Event Required") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Major Life Event Required"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }

        /// <summary>
        /// Determines if the Astrik next to the label should be shown of not.
        /// This is always collapsed because they decided to not show it on the screen
        /// i have made it this way to it is easy to turn it back on.
        /// </summary>
        public Visibility MajorLifeQuestionkVis
        {
            get
            {
                if (PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetActive("Major Life Event Required") && PangeaCollections.ColumnRulesCollection.GetRulesByModule(PangeaInfo.EnrollingChild).GetRequired("Major Life Event Required"))
                    return Visibility.Visible;

                return Visibility.Collapsed;
            }
        }
    }
}
