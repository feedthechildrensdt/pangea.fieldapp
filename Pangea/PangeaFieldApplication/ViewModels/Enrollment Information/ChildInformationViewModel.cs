﻿/////////////////////////////////////////////////////////////////////////
/// View Model for the General Information Control.
/// The data seen on the screen is populated by this View Model and 
/// the data changed on the screen is saved to the Buisness Objects
/// though the different properties and functions.
////////////////////////////////////////////////////////////////////////

using System;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

using Pangea.Data.Model.CodeTables;
using Pangea.Utilities.Tools;
using Pangea.Data.Collections;

namespace Pangea.FieldApplication.ViewModels
{
    public partial class ChildInformationViewModel : PangeaBaseViewModel
    {
        public ChildInformationViewModel()
        {
            LogManager.DebugLogManager.MethodBegin("ChildInformationViewModel");

            _dobStrVis = Visibility.Collapsed;

            PangeaInfo.Child.PropertyChanged += Child_PropertyChanged;
            PangeaInfo.Child.AdditionalResources.CollectionChanged += AdditionalResources_CollectionChanged;

            LogManager.DebugLogManager.MethodEnd("ChildInformationViewModel");
        }

        /// <summary>
        /// This is thrown after the General Information View has finished
        /// loading.
        /// </summary>
        /// <param name="sender">The View</param>
        /// <param name="e">The Loaded event Arguments</param>
        public void GeneralInformationControl_Loaded(object sender, RoutedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("ChildInformationViewModel.GeneralInformationControl_Loaded");

            LogManager.DebugLogManager.MethodEnd("ChildInformationViewModel.GeneralInformationControl_Loaded");
        }

        /// <summary>
        /// This is the Property used to fill in the Country ComboBox.
        /// </summary>
        public CountryCollection Countries
        {
            get
            {
                
                if (SelectedCountry != null)
                {

                    System.Collections.Generic.List<Country> the_countries = new System.Collections.Generic.List<Country>();

                    the_countries.Add(SelectedCountry);

                    CountryCollection the_CountryCollection = new CountryCollection(the_countries);

                    return the_CountryCollection;
                }
                else
                {
                    return PangeaCollections.CountryCollection_With_Just_Active;
                }

            }
        }

        /// <summary>
        /// This is used to Display and Update the Child Selected Country.
        /// </summary>
        public Country SelectedCountry
        {
            get 
            { 
                return PangeaInfo.Child.ChildCountry; 
            }

            set 
            {
                PangeaInfo.Child.ChildCountry = value; 
            }
        }

        /// <summary>
        /// This is the Property used to fill in the Location ComboBoxes.
        /// The Location Collection is based off of the Country Code ID
        /// </summary>
        public LocationCollection Locations
        {
            get
            {
                if (SelectedCountry != null)
                    return PangeaCollections.LocationCollection.GetCountryLocations(SelectedCountry.CodeID);
                else
                    return null;
            }
        }

        /// <summary>
        /// This is used to Display and Update the Child Selected Location
        /// </summary>
        public Location SelectedLocation
        {
            get { return PangeaInfo.Child.ChildLocation; }
            set { PangeaInfo.Child.ChildLocation = value; }
        }

        /// <summary>
        /// This is used to determine if the Location Combo Boxes are 
        /// enabled or not.  They are only disabled if a Country has not been
        /// Selected since we populate the Location information based on the 
        /// Country Code ID.
        /// </summary>
        public bool LocationInformationEnabled
        {
            // Bug 317 - Should just be checking if it is an Enrolling Child or not.
            get { return PangeaInfo.Child.ChildCountry != null && PangeaInfo.EnrollingChild; }
        }

        /// <summary>
        /// Will Disable and Enable fields that are not allowed 
        /// when doing a Child Update.
        /// </summary>
        public bool IsUpdating
        {
            get { return !PangeaInfo.UpdatingChild; }
        }

        /// <summary>
        /// This is the Property that is used for Displaying
        /// and updating the Childs First Name.
        /// </summary>
        public string FirstName
        {
            get { return PangeaInfo.Child.FirstName; }
            set { PangeaInfo.Child.FirstName = value; }
        }

        /// <summary>
        /// Maximum number of Characters for the First Name
        /// </summary>
        public int MaxNumCharactersFName
        {
            get { return 50; } // TODO : We need to move this to a variable that can be changed outside of compiling the Application
        }

        /// <summary>
        /// This is the Property that is used for Displaying
        /// and updating the Childs Last Name.
        /// </summary>
        public string LastName
        {
            get { return PangeaInfo.Child.LastName; }
            set { PangeaInfo.Child.LastName = value; }
        }

        /// <summary>
        /// Maximum number of Characters for the First Name
        /// </summary>
        public int MaxNumCharactersLName
        {
            get { return 50; } // TODO : We need to move this to a variable that can be changed outside of compiling the Application
        }

        /// <summary>
        /// This is the Property that is used for Displaying
        /// and updating the Childs Middle Name.
        /// </summary>
        public string MiddleName
        {
            get { return PangeaInfo.Child.MiddleName; }
            set { PangeaInfo.Child.MiddleName = value; }
        }

        /// <summary>
        /// Maximum number of Characters for the First Name
        /// </summary>
        public int MaxNumCharactersMName
        {
            get { return 50; } // TODO : We need to move this to a variable that can be changed outside of compiling the Application
        }

        /// <summary>
        /// This is the Value used to update the Other Name
        /// Child Goes by field on the View.
        /// </summary>
        public string NickName
        {
            get { return PangeaInfo.Child.NickName; }
            set { PangeaInfo.Child.NickName = value; }
        }

        /// <summary>
        /// This is the Value used if Year only is not selected.
        /// </summary>
        public DateTime? DateOfBirth
        {
            get { return PangeaInfo.Child.DateOfBirth; }
            set
            {
                if (value <= DateTime.Today)
                    PangeaInfo.Child.DateOfBirth = value;
                else
                    SendPropertyChanged("DateOfBirth");
            }
        }

        /// <summary>
        /// This is the Value used for if Year only is selected.
        /// This Converts the Date of Birth to year only to be displayed
        /// and sets the value to be 1/1/Year.
        /// </summary>
        public String DateOfBirthStr
        {
            get
            {
                if (YearOnly)
                    return PangeaInfo.Child.DateOfBirth.HasValue ? PangeaInfo.Child.DateOfBirth.Value.Year.ToString() : String.Empty;

                return PangeaInfo.Child.DateOfBirth.HasValue ? PangeaInfo.Child.DateOfBirth.Value.ToUniversalTime().Date.ToShortDateString() : String.Empty;
            }
            set
            {
                if (String.IsNullOrEmpty(value) || String.IsNullOrWhiteSpace(value))
                {
                    PangeaInfo.Child.DateOfBirth = null;
                }
                else
                {
                    if (YearOnly)
                    {
                        if (new DateTime(Convert.ToInt32(value), 1, 1).ToUniversalTime().Date.Year <= DateTime.Today.ToUniversalTime().Date.Year)
                            PangeaInfo.Child.DateOfBirth = new DateTime(Convert.ToInt32(value), 1, 1).ToUniversalTime();
                    }
                    else
                    {
                        if (DateTime.Parse(value).ToUniversalTime().Date <= DateTime.Today.ToUniversalTime().Date)
                            PangeaInfo.Child.DateOfBirth = DateTime.Parse(value);
                    }
                }

                SendPropertyChanged("DateOfBirthStr");
            }
        }

        /// <summary>
        /// This Determines if the Calendar functionality of the
        /// Date of Birth is clickable or if only the TextBox to put
        /// a Year is Visable.
        /// </summary>
        private bool _YearOnly;
        public bool YearOnly
        {
            get { return _YearOnly; }
            set
            {
                if (_YearOnly != value)
                {
                    SendPropertyChanging();
                    _YearOnly = value;
                    DobStrVisibility = _YearOnly ? Visibility.Visible : Visibility.Collapsed;
                    DobVisibility = _YearOnly ? Visibility.Collapsed : Visibility.Visible;
                    SendPropertyChanged("YearOnly");
                    SendPropertyChanged("DateOfBirthStr");
                }
            }
        }

        /// <summary>
        /// This is used to populate the ComboBox for the 
        /// gender information.
        /// </summary>
        public GenderCollection Genders
        {
            get { return PangeaCollections.GenderCollection; }
        }

        /// <summary>
        /// This is the Value used by the View to display and update
        /// the Child gender.
        /// </summary>
        public Gender SelectedGender
        {
            get { return PangeaInfo.Child.Gender; }
            set { PangeaInfo.Child.Gender = value; }
        }

        /// <summary>
        /// The Child Image Source.  This is Used to display the Child Image on the Screen.
        /// </summary>
        private ImageSource _imageSrc;
        public ImageSource ImageSrc
        {
            get
            {
                try
                {
                    // BUG 383 - Modified to fix the Update Child to not have the Profile Image Change to the new Profile Image. - JDM
                    if (PangeaInfo.EnrollingChild)
                    {
                        if (PangeaInfo.Child.AdditionalResources.Contains(PangeaCollections.ContentTypeCollection.Find("Profile")))
                            _imageSrc = new BitmapImage(new Uri(PangeaInfo.Child.AdditionalResources.Find(PangeaCollections.ContentTypeCollection.Find("Profile")).FirstOrDefault().FullFileLoc));
                        else
                            _imageSrc = DefaultImages.DefaultProfileImageSrc;
                    }
                    else if (PangeaInfo.UpdatingChild)
                    {
                        if (PangeaInfo.ChildHistory.AdditionalResources.Contains(PangeaCollections.ContentTypeCollection.Find("Profile")))
                            _imageSrc = new BitmapImage(new Uri(PangeaInfo.ChildHistory.AdditionalResources.Find(PangeaCollections.ContentTypeCollection.Find("Profile")).FirstOrDefault().FullFileLoc));
                        else
                            _imageSrc = DefaultImages.DefaultProfileImageSrc;
                    }
                }
                catch (Exception ex)
                {
                    LogManager.ErrorLogManager.WriteError("ChildInformationViewModel.ImageSrc", ex);

#if DEBUG
                    MessageBox.Show(ex.Message, "ChildInformationViewModel.ImageSrc");
#endif
                    _imageSrc = DefaultImages.DefaultProfileImageSrc;
                }

                return _imageSrc;
            }
        }

        /// <summary>
        /// The Child ID to be displayed under the Childs Image.
        /// </summary>
        public String ChildIDStr
        {
            get { return PangeaInfo.Child.ChildID.HasValue ? PangeaInfo.Child.ChildID.Value.ToString() : String.Empty; }
        }

        /// <summary>
        /// The Child Number to be displayed under the Childs Image.
        /// </summary>
        public String ChildNumberStr
        {
            get
            {
                return PangeaInfo.Child.ChildNumber.HasValue ? PangeaInfo.Child.ChildNumber.Value.ToString() : String.Empty;
            }
        }

        /// <summary>
        /// This listens to the ChildInfo Class for Changes in its Properties.  It then throws a Property
        /// Changed event for the Corresponding View Model Property so that the changes will be reflectted
        /// to the user on the screen.
        /// </summary>
        /// <param name="sender">Child Info Class</param>
        /// <param name="e">The Property Changed Event Information.  Includes the Name of the Property that
        /// Threw the Event. </param>
        private void Child_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("ChildInformationViewModel.Child_PropertyChanged");

            switch (e.PropertyName)
            {
                case "ChildNumber":
                    SendPropertyChanged("ChildNumberStr");
                    break;
                case "ChildID":
                    SendPropertyChanged("ChildIDStr");
                    break;
                case "ChildCountry":
                    SendPropertyChanged("SelectedCountry");
                    SendPropertyChanged("CoutryAstrikVis");
                    SendPropertyChanged("Locations");
                    SendPropertyChanged("LocationInformationEnabled");
                    break;
                case "ChildLocation":
                    SendPropertyChanged("SelectedLocation");
                    SendPropertyChanged("LocAstrikVis");
                    SendPropertyChanged("Locations");
                    SendPropertyChanged("LocationInformationEnabled");
                    break;
                case "FirstName":
                    SendPropertyChanged("FirstName");
                    SendPropertyChanged("FNameAstrikVis");
                    break;
                case "LastName":
                    SendPropertyChanged("LastName");
                    SendPropertyChanged("LNameAstrikVis");
                    break;
                case "MiddleName":
                    SendPropertyChanged("MiddleName");
                    break;
                case "NickName":
                    SendPropertyChanged("NickName");
                    break;
                case "DateOfBirth":
                    SendPropertyChanged("DateOfBirth");
                    SendPropertyChanged("DateOfBirthStr");
                    SendPropertyChanged("DOBAstrikVis");
                    break;
                case "Gender":
                    SendPropertyChanged("SelectedGender");
                    SendPropertyChanged("GenderAstrikVis");
                    break;
                case "AdditionalResources":
                    PangeaInfo.Child.AdditionalResources.CollectionChanged += AdditionalResources_CollectionChanged;
                    SendPropertyChanged("ImageSrc");
                    break;
            }

            LogManager.DebugLogManager.MethodEnd("ChildInformationViewModel.Child_PropertyChanged");
        }

        /// <summary>
        /// This listens to the Addition Resources Collection of the Child Class for Changes.  It then throws a Property
        /// Changed event for the Corresponding View Model Property so that the changes will be reflectted
        /// to the user on the screen.
        /// </summary>
        /// <param name="sender">Additional Resources Collection</param>
        /// <param name="e">The type of Change that happened to the Collection Class. </param>
        private void AdditionalResources_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            LogManager.DebugLogManager.MethodBegin("ChildInformationViewModel.AdditionalResources_CollectionChanged");

            SendPropertyChanged("ImageSrc");

            LogManager.DebugLogManager.MethodEnd("ChildInformationViewModel.AdditionalResources_CollectionChanged");
        }
    }
}
