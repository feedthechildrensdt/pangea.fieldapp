﻿/////////////////////////////////////////////////////////////////////////
/// View Model for the Main Window.
/// This controls the language changing functionality.
////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;

using Pangea.Utilities.CustomControls.LanguageControl;
using Pangea.Utilities.CustomControls.LanguageControl.Entities;
using Pangea.Utilities.Tools;
using System.Reflection;
using System.Windows;

namespace Pangea.FieldApplication.ViewModels
{
    public partial class MainWindowViewModel : PangeaBaseViewModel
    {
        
        private string _uiLanguageCode;

        public event EventHandler LanguageChanged;

        /// <summary>
        /// Collection of all the Languages in the SupportedLanguages.xml file.
        /// </summary>
        public ObservableCollection<String> LanguageList
        {
            get { return new ObservableCollection<string>(UILanguageDefn.AllSupportedLanguageCodes.AsQueryable<String>()); }
        }

        /// <summary>
        /// The Current Language Definition file.
        /// </summary>
        public UILanguageDefn CurrentLanguage
        {
            get { return PangeaInfo.UILanguageDefMapping; }
        }

        /// <summary>
        /// The Current Selected Language.
        /// </summary>
        public String CurrentLanguageCode
        {
            get { return _uiLanguageCode; }
            set
            {
                if (_uiLanguageCode != value)
                {
                    _uiLanguageCode = value;

                    RefreshUILanguage();

                    SendPropertyChanged("CurrentLanguageCode");
                }
            }
        }

        /// <summary>
		/// Updates the UI language data from that defined in the corresponding language file.
		/// </summary>
        public void LoadSuppotedLanguages()
        {
            LogManager.DebugLogManager.MethodBegin("LoadSuppotedLanguages");

            try
            {
                // Can't be an Manifest resource Stream, needs to be saved in a folder somewhere.
                string resourcePath = String.Format("PangeaFieldApplication.LanguageData.SupportedLanguages.xml");

                System.IO.Stream supportedLanguageFile = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourcePath);

                //string resourcePath = String.Format("{0}\\LanguageData\\SupportedLanguages.xml", System.IO.Directory.GetCurrentDirectory());
                //System.IO.Stream supportedLanguageFile = new System.IO.FileStream(resourcePath, System.IO.FileMode.Open);

                XmlDocument supportedLanguages = new XmlDocument();

                supportedLanguages.Load(supportedLanguageFile);

                UILanguageDefn.LoadLanguageNames(supportedLanguages.DocumentElement);

                supportedLanguageFile.Close();
            }
            catch (Exception ex)
            {
                LogManager.DebugLogManager.LogSection("LoadSuppotedLanguages", ex.Message);
#if DEBUG
                MessageBox.Show(ex.Message, "LoadSuppotedLanguages");
#endif
            }
            LogManager.DebugLogManager.MethodEnd("LoadSuppotedLanguages");
        }

        /// <summary>
        /// Starts the Refreshing of all the items that have the Different Language Information.
        /// </summary>
        public void RefreshUILanguage()
        {
            LogManager.DebugLogManager.MethodBegin("RefreshUILanguage");

            UpdateLanguageData();

            PangeaLanguageControl.Current.LanguageDefn = CurrentLanguage;

            //Notify any other internal logic to prompt a refresh (as necessary)
            LanguageChanged?.Invoke(this, new EventArgs());

            LogManager.DebugLogManager.MethodEnd("RefreshUILanguage");
        }

        /// <summary>
        /// Updates the UI language data from that defined in the corresponding language file.
        /// </summary>
        public void UpdateLanguageData()
        {
            LogManager.DebugLogManager.MethodBegin("UpdateLanguageData");

            try
            {
                string languageCode = CurrentLanguageCode;

                if (String.IsNullOrEmpty(languageCode)) 
                    return;

                //This follows a convention for language definition files to be named 'LangXX.xml' (or 'LangXX-XX.xml')
                // where XX is the ISO language code.
                // Can't be an Manifest resource Stream, needs to be saved in a folder somewhere.
                string resourcePath = String.Format("PangeaFieldApplication.LanguageData.Lang{0}.xml", languageCode.ToUpper());

                System.IO.Stream file = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourcePath);

                //string resourcePath = String.Format("{0}\\LanguageData\\Lang{1}.xml", System.IO.Directory.GetCurrentDirectory(), languageCode.ToUpper());
                //if (!System.IO.File.Exists(resourcePath)) return;
                //System.IO.Stream file = new System.IO.FileStream(resourcePath, System.IO.FileMode.Open);

                XmlDocument languageData = new XmlDocument();

                languageData.Load(file);


                PangeaInfo.UILanguageDefMapping = new UILanguageDefn();

                PangeaInfo.UILanguageDefMapping.LoadLanguageData(languageData.DocumentElement);

                file.Close();
            }
            catch (Exception ex)
            {
                LogManager.DebugLogManager.LogSection("UpdateLanguageData", ex.Message);
#if DEBUG
                MessageBox.Show(ex.Message, "UpdateLanguageData");
#endif
            }

            LogManager.DebugLogManager.MethodEnd("UpdateLanguageData");
        }
    }
}
