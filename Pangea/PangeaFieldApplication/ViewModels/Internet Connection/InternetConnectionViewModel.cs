﻿using System.Windows;
using System.Windows.Media;

namespace Pangea.FieldApplication.ViewModels
{
    public class InternetConnectionViewModel : PangeaBaseViewModel
    {
        public InternetConnectionViewModel()
        { }

        /// <summary>
        /// This the download Connection Speed Calculated by the
        /// System.
        /// </summary>
        private double _downloadSpeed;
        public double DownloadSpeed
        {
            get { return _downloadSpeed; }
            set
            {
                if (_downloadSpeed != value)
                {
                    SendPropertyChanging();
                    _downloadSpeed = value;
                    SendPropertyChanged("DownloadSpeed");
                    SendPropertyChanged("Bar1RetFillColor");
                    SendPropertyChanged("Bar2RetFillColor");
                    SendPropertyChanged("Bar3RetFillColor");
                    SendPropertyChanged("Bar4RetFillColor");
                    SendPropertyChanged("Bar5RetFillColor");
                }
            }
        }

        public SolidColorBrush Bar1RetFillColor
        {
            get
            {
                if (DownloadSpeed > 0.0)
                    return new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f58021"));

                return new SolidColorBrush(Colors.LightSlateGray);
            }
        }

        public SolidColorBrush Bar2RetFillColor
        {
            get
            {
                if (DownloadSpeed >= 2.0)
                    return new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f58021"));

                return new SolidColorBrush(Colors.LightSlateGray);
            }
        }

        public SolidColorBrush Bar3RetFillColor
        {
            get
            {
                if (DownloadSpeed >= 3.0)
                    return new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f58021"));

                return new SolidColorBrush(Colors.LightSlateGray);
            }
        }

        public SolidColorBrush Bar4RetFillColor
        {
            get
            {
                if (DownloadSpeed >= 4.0)
                    return new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f58021"));

                return new SolidColorBrush(Colors.LightSlateGray);
            }
        }

        public SolidColorBrush Bar5RetFillColor
        {
            get
            {
                if (DownloadSpeed >= 5.0)
                    return new SolidColorBrush((Color)ColorConverter.ConvertFromString("#f58021"));

                return new SolidColorBrush(Colors.LightSlateGray);
            }
        }
    }
}
