﻿
using System;
using System.Net;


using Pangea.API.FeedService;


using Pangea.Utilities.Tools;


namespace Pangea.API
{
    public class Users
    {
        public static bool GetUserRules()
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.GetUserRules");

            bool retVal = true;

            BasicHttpsBinding_IFeedService _feedServiceAPI = new BasicHttpsBinding_IFeedService();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            try
            {
                UserPermissions user = new UserPermissions();

                user.AppName = "Pangea";

                user.AppShortName = "Pan";

                user.UserID = PangeaInfo.User.UserID.ToString(); // GUID

                ;

                if (Centraler.Settingz.Use_FEEDServiceAPI_UserPermissions)
                {
                    user = _feedServiceAPI.GetUserPermissions(user);

                    if (user.Permissions[0].Name.ToLower().Contains("error"))
                    {
                        // Error getting the Permissions of the User.
                        // Load Previous User.  If no user report needing to relogin or get permissions
                    }
                    else
                    {
                        // Set the User Permissions.
                        foreach (Permission _perm in user.Permissions)
                        {
                            if (_perm.Name.ToLower().Contains("enrollment"))
                                PangeaInfo.User.CanEnroll = true;

                            if (_perm.Name.ToLower().Contains("update"))
                                PangeaInfo.User.CanUpdate = true;
                        }
                    }
                }
                else
                {
                    PangeaInfo.User.CanEnroll = true;
                    
                    PangeaInfo.User.CanUpdate = true;
                }

            }
            catch (Exception ex)
            {
                LogManager.ErrorLogManager.WriteError("PangeaAPI.GetUserRules", ex);
            }

            _feedServiceAPI.Dispose();

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.GetUserRules");

            return retVal;
        }
    }
}
