﻿using System;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;

using Pangea.Utilities.Tools;

using Pangea.API.FeedService;


using System.Text;
using System.Security.Cryptography;

namespace Pangea.API
{
    public class Connection
    {
        /// <summary>
        /// This is used to test if we have Internet Connection.  It will return the KB/S of the Internet speed.
        /// </summary>
        /// <returns>KB/S of the download speed.</returns>
        public static double TestConnection()
        {
            LogManager.DebugLogManager.MethodBegin("PangeaAPI.TestConnection");

            double kbsec = 0.0;

            BasicHttpsBinding_IFeedService _feedServiceApiConnection = new BasicHttpsBinding_IFeedService();

            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            try
            {
                if (NetworkInterface.GetIsNetworkAvailable())
                {
                    ConnectivityTestObject _conTestObj = new ConnectivityTestObject();

                    using (MemoryStream ms = new MemoryStream())
                    {
                        Properties.Resources.ConnectionTestObject.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);

                        _conTestObj.FileBytes = ms.ToArray();
                    }

                    _conTestObj.OriginSentSpecified = true;

                    _conTestObj.OriginSent = DateTime.Now;
                    
                    ;

                    if (Centraler.Settingz.Do_Login)
                    {

                        ;

                        ConnectivityTestObject _results = new ConnectivityTestObject();

                        DateTime dt_now = DateTime.Now;

                        if (Centraler.Settingz.Use_API_Login_Data_And_Not_Mocked_Login_Data)
                        {
                            _results = _feedServiceApiConnection.TestConnectivity(_conTestObj);
                        }
                        else
                        {

                            _results.FileBytes = new byte [] { 1, 2, 3 };

                            _results.GoodFile = true;
                            
                            _results.GoodFileSpecified = true;

                            _results.OriginSent = dt_now;

                            _results.OriginSentSpecified = true;

                            _results.ServerRecive = dt_now.AddMilliseconds(123);

                            _results.ServerReciveSpecified = true;

                            _results.ServerSent = dt_now.AddMilliseconds(56);

                            _results.ServerSentSpecified = true;                            

                        }

                        // Use_Mocked_Login_Data

                        DateTime clientRec = DateTime.Now;

                        double ServerRecMS = (((((_results.ServerRecive.Hour * 60.0) * 60.0) + (_results.ServerRecive.Minute * 60.0) + _results.ServerRecive.Second) * 1000.0) + _results.ServerRecive.Millisecond);

                        double serverSentMS = (((((_results.ServerSent.Hour * 60.0) * 60.0) + (_results.ServerSent.Minute * 60.0) + _results.ServerSent.Second) * 1000.0) + _results.ServerSent.Millisecond);

                        double clientSentMS = (((((_results.OriginSent.Hour * 60.0) * 60.0) + (_results.OriginSent.Minute * 60.0) + _results.OriginSent.Second) * 1000.0) + _results.OriginSent.Millisecond);

                        double clientRecMS = (((((clientRec.Hour * 60.0) * 60.0) + (clientRec.Minute * 60.0) + clientRec.Second) * 1000.0) + clientRec.Millisecond);

                        double uploadSecs = (ServerRecMS - clientSentMS) / 1000.0;

                        double downloadSecs = (clientRecMS - serverSentMS) / 1000.0;

                        kbsec = Math.Round(1024.0 / downloadSecs);

                    }

                }
            }
            catch (Exception ex)
            {                
                LogManager.ErrorLogManager.WriteError("PangeaAPI.TestConnection", ex);

                kbsec = 0.0;
            }

            _feedServiceApiConnection.Dispose();

            LogManager.InfoLogManager.WriteLog(kbsec.ToString());

            LogManager.DebugLogManager.MethodEnd("PangeaAPI.TestConnection");

            return (kbsec / 1000.0);
        }

        public static String Encrypt(String orgText, String passwordHash, String saltKey, String viKey)
        {
            if (String.IsNullOrEmpty(orgText))
                return String.Empty;

            byte[] plainTextBytes = Encoding.UTF8.GetBytes(orgText);

            byte[] keyBytes = new Rfc2898DeriveBytes(passwordHash, Encoding.ASCII.GetBytes(saltKey)).GetBytes(256/8);

            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };

            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(viKey));

            byte[] cipherTextBytes;

            using (var memStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

                    cryptoStream.FlushFinalBlock();

                    cipherTextBytes = memStream.ToArray();

                    cryptoStream.Close();
                }
                memStream.Close();
            }

            return Convert.ToBase64String(cipherTextBytes);
        }

        public static String Decrypt(String encryptText, String passwordHash, String saltKey, String viKey)
        {
            if (String.IsNullOrEmpty(encryptText))
                return String.Empty;

            byte[] cipherTextBytes = Convert.FromBase64String(encryptText);

            byte[] keyBytes = new Rfc2898DeriveBytes(passwordHash, Encoding.ASCII.GetBytes(saltKey)).GetBytes(256 / 8);

            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(viKey));

            var memStream = new MemoryStream(cipherTextBytes);

            var cryptoStream = new CryptoStream(memStream, decryptor, CryptoStreamMode.Read);

            byte[] plainTextBytes = new byte[cipherTextBytes.Length];


            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);

            memStream.Close();

            cryptoStream.Close();

            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }
    }
}
