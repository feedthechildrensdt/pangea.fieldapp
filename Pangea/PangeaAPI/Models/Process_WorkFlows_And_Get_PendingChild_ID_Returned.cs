﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pangea.API.Models
{

    public class Process_WorkFlows_And_Get_PendingChild_ID_Returned
    {

        public bool Step_1_Succeeded;

        public bool Step_2_Succeeded;

        public bool Both_Steps_Succeeded
        {

            get
            {
                return Step_1_Succeeded && Step_2_Succeeded;
            } 
            
        }

        public int? PendingChild_ID;

    }

}
