﻿////////////////////////////////////////////////////////////////
/// This is a Generic Event Arguments for the Custom ICommand
/// Relay Commands.
////////////////////////////////////////////////////////////////

using System;

namespace Pangea.Utilities.Base.Commands
{
    public class EventArgs<T> : EventArgs
    {
        public EventArgs(T value)
        {
            Value = value;
        }

        public T Value { get; private set; }
    }
}
