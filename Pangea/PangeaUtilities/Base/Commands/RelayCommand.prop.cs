﻿////////////////////////////////////////////////////////////////////////////////
/// This is a custom interface of the ICommand properties.  The ICommands are 
/// used to allow Binding of different types of commands to different Actions
/// of Controls.  These Generic Command Classes reduce the rewriting of different
/// values in the classes that use custom ICommands.
////////////////////////////////////////////////////////////////////////////////

using System;
using System.Windows.Input;

namespace Pangea.Utilities.Base.Commands
{
    public class RelayCommand<T> : ICommand
    {
        private readonly Predicate<T> _canExecute;

        private readonly Action<T> _execute;

        public RelayCommand(Action<T> execute)
            : this(execute, null)
        { }

        public RelayCommand(Action<T> execute, Predicate<T> canExecute)
        {
            _execute = execute;
            _canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return _canExecute?.Invoke((T)parameter) ?? true;
        }

        public void Execute(object parameter)
        {
            _execute?.Invoke((T)parameter);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }

            remove { CommandManager.RequerySuggested -= value; }
        }
    }
}
