﻿using System;
using System.Windows.Data;
using System.Globalization;

using Pangea.Utilities.CustomControls.LanguageControl.Entities;

namespace Pangea.Utilities.CustomControls.LanguageControl.Converters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class LanguageNameConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string currentLang = value as string;

            string langName = UILanguageDefn.GetLanguageName(currentLang);

            return langName;
        }



        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
