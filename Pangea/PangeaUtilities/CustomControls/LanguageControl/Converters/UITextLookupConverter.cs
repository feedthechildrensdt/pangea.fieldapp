﻿/*
 * RePaver
 * 
 * Copyright (c)2009, Daniel McGaughran
 * 
 * Licence:	CodeProject Open Licence (CPOL) 1.2
 *			Please refer to 'Licence.txt' for further details of this licence.
 *			Alternatively, the licence may be viewed at:
 *			http://www.codeproject.com/info/cpol10.aspx
 * 
 */

using System;
using System.Windows.Data;
using System.Globalization;

using Pangea.Utilities.CustomControls.LanguageControl.Entities;

namespace Pangea.Utilities.CustomControls.LanguageControl.Converters
{
    [ValueConversion(typeof(UILanguageDefn), typeof(string))]
    class UITextLookupConverter : IValueConverter
    {
        private static UITextLookupConverter _sharedConverter;



        static UITextLookupConverter()
        {
            _sharedConverter = new UITextLookupConverter();
        }



        public static Binding CreateBinding(string key)
        {
            Binding languageBinding = new Binding("LanguageDefn")
            {
                Source = PangeaLanguageControl.Current,
                Converter = _sharedConverter,
                ConverterParameter = key,
            };
            return languageBinding;
        }



        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string key = parameter as string;
            UILanguageDefn defn = value as UILanguageDefn;

            if (defn == null || key == null) return "";

            return defn.GetTextValue(key);
        }



        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }



        /// <summary>
        /// Gets the localised UI text for the given key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The localised text.</returns>
        internal static string GetText(string key)
        {
            UILanguageDefn languageDefn = PangeaLanguageControl.Current.LanguageDefn;
            if (languageDefn == null || String.IsNullOrEmpty(key))
                return "";

            return languageDefn.GetTextValue(key);
        }
    }
}
