﻿/*
 * RePaver
 * 
 * Copyright (c)2009, Daniel McGaughran
 * 
 * Licence:	CodeProject Open Licence (CPOL) 1.2
 *			Please refer to 'Licence.txt' for further details of this licence.
 *			Alternatively, the licence may be viewed at:
 *			http://www.codeproject.com/info/cpol10.aspx
 * 
 */

using System;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Markup;

using Pangea.Utilities.CustomControls.LanguageControl.Converters;

namespace Pangea.Utilities.CustomControls.LanguageControl.Extensions
{
    public class LocalisedTextExtension : MarkupExtension
    {
        private Binding _lookupBinding;


        public LocalisedTextExtension()
        {
            _lookupBinding = UITextLookupConverter.CreateBinding("");
        }



        [DefaultValue("")]
        public string Key
        {
            get { return (string)_lookupBinding.ConverterParameter; }
            set { _lookupBinding.ConverterParameter = value; }
        }



        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return _lookupBinding.ProvideValue(serviceProvider);
        }
    }
}
