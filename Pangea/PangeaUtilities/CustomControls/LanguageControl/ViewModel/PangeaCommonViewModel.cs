﻿/*
 * RePaver
 * 
 * Copyright (c)2009, Daniel McGaughran
 * 
 * Licence:	CodeProject Open Licence (CPOL) 1.2
 *			Please refer to 'Licence.txt' for further details of this licence.
 *			Alternatively, the licence may be viewed at:
 *			http://www.codeproject.com/info/cpol10.aspx
 * 
 */

using System;
using System.ComponentModel;

using Pangea.Utilities.CustomControls.LanguageControl.Entities;

namespace Pangea.Utilities.CustomControls.LanguageControl
{
    public class PangeaLanguageControl : INotifyPropertyChanging, INotifyPropertyChanged
    {
        private static PangeaLanguageControl _current;

        private UILanguageDefn _languageDefn;


        private PangeaLanguageControl()
        {
            _languageDefn = null;
        }


        /// <summary>
		/// Gets the current instance of this singleton.
		/// </summary>
		/// <value>The current instance.</value>
        public static PangeaLanguageControl Current
        {
            get
            {
                if (_current == null)
                    _current = new PangeaLanguageControl();

                return _current;
            }
        }

        /// <summary>
        /// The Current Loaded language Definition
        /// This is a link to the read in XML language information.
        /// </summary>
        public UILanguageDefn LanguageDefn
        {
            get { return _languageDefn; }
            set
            {
                if (_languageDefn != value)
                {
                    _languageDefn = value;
                    SendPropertyChanged("LanguageDefn");
                    SendPropertyChanged("HeadingFontSize");
                    SendPropertyChanged("MinFontSize");
                    SendPropertyChanged("IsRightToLeft");
                }
            }
        }

        /// <summary>
        /// This is the Heading Font Size found in the Language XML file.
        /// </summary>
        public double HeadingFontSize
        {
            get
            {
                if (_languageDefn != null)
                    return (double)_languageDefn.HeadingFontSize;

                return (double)UILanguageDefn.DefaultHeadingFontSize;
            }
        }


        /// <summary>
        /// This is the Min Font Size found in the Language XML File.
        /// </summary>
        public double MinFontSize
        {
            get
            {
                if (_languageDefn != null)
                    return (double)_languageDefn.MinFontSize;

                return (double)UILanguageDefn.DefaultMinFontSize;
            }
        }


        /// <summary>
        /// This tells us if the Language in the XML Language file is read from Right to Left.
        /// </summary>
        public bool IsRightToLeft
        {
            get
            {
                if (_languageDefn != null)
                    return _languageDefn.IsRightToLeft;

                return false;
            }
        }


        #region INotifyPropertyChanged Properties
        /// Needed to implement INotifyPropertyChanged.
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Used to Raise the Property has changed event.
        /// </summary>
        /// <param name="prop">Property that has Changed.</param>
        public void SendPropertyChanged(string propertyName)
        {
            // Will not be invoked if PropertyChanged is NULL.
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        # region INotifyPropertyChanging Properties
        /// <summary>
        /// Needed to Raise the Property Changing event.
        /// </summary>
        public event PropertyChangingEventHandler PropertyChanging;

        /// <summary>
        /// Used to Raise the Property is Changing event. Which allows anything
        /// listening to the preview event to see this property is about to change.
        /// </summary>
        public void SendPropertyChanging()
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(String.Empty));
        }
        #endregion
    }
}
