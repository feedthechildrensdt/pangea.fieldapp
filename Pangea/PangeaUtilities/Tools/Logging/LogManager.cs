﻿//////////////////////////////////////////////////////////////////////////////////
/// The Main Logging Class.  This Keeps Static Instances of the LogDebug class and
/// LogError class so that they dont have to be instantuatd in every class of the
/// project.
/// Allows use to do LogManager.LogDebug.[MethodName] to use the Logging Functions
//////////////////////////////////////////////////////////////////////////////////

using Pangea.Utilities.Tools.Logging;

namespace Pangea.Utilities.Tools
{
    public class LogManager
    {
        /// <summary>
        /// Reference to the Debug Logging Class.
        /// </summary>
        private static LogDebug _debugLogManager;
        public static LogDebug DebugLogManager
        {
            get
            {
                if (_debugLogManager == null)
                    _debugLogManager = new LogDebug();

                return _debugLogManager;
            }
        }

        /// <summary>
        /// Reference to the Error Logging Class.
        /// </summary>
        private static LogError _errorLogManager;
        public static LogError ErrorLogManager
        {
            get
            {
                if (_errorLogManager == null)
                    _errorLogManager = new LogError();

                return _errorLogManager;
            }
        }

        /// <summary>
        /// Reference to the Information Logging Class.
        /// </summary>
        private static LogInfo _infoLogManager;
        public static LogInfo InfoLogManager
        {
            get
            {
                if (_infoLogManager == null)
                    _infoLogManager = new LogInfo();

                return _infoLogManager;
            }
        }

        /// <summary>
        /// Reference to Event Logging Manager
        /// </summary>
        private static LogEvent _eventLogManager;
        public static LogEvent EventLogManager
        {
            get
            {
                if (_eventLogManager == null)
                    _eventLogManager = new LogEvent();

                return _eventLogManager;
            }
        }
    }
}
