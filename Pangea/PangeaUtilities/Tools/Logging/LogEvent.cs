﻿using System;
using System.Diagnostics;

namespace Pangea.Utilities.Tools
{
    public class LogEvent
    {
        public LogEvent()
        {

            try
            {

                if (!EventLog.SourceExists("PangeaFieldAppInstaller"))
                {
                    EventLog.CreateEventSource("PangeaFieldAppInstaller", "PangeaFieldAppInstaller");
                }

            }
            catch(Exception ee)
            {
                
            }
            
        }

        /// <summary>
        /// Writes a message to the Event Log.
        /// </summary>
        /// <param name="msg">Message to write</param>
        public void WriteEventLog(String msg)
        {

            try
            {

                if (EventLog.SourceExists("PangeaFieldAppInstaller"))
                {
                    EventLog.WriteEntry("PangeaFieldAppInstaller", msg);
                }

            }
            catch (Exception ee)
            {

            }
            
        }

        /// <summary>
        /// Writes a message to the Event Log.
        /// </summary>
        /// <param name="msg">Message to write</param>
        /// <param name="msgType">Type of message</param>
        public void WriteEventLog(String msg, EventLogEntryType msgType)
        {

            try
            {

                if (EventLog.SourceExists("PangeaFieldAppInstaller"))
                {
                    EventLog.WriteEntry("PangeaFieldAppInstaller", msg, msgType);
                }

            }
            catch (Exception ee)
            {

            }
            
        }
    }
}
