﻿//////////////////////////////////////////////////////////////////////////////////
/// The Structure used for Debug Logging.  Made it its own stuct incase 
/// it was to be used for Error Logging or anything else.
//////////////////////////////////////////////////////////////////////////////////

using System.Diagnostics;

namespace Pangea.Utilities.Tools.Logging
{
    public struct LogStruct
    {
        /// <summary>
        /// The stop watch for this Method
        /// </summary>
        Stopwatch _stopWatch;
        public Stopwatch Timer
        {
            get { return _stopWatch; }
        }

        /// <summary>
        /// Keept track of when the last log was written to make judging changes easier (in ms)
        /// </summary>
        long _lastLoggedTimeMs;
        public long LastLoggedTimeMs
        {
            get { return _lastLoggedTimeMs; }
        }

        /// <summary>
        /// Keept track of when the last log was written to make judging changes easier (in ticks)
        /// </summary>
        long _lastLoggedTimeTicks;
        public long LastLoggedTimeTicks
        {
            get { return _lastLoggedTimeTicks; }
        }

        // Used to Initialize the Sopwatch and set the default values of last logged times.
        public void Initialize()
        {
            _stopWatch = new Stopwatch();
            _lastLoggedTimeMs = 0;
            _lastLoggedTimeTicks = 0;
        }

        /// <summary>
        /// Used to Updated the Last Logged Times
        /// </summary>
        public void Update()
        {
            _lastLoggedTimeMs = _stopWatch.ElapsedMilliseconds;
            _lastLoggedTimeTicks = _stopWatch.ElapsedTicks;
        }
    };
}
