﻿/////////////////////////////////////////////////////////////////////////////
/// This is the Basic class for the log classes.  It just has the common
/// items that both classes have to use.
/////////////////////////////////////////////////////////////////////////////

using System;
using System.IO;

namespace Pangea.Utilities.Tools.Logging
{
    public class LogBasic
    {
        /// <summary>
        /// Stream that will be used to write the File.
        /// </summary>
        private StreamWriter _logStream;
        protected StreamWriter LogStream
        {
            get { return _logStream; }
            set
            {
                if (_logStream != value)
                {
                    if (_logStream != null)
                        _logStream.Close();

                    _logStream = value;
                }
            }
        }

        /// <summary>
        /// Name of the Log File.
        /// </summary>
        private String _fileName;
        protected String FileName
        {
            get { return _fileName; }
            set
            {
                if (_fileName != value)
                    _fileName = value;
            }
        }

        /// <summary>
        /// Name of the 2nd Log File.
        /// Only Used for the Info Logger
        /// </summary>
        private String _fileName2;
        protected String FileName2
        {
            get { return _fileName2; }
            set
            {
                if (_fileName2 != value)
                    _fileName2 = value;
            }
        }

        /// <summary>
        /// Should we append to the file or not.
        /// Defaults to true.
        /// </summary>
        private bool? _appendToFile;
        protected bool? AppendToFile
        {
            get { return _appendToFile; }
            set
            {
                if (_appendToFile != value)
                    _appendToFile = value;
            }
        }

        /// <summary>
        /// Write the message sent to the Log Stream.
        /// </summary>
        /// <param name="_message">Message to be saved to the File.</param>
        protected virtual void WriteLine(string _message, bool _addDateTime = true, bool _2ndFile = false)
        {
            try
            {
                if (_2ndFile)
                    LogStream = new StreamWriter(FileName2, _appendToFile.HasValue ? _appendToFile.Value : true);
                else
                    LogStream = new StreamWriter(FileName, _appendToFile.HasValue ? _appendToFile.Value : true);

                if (_addDateTime)
                    LogStream.WriteLine(String.Format("{0}  :  {1}", DateTime.Now, _message));
                else
                    LogStream.WriteLine(_message);

                LogStream.Close();
            }
            catch (Exception)
            {

            }
        }
    }
}
